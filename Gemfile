source 'https://rubygems.org'


gem 'rails', '4.0.2'
# Sass stylesheet integration with RoR
gem 'sass-rails', '~> 4.0.0'
# Our choice for robust and kickass RDBMS
gem 'pg'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# Provides generators for and ensures that app/interactors is included in your autoload paths
gem 'interactor-rails', '~> 1.0'
# Allows any model to follow any other model through a double polymorphic relationship on the Follow model
gem 'acts_as_follower'
# Allows any model to vote/be voted on
gem 'acts_as_votable', '~> 0.8.0'
# Activity tracking for models' used to create activity feed
gem 'public_activity'
# Provides models for questionnaire, surveys
gem 'survey', '~> 0.1'
# jQuery UI assets for Rails assets integration
gem 'jquery-ui-rails'
# HTTP server for Rails
gem 'unicorn'
# Procfile management
gem 'foreman'
# Twitter Bootstrap
gem 'bootstrap-sass', '~> 3.1.0'
# Calculating median for attributes
gem 'active_median'
# Cron job scheduler
gem 'rufus-scheduler'
# Access Google APIs
gem 'google-api-client'
# Grouping timestamps by week, month etc.
gem 'groupdate'
# File upload
gem 'paperclip', '~> 3.0'
# Sending email notifications when errors occur
gem 'exception_notification'
# Social auth and connect for Google Plus
gem 'omniauth-google-oauth2'
# for contacts
gem "omnicontacts"
# User authentication
gem 'devise'
# Social authentication
gem 'omniauth'
# FB authentication
gem 'omniauth-facebook'
# Background processing of jobs
gem 'sidekiq'
gem "net-ssh", "~> 2.7.0"
# jQuery resources for Rails assets pipeline integration
gem 'jquery-rails'
# Invite users through devise
gem 'devise_invitable'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# FB Graph API Ruby library
gem 'koala', '~> 1.8.0rc1'
# File hosting
gem 'aws-sdk'
# Identity model to store user authentication details
gem 'omniauth-identity'
# Font Awesome resources for Rails assets pipeline integration
gem 'font-awesome-rails'
# Pretty printing of objects for debugging and logging
gem 'awesome_print'
# Allow remote multi-part uploads
gem 'remotipart', '~> 1.2'
# Allow jQuery event binding with turbolinks
gem 'jquery-turbolinks'
# Bootstrap calender widget
gem 'bootstrap-datepicker-rails'
# Converts numbers to human readable strings
gem 'humanize'
# Loads environment variables from .env into ENV
gem 'dotenv-rails'
# Decorating objects for views
gem 'draper', '~> 1.3'
# for ordering of the weekly challenges
gem 'acts_as_list'
# Provides comments model with threaded comments support
gem 'acts_as_commentable_with_threading'
# Configuration/settings solution that uses an ERB enabled YAML file
gem 'settingslogic'
gem 'websocket-driver', '~> 0.3.3'
# Distribution of the v8 runtime libraries and headers
gem 'libv8', '3.3.10.4', :platform => :ruby
# Javascript runtime
gem 'therubyracer', '0.10.2', :platform => :ruby
# AWS S3
gem 'aws-s3'
# For making URLs look pretty
# for hyperlinking any url in the post
gem "auto_html"
# jquery file upload
gem 'jquery-fileupload-rails'
# for getting the meta information of a website.
gem 'metainspector'
# fitbit
gem 'fitgem'
# fitbit omniauth
gem 'omniauth-fitbit'
# moves omniauth
gem 'omniauth-moves'
# moves
gem 'moves'
# passing variable from rails to javascript
gem 'gon'
# Sugar.js resources for Rails assets pipeline integration
gem 'sugar-rails'
# Local Time is a Rails engine with helpers and JavaScript for displaying times and dates
gem 'local_time'
gem 'rails_admin'
# for providing authorization
gem 'cancancan', '~> 1.8'
# Automatically detect mobile devices
gem 'mobile-fu'
# for nested forms
gem 'nested_form'
gem 'httplog'
gem 'http_logger'
gem 'friendly_id', '~> 5.0.0'
# for creating api's
gem 'grape'
gem 'rest-client'
gem 'request_store'
# Row sorting library based on position attribute
gem 'ranked-model'
# To send messages inside a web application
gem 'mailboxer'

group :development do
  # rake routes in browser
  gem 'sextant'
  # Preview email in the browser without setting up email system
  gem "letter_opener"
  # Detect unused eager loading and log N+1 queries
  gem "bullet"
  # Disable asset load logging
  gem 'quiet_assets'
  # Profiling middleware for development
  gem 'rack-mini-profiler'
  # Find missing DB indexes
  gem 'lol_dba'
  # Capistrano
  gem 'capistrano', '3.1'
  gem 'capistrano-rvm'
  # Useful task libraries and methods for Capistrano
  gem 'capistrano-ext'
  # Performance management system for development
  gem 'newrelic_rpm'
end

group :staging, :production do
  # Heroku gem to manage assets pipeline and logging
  # This Heroku gem conflicts with logging etc. in our own server
  gem 'rails_12factor'
end

group :test do
  # For testing mailers
  gem 'capybara-email'
  # JS driver for capybara
  gem "poltergeist"
  # Simulate user for testing
  gem 'capybara'
  # Launching browser
  gem 'launchy'
  # Time travelling
  gem "timecop"
  # ActiveModel and ActiveRecord matchers like validations
  gem 'shoulda-matchers'
end

group :development, :test do
  # TDD framework
  gem 'rspec-rails'
  # Keep tests running all automatically
  gem 'guard-rspec', '2.5.0'
  # Spec DRb server
  gem 'spork-rails', '4.0.0'
  # Guard for spock
  gem 'guard-spork', '1.5.0'
  # Controlling external programs running in the background
  gem 'childprocess'
  # Factory generator for models
  gem 'factory_girl_rails'
  # Cleaning databases in testing
  gem "database_cleaner", "~> 1.2.0"
  # Debugging
  gem 'debugger'
  # Library for generating fake data such as names, addresses, and phone numbers
  gem 'faker'
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :profile do
  # For sql tuning and performance
  gem 'ruby-prof'
end
