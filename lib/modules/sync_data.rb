module SyncData
  def sync_data(user_id)
    user = User.find(user_id)
    identity = user.identities.find_by(provider: "fitbit") || user.identities.find_by(provider: "moves")
    if identity && identity.provider == "fitbit"
      @client = Fitgem::Client.new({
        :consumer_key => ENV["FITBIT_KEY"],
        :consumer_secret => ENV["FITBIT_SECRET"],
        :token => identity.token,
        :secret => identity.secret,
        :ssl => true
      })
      devices = @client.devices
      if !devices[0]#["errors"]["errorType"] == "oauth"
        # identity.delete
        Mailer.fitbit_reconnect_notifier(user).deliver
        @client.reconnect(identity.token,identity.secret)
      else
        ap devices
        last_sync_time = Date.parse devices[0]["lastSyncTime"]  rescue nil
        activities_to_be_synced = user.daily_activities.where("daily_activities.start_time >= ?",user.last_fitbit_synced_at).where("daily_activities.start_time < ?",last_sync_time).where(steps_covered:nil).order("daily_activities.start_time")

        if activities_to_be_synced.count > 0
          if (last_sync_time - user.last_fitbit_synced_at).to_i <= 30
            steps = @client.activity_on_date_range("steps", user.last_fitbit_synced_at, last_sync_time)["activities-steps"]
            calories = @client.activity_on_date_range("calories",user.last_fitbit_synced_at, last_sync_time)["activities-calories"]
            ap steps
            ap calories
          else
            steps = @client.activity_on_date_range("steps", last_sync_time - 30, last_sync_time)["activities-steps"]
            calories = @client.activity_on_date_range("calories",last_sync_time - 30, last_sync_time)["activities-calories"]
          end

          ap activities_to_be_synced.pluck(:start_time)
          activities_to_be_synced.each do |d|
            step = steps.select {|hash| Date.parse(hash["dateTime"]) == d.start_time }[0] || {}
            calorie =  calories.select {|hash| Date.parse(hash["dateTime"]) == d.start_time }[0] || {}
            ap step
            ap calorie
            if step["value"].to_i > 0 && calorie["value"].to_i > 0
              d.update_attributes(source:"fitbit")
              params = {}
              params["daily_activity"] = {}
              params["daily_activity"]["steps_covered"] = step["value"]
              params["daily_activity"]["calories"] = calorie["value"]
              params["daily_activity"]["start_time"] = d.start_time
              params["provider"] = "fitbit"
              params["user_id"] = user.id
              UpdateDailyActivity.perform(params)

              user.update_attributes(last_fitbit_synced_at: last_sync_time)
            end
          end
        end
        user.update_attributes(last_synced_at: Date.today)
      end
    end
    if identity && identity.provider == "moves"
      moves = Moves::Client.new(identity.auth_token)
      devices = moves.daily_summary
      last_sync_time = Date.parse devices[0]["lastUpdate"]  rescue nil
      activities_to_be_synced = user.daily_activities.where("daily_activities.start_time >= ?",user.last_moves_synced_at).where("daily_activities.start_time < ?",last_sync_time).where(steps_covered:nil).order("daily_activities.start_time")

      activities_to_be_synced.each do |d|
        summary = moves.daily_summary(d.start_time)[0]["summary"] rescue nil
        ap summary
        if summary
          steps = summary.select {|hash| hash["activity"] == "walking" }[0]["steps"]
          d.update_attributes(steps_covered: steps)
          d.update_attributes(source:"moves")
          params = {}
          params["daily_activity"] = {}
          params["daily_activity"]["steps_covered"] = steps
          params["daily_activity"]["start_time"] = d.start_time
          params["provider"] = "moves-app"
          params["user_id"] = user.id
          user.update_attributes(last_moves_synced_at: last_sync_time)
          UpdateDailyActivity.perform(params)
        end
      end
      user.update_attributes(last_synced_at: Date.today)
    end
  end
end