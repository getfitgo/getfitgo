desc "PG Backup"
namespace :pg do
  task :backup => [:environment] do
    # stamp the filename
    Time.zone = 'Chennai'
    datestamp = Time.now.in_time_zone.strftime("%Y-%m-%d_%H-%M-%S")

    # drop it in the db/backups directory temporarily
    backup_file = "/home/ubuntu/data_dump/#{Rails.env}_#{datestamp}_dump.sql.gz"

    # dump the backup and zip it up
    sh "pg_dump -h localhost -U postgres getfitgo_#{Rails.env} | gzip -c > #{backup_file}"

    send_to_amazon backup_file
    # remove the file on completion so we don't clog up our app
    # File.delete backup_file
  end
end

def send_to_amazon(file_path)
  file_name = File.basename(file_path)
  s3 = AWS::S3.new(:access_key_id => 'AKIAJF255ZNBUOTPZRRA',:secret_access_key => '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')
  bucket = s3.buckets[ENV['S3_BUCKET_NAME']]
  obj = bucket.objects["pg_backup/#{file_name}"]
  # push the file up
  obj.write(File.open("#{file_path}"))
end
