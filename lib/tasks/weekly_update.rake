desc "Weekly Update"
namespace :weekly do
  task :update => [:environment] do
    # stamp the filename
    Time.zone = 'Chennai'
    datestamp = Time.now.in_time_zone.strftime("%Y-%m-%d_%H-%M-%S")
    a=[]
    User.joins(:user_activities).where("user_activities.status = 'Progress'").joins(:challenges).where("challenges.type = 'MonthlyChallenge'").uniq.each{ |u|
      applicable_daily_activities = u.daily_activities.where.not(steps_covered: nil).where(start_time: [(Date.today - 8.days)...Date.today])
      best_performance = applicable_daily_activities.order(steps_covered: :desc).first
      a << [u.user_name,u.email,applicable_daily_activities.sum(:steps_covered),u.current_challenge.challenge.number_of_steps, u.current_challenge.start_time.strftime('%d/%m/%Y'), (best_performance.steps_covered rescue 0), (best_performance.start_time.strftime('%d/%m/%Y') rescue 'N.A.'),u.current_challenge.average, u.total_required_run_rate, (User.find(u.current_challenge.challenge.user_activities.select { |ua| ua.updated_daily_activities.count >= 5 }.map(&:user_id)).sort_by { |user| -user.current_challenge.total_steps_covered_in_last_week }.first.user_name rescue 'N.A.')]}; nil
      CSV.open("/home/ubuntu/data_dump/weekly_update_#{datestamp}.csv", "ab") do |csv|
        csv << ['User Name','Email','Steps Covered in Current Week','Current Challenge Target','Start Date','Best Steps Covered in Week','Best Day in Week','Average Steps','Required Run Rate','Performer of the Week']
        a.each { |b|
        csv << b
      }
    end
    Mailer.weekly_update_csv("/home/ubuntu/data_dump/weekly_update_#{datestamp}.csv").deliver if Rails.env.production?
  end
end