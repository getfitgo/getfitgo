Getfitgo::Application.routes.draw do
  match 'auth/fitbit/callback', to: 'omniauth_callbacks#fitbit', via: [:get, :post]
  match 'auth/moves/callback', to: 'omniauth_callbacks#moves', via: [:get, :post]
  match 'auth/facebook/callback', to: 'omniauth_callbacks#facebook', via: [:get, :post]
  match 'auth/google_oauth2/callback', to: 'omniauth_callbacks#google_oauth2', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]

  get "/policy" => "static_pages#policy"
  get "/terms_and_conditions" => "static_pages#tnc"
  namespace :questionnaires do
    resources :surveys
    resources :attempts, :only => [:new, :create,:update,:edit]
  end

  root to: 'homes#index'
  resource :dashboard, only: [:show]
  devise_for :users, path_names: {sign_in: 'login', signout: 'logout'}, controllers: {:invitations => 'invitations', :registrations => 'registrations', confirmations: 'confirmations'}

  match '/signup/:invitation_token', :controller => 'users', :action => 'new', via: :get, as: :signup

  resources :users, only: [:show] do
    resources :teams, except: [:edit, :update, :destroy]
    resource :reports, only: [:show]

  end
  resources :teams, only: [:new, :index, :show, :create] do
    post 'join' => 'memberships#create'
    resources :challenges, only: [:create, :show, :index] do
      post 'join' => 'user_activities#create'
      post 'choose' => 'team_activities#create'
    end
  end
  resources :users, only: [:show] do
    resource :report, only: :show
  end
  resources :monthly_challenges, controller: :challenges
  resources :challenges, only: [:create, :show] do
    post 'join' => 'user_activities#create'
    resources :teams, only: [:new, :create, :show] do
      post 'join' => 'memberships#create'
    end
  end
  get '/contacts/:importer/callback' => 'contacts#contacts_callback'
  get '/contacts/contacts_callback'
  resources :user_invitation_team, only: [:new]
  devise_scope :user do
    match '/team/:id/invite' => 'invitations#team', via: [:get, :post]
    post 'batch_invite' => 'invitations#batch_invite'
    get '/registrations/resend' => 'registrations#resend'
    get '/registrations/welcome_page' => 'registrations#welcome_page'
    get '/introduction' => 'registrations#introduction'
    get '/update_profile' => 'registrations#update_profile'
  end
  get '/oauth2callback' => 'contacts#contacts_callback'
  resources :daily_activities, only: [:create, :update]
  resources :team_activities, only: :update
  resources :user_activities, only: :update
  post 'sync' => 'user_activities#sync'

  resources :identities, :only => :destroy
  get 'post_registrations' => 'social_sharing#post_registrations', as: :post_registrations
  post 'post_badge_won' => 'social_sharing#post_badge_won', as: :post_badge_won
  get 'post_challenge_join/:id' => 'social_sharing#post_challenge_join', as: :post_challenge_join
  get 'post_challenge_completion/:id' => 'social_sharing#post_challenge_completion', as: :post_challenge_completion

  resources :friendships, only: [:create, :edit, :destroy]

  resources :activity, only: [] do
    post 'wow' => 'activities#wow'
    delete 'unwow' => 'activities#unwow'
  end
  resources :action_messages, only: [:new] do
    collection { post :import }
  end
  resources :posts, only: [:create, :destroy]

  resources :comments, only: [:create, :destroy, :update] do
  end

  post '/request_invite' => 'users#request_invite', as: :request_invite
  get '/activities/view_more' => 'activities#view_more'

  get '/notifications/unread' => 'notifications#unread'
  put '/notifications/mark_as_read' => 'notifications#mark_as_read'
  get '/notifications/refresh' => 'notifications#refresh'
end

