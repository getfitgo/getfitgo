Rails.application.config.middleware.use OmniAuth::Builder do  
  provider	:facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_SECRET'], scope: "publish_stream,email"
  provider :google_oauth2, ENV['GOOGLE_KEY'], "xsWvpIn4eOUwhHVJDByPVoUE", scope: "plus.me,userinfo.email"
  provider :fitbit,  ENV["FITBIT_KEY"], ENV["FITBIT_SECRET"]
  provider :moves, ENV['MOVES_KEY'], ENV['MOVES_SECRET'],scope: "activity"
end