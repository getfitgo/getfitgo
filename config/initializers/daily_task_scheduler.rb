require 'modules/sync_data'
include SyncData
s = Rufus::Scheduler.new

s.cron("0 4 * * *") do
  db do
    User.confirmed_users.each { |user|
      current_user_activity = user.current_challenge
      current_user_activity.update_status if current_user_activity
    }
  end
end

s.cron("0 5 * * *") do
  db do
    DailyTaskScheduler.perform if Rails.env.development?
  end
end

s.cron("30 4 * * *") do
  db do
    Identity.where(provider:"fitbit").each { |identity|
     sync_data(identity.user.id)
   }
   Identity.where(provider:"moves").each { |identity|
     sync_data(identity.user.id)
   }
 end
end

s.cron("30 13 * * *") do
  db do
    Identity.where(provider:"fitbit").each { |identity|
     sync_data(identity.user)
   }
   Identity.where(provider:"moves").each { |identity|
     sync_data(identity.user)
   }
 end
end

s.cron("30 5 * * *") do
  db do
    User.confirmed_users.each { |user|
      Notification.day_2_of_challenge(user) if user.last_completed_challenge && (user.current_challenge.day_of_challenge.eql? 2)
      Notification.day_4_of_warm_up(user) if !user.current_challenge.challenge.not_warm_up? && (user.current_challenge.day_of_challenge.eql? 4)
      Notification.update_profile(user) if user.profile.nil? && !user.current_challenge.challenge.not_warm_up? && (user.current_challenge.day_of_challenge >= 6) # TODO: Notification will not get cleared on profile update
      Notification.day_7_profile_updated(user) if user.profile && (user.current_challenge.day_of_challenge.eql? 7)
      Notification.day_after_challenge_completion(user) if user.last_completed_challenge && user.last_completed_challenge.challenge.not_warm_up? && (user.current_challenge.day_of_challenge.eql? 1)
      Notification.data_not_updated(user) if user.data_not_updated_for(3)
    }
  end
end

s.cron("45 5 * * mon") do
  Notification.consistent_performance(user) if user.consistent_performer?
end

def db
  ActiveRecord::Base.connection_pool.with_connection do
    yield
  end
end
