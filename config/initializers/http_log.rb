require 'httplog'
require 'http_logger'
HttpLog.options[:logger] = Rails.logger
HttpLog.options[:severity]      = Logger::Severity::DEBUG
HttpLog.options[:log_connect]   = true
HttpLog.options[:log_request]   = true
HttpLog.options[:log_headers]   = false
HttpLog.options[:log_data]      = true
HttpLog.options[:log_status]    = true
HttpLog.options[:log_response]  = true
HttpLog.options[:log_benchmark] = true
HttpLog.options[:compact_log]   = false # setting this to true will make all "log_*" options redundant

# only log requests made to specified hosts (URLs)
HttpLog.options[:url_whitelist_pattern] = /.*/
# overrides whitelist 
HttpLog.options[:url_blacklist_pattern] = nil 



HttpLogger.logger = Rails.logger
HttpLogger.log_headers = true 