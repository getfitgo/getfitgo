require "omnicontacts"

Rails.application.middleware.use OmniContacts::Builder do
  importer :gmail, ENV["GMAIL_KEY"], ENV["GMAIL_SECRET"], {:redirect_path => "/contacts/contacts_callback",:max_results => 10000}
end

