# config valid only for Capistrano 3.1
lock '3.1.0'


set :application, 'getfitgo'
set :repo_url, 'git@bitbucket.org:getfitgo/getfitgo.git'
# role :web, "ec2-54-254-153-84.ap-southeast-1.compute.amazonaws.com" # This may be the same as your `Web` server
# role :db, "ec2-54-254-153-84.ap-southeast-1.compute.amazonaws.com", :primary => true # This is where Rails migrations will run

# Default branch is :master
# set :branch, "staging"
# set :rails_env, "production"
set :migrate_target, :current
# set :migrate_env, :production
set :linked_files, %w{config/database.yml .env}

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  task :precompile do
    on roles(:web) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "install"
          execute :bundle, "exec rake assets:precompile"
          execute :bundle, "exec rake db:migrate"
        end
      end
    end
  end
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end
  before :publishing, :precompile
  after :publishing, :restart


  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
     execute "sudo service apache2 restart"
   end
 end
end

namespace :pg do
  desc "Run PG backup rake task"
  task :backup do
    on roles(:web) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "exec rake pg:backup"
        end
      end
    end
  end
end

namespace :logs do
 desc "tail rails logs"
  task :tail do
    on roles(:app) do
      trap("INT") { puts 'Y U NO SEE MORE LOGS?!'; exit 0; }
      execute "tail -f #{current_path}/log/#{fetch(:rails_env)}.log"
    end
  end
end

namespace :weekly do
 desc "email weekly update csv"
  task :update do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "exec rake weekly:update"
        end
      end
    end
  end
end
