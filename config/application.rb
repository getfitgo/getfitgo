require File.expand_path('../boot', __FILE__)

require 'csv'
require 'rails/all'
require 'net/http'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Getfitgo
  class Application < Rails::Application
    I18n.config.enforce_available_locales = true
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif *.svg *.eot *.woff *.ttf)

    # add app/assets/fonts to the asset path
    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    config.time_zone = 'Chennai'
    config.active_record.default_timezone = :local
    # Speed things up by not loading Rails env
    config.assets.initialize_on_precompile = false

    config.quiet_assets = true
    config.autoload_paths += %W(#{config.root}/lib)
  end
end
