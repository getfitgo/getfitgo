require 'rubygems'
require 'spork'
require "paperclip/matchers"
require 'capybara/poltergeist'
require 'capybara/email/rspec'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
  ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

  RSpec.configure do |config|
    Capybara.javascript_driver = :poltergeist
    config.include Paperclip::Shoulda::Matchers
    config.include Features::SessionHelpers, type: :feature
    config.include Devise::TestHelpers, :type => :controller
    config.include Devise::TestHelpers, :type => :helper
    config.include Features::SessionHelpers
    config.use_transactional_fixtures = false
    config.include FactoryGirl::Syntax::Methods
    config.infer_base_class_for_anonymous_controllers = false
    config.order = "random"
    config.include Features::SessionHelpers, type: :feature
    config.include Capybara::DSL
    config.before(:suite) do
      DatabaseCleaner.clean_with(:truncation)
    end

    config.before(:each) do
      DatabaseCleaner.strategy = :transaction
    end

    config.before(:each, :js => true) do
      DatabaseCleaner.strategy = :truncation
    end
    DatabaseCleaner.logger = Rails.logger

    config.before(:each) do
      DatabaseCleaner.start
    end

    config.after(:each) do
      DatabaseCleaner.clean
    end

    config.tty = true
  end
end

Spork.each_run do
  # This code will be run each time you run your specs.

end
