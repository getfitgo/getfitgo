# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :corporate do
    name "MyString"
    url "MyString"
    message "MyText"
    status false
    start_date "2014-05-20"
    end_date "2014-05-20"
  end
end
