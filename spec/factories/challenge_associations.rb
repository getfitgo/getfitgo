# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :challenge_association do
    monthly_challenge nil
    weekly_challenge nil
  end
end
