# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :corporate_challenge, :class => 'CorporateChallenges' do
    corporate nil
    challenge nil
  end
end
