# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :insight do
    priority 1
    rule "MyText"
    message "MyText"
  end
end
