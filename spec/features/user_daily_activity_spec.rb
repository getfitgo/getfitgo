require 'spec_helper'

feature "Daily Activity" do
  before do
    FactoryGirl.reload
    Timecop.freeze(Time.local(2014, 01, 01))
  end

  after do
    Timecop.return
  end
  scenario "User can feed in his daily activity" do
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    click_button "Update"
    fill_in "daily_activity_steps_covered",with: 10000
    fill_in "daily_activity_calories",with: 1000
    fill_in "daily_activities_feel","Happy"
    click_button "Save"
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    user_activity = create(:user_activity_with_daily_activity, challenge: challenge, user: user, start_time: Time.now)
    daily_activities = user.daily_activities.where("daily_activities.start_time < ?", Time.now)
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    click_link challenge.name

    expect(page).to have_content "Challenge started on #{ActiveSupport::Inflector.ordinalize((Time.now - 1.day).strftime("%B %d, %Y"))}"
    fill_in (Time.now-1.day).strftime("%Y-%m-%d"), with: 100000

    click_button "add_steps_#{(Time.now-1.day).strftime("%Y-%m-%d")}"
    expect(page).to have_content "Congrats you just won another badge. MasterBlaster badge."
  end
end
