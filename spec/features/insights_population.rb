require 'spec_helper'

feature "Insights" do
  scenario "Anonymous access to insights url" do
    it "should redirect to login page" do
      visit insights_path
      page.should redirect_to new_user_sessions_path
    end
  end

  scenario "Anyone except the authorized user visits insights page"
    it "should throw unauthorized alert message" do

    end

end
