require 'spec_helper'

feature 'User starts walking' do
  before do
    new_time = Time.local(2008, 9, 1, 12, 0, 0)
    Timecop.freeze(new_time)
    Badge.create(:name => "Debutant", :display_name => "Debutant")
    Badge.create(:name => "MasterBlaster", :display_name => "MasterBlaster")
    Badge.create(:name => "Legend", :display_name => "Legend")
    Badge.create(:name => "Hattrick", :display_name => "Hattrick")
    Badge.create(:name => "Maiden", :display_name => "Maiden")
  end
  scenario "does 10k steps for the first time" do
    challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
    sign_in_as_dummy_user
    click_link "Walking Challenge"
    click_link "Join Challenge"
    fill_in "user_activity_start_time", with: "31/08/2008"
    click_button "Start Challenge"
    click_link "Walking Challenge"
    fill_in "2008-08-31", with: "10000"
    click_button "add_steps_2008-08-31"
    expect(page).to have_content "Congrats you just won your first badge. Debutant badge."
  end

  scenario "does 10k steps 3 times continously for the first time" do
    challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
    sign_in_as_dummy_user
    click_link "Walking Challenge"
    click_link "Join Challenge"
    fill_in "user_activity_start_time", with: "29/08/2008"
    click_button "Start Challenge"
    click_link "Walking Challenge"
    fill_in "2008-08-29", with: "10000"
    click_button "add_steps_2008-08-29"
    fill_in "2008-08-30", with: "10000"
    click_button "add_steps_2008-08-30"
    fill_in "2008-08-31", with: "10000"
    click_button "add_steps_2008-08-31"
    expect(page).to have_content "Congrats you just won another badge. Hattrick badge."
  end

  scenario "does 10k steps 7 times continously for the first time" do
    challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
    sign_in_as_dummy_user
    click_link "Walking Challenge"
    click_link "Join Challenge"
    fill_in "user_activity_start_time", with: "25/08/2008"
    click_button "Start Challenge"
    click_link "Walking Challenge"
    fill_in "2008-08-25", with: "10000"
    click_button "add_steps_2008-08-25"
    fill_in "2008-08-26", with: "10000"
    click_button "add_steps_2008-08-26"
    fill_in "2008-08-27", with: "10000"
    click_button "add_steps_2008-08-27"
    fill_in "2008-08-28", with: "10000"
    click_button "add_steps_2008-08-28"
    fill_in "2008-08-29", with: "10000"
    click_button "add_steps_2008-08-29"
    fill_in "2008-08-30", with: "10000"
    click_button "add_steps_2008-08-30"
    fill_in "2008-08-31", with: "10000"
    click_button "add_steps_2008-08-31"
    expect(page).to have_content "Congrats you just won another badge. Maiden badge."
  end

  # scenario "does 10k steps 15 times continously for the first time" do
  #   expect(page).to have_content "Congrats you ust won another badge. Consistent badge."
  # end

  scenario "does 15k steps on a given day" do
    challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
    sign_in_as_dummy_user
    click_link "Walking Challenge"
    click_link "Join Challenge"
    fill_in "user_activity_start_time", with: "31/08/2008"
    click_button "Start Challenge"
    click_link "Walking Challenge"
    fill_in "2008-08-31", with: "15000"
    click_button "add_steps_2008-08-31"
    expect(page).to have_content "Congrats you just won another badge. MasterBlaster badge."

  end

  # scenario "does 300k steps totally" do
  #   expect(page).to have_content "Congrats you ust won another badge. Champion badge."
  # end
end
