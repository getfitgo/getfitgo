require "spec_helper"

feature "User login" do

  scenario "with valid email and password", :js => true do
    Timecop.travel(Time.now)
    sign_up_with "ankothari@gmail.com", "Ankur Kothari", "aakk3322"
    expect(page).to have_content "Welcome Ankur Kothari,"
    click_link "Logout"
    click_link "Login"
    fill_in "Email", with: "ankothari@gmail.com"
    fill_in "Password", with: "aakk3322"
    click_button "Sign in"
    expect(page).to have_content "Logged in as Ankur Kothari"
    expect(page).to have_link "Dashboard"
  end
end
