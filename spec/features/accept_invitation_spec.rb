require 'spec_helper'

feature "Accept invitation" do
	background do
    # will clear the message queue
    clear_emails

  end
  scenario "invitee gets 50 points" do
  	user = create(:user)
    user.confirm!
    sign_in_as(user)
    save_and_open_page
    visit '/users/invitation/new'
    save_and_open_page
    fill_in "contact_list",with: "a@a.com"
    click_link "Send Invites"
    click_link "Manage Account"
    click_link "Sign out"
  	link = ActionMailer::Base.deliveries.last.body.raw_source.match(/href="(?<url>.+?)">/)[:url]
    ap link
    visit link
    save_and_open_page
    fill_in "user_user_name",with:"ankur"
    fill_in "user_password",with:"aaaaaaaaa"
    check 'tnc'
    click_button "Sign Up"
    expect(inviter.score).to eq 50
  end
 end