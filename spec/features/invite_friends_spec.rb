require 'spec_helper'

feature "Invite friends" do
  context "unconfirmed user" do
    scenario "cannot invite friends" do
      visit root_path
      user = create(:user)
      sign_in_as(user)
      click_link "Invite friends to getfitgo"
      fill_in "contact_list", with: "ankothari4@gmail.com"
      click_button "Invite!"
      expect(page).to have_content "Please confirm account first"
    end
  end

  context "confirmed user" do
    scenario "can invite friends" do
      visit root_path
      sign_in_as_dummy_user
      click_link "Invite friends to getfitgo"
      fill_in "contact_list", with: "ankothari4@gmail.com"
      click_button "Invite!"
      expect(page).to have_content "Invitations sent"
    end
  end
end
