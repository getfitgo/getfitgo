require 'spec_helper'

feature "User finishes a monthly challenge" do
  before do
    FactoryGirl.reload
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    user = create(:user)
    user.confirm!
    user_activity = create(:user_activity, status: "Completed", steps_covered: 5001, challenge: challenge, user: user)
    sign_in_as user
  end
  context "basic challenge" do
    scenario "finishes the challenge given to him on the last day of the challenge" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "6800"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 197200 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "2800"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 200000 points."
    end

    #   scenario "finishes the challenge given to him before the last day of the challenge" do
    #     expect(page).to have_content "Congrats for finishing the challenge. Do you want to continue or start a new challenge?."
    #   end

    scenario "does not finish the challenge" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "5000"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 145000 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "5000"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 150000 points."
    end

    scenario "finishes the basic challenge and also overshoots the advanced challenge" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "11000"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 319000 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "4000"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 323000 points."
    end
  end

  context "advanced challenge" do
    scenario "finishes the advanced challenge given to him" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "10000"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 290000 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "10000"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 300000 points."
    end

    scenario "does not finish advanced challenge and not even the basic challenge" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "5000"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 145000 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "5000"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 150000 points."
    end

    scenario "finishes the basic challenge but not the advanced challenge" do
      Timecop.travel(Time.now + 30.days)
      click_link "Walking Challenge (200000)"
      1.upto(29).each do |index|
        fill_in "#{(Time.now-index.day).strftime("%Y-%m-%d")}", with: "10000"
        click_button "add_steps_#{(Time.now-index.day).strftime("%Y-%m-%d")}"
      end
      expect(page).to have_content "You have covered 290000 steps so far since beginning of the challenge"
      fill_in "#{(Time.now-30.day).strftime("%Y-%m-%d")}", with: "5000"
      click_button "add_steps_#{(Time.now-30.day).strftime("%Y-%m-%d")}"
      expect(page).to have_content "Congrats for finishing the challenge. You just got 295000 points."
    end
  end
end
