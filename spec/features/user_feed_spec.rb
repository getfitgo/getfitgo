require 'spec_helper'

feature "User activity feed, wow and number of wow and who wowed them" do
  scenario "User can see his own activity feed" do
    user = create(:user)
    user.confirm!
    sign_in_as(user)
    expect(page).to have_content "Started Warmup challenge"
    expect(page).to have_content 'Joined getfitgo'
  end

  scenario "User sees the link to high5 other users feed" do
    user = create(:user)
    user.confirm!
    user1 = create(:user)
    user1.confirm!
    sign_in_as user1
    visit user_path user
    expect(page).to have_link "High 5"
  end

  scenario "User can high5 anothers user feed" do
    user = create(:user)
    user.confirm!
    user1 = create(:user)
    user1.confirm!
    ids = user.activities.pluck(:id)
    sign_in_as user1
    visit user_path user
    click_link "high5_#{ids[0]}"
    expect(page).to have_content "1"
    expect(page).not_to have_link "high5_#{ids[0]}"
  end
end
