require "spec_helper"

feature "User registeration", :js => true do
  context "User can signup" do
    scenario "with valid email, name and password" do
      visit root_path
      sign_up_with "ankothari@gmail.com", "Ankur Kothari", "aakk3322"
      # User.find_by(email:"ankothari@gmail.com")
      expect(page).to have_content("Welcome Ankur Kothari,")
      expect(page).to have_content("Logged in as Ankur Kothari")
    end
  end
  context "User can not signup" do
    scenario "with invalid email" do
      visit root_path
      sign_up_but_no_confirmation "ankothari@gmail", "Ankur Kothari", "aakk3322"
      expect(page).to have_content("Email is invalid")
    end
    scenario "with password less than 8 characters" do
      visit root_path
      sign_up_but_no_confirmation "ankothari@gmail.com", "Ankur Kothari", "aakk332"
      expect(page).to have_content("Password is too short (minimum is 8 characters)")
    end

    scenario "with empty email, password, username" do
      visit root_path
      sign_up_but_no_confirmation "", "", ""
      expect(page).to have_content("Email can't be blank")
      expect(page).to have_content("Password can't be blank")
      expect(page).to have_content("User name can't be blank")
    end
  end
end
