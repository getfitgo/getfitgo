require 'spec_helper'

feature "Challenges" do
  before do
    FactoryGirl.reload
    Timecop.freeze(Time.now)
    redis_instance = MockRedis.new
    Redis.stub(:new).and_return(redis_instance)
    Redis::Store.stub(:new).and_return(redis_instance)
    @challenge1 = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    challenge2 = create(:challenge, number_of_steps: 60000, number_of_days: 7)
    challenge3 = create(:challenge, number_of_steps: 70000, number_of_days: 7)
    challenge4 = create(:challenge, number_of_steps: 80000, number_of_days: 7)
  end
  scenario "User can view challenges" do
    visit root_path
    sign_in_as_dummy_user
    click_link "Challenges"
    expect(page).to have_link "Walking Challenge 7 days 80000 steps"
  end
  scenario "User can view a particular challenge" do
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    visit root_path
    sign_in_as_dummy_user
    click_link "Challenges"
    click_link "Walking Challenge 7 days 80000 steps"
    expect(page).to have_link "Join Challenge"
  end

  scenario "User is asked to login when he tries to join a challenge" do
    challenge = create(:challenge)
    visit root_path
    click_link "Challenges"
    click_link "Walking Challenge 7 days 80000 steps"
    expect(page).to have_css "h2", text: "Sign in"
  end
  scenario "User can join a challenge" do
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    sign_in_as_dummy_user
    click_link "Challenges"
    click_link "Walking Challenge 7 days 80000 steps"
    click_link "Join Challenge"
    expect(page).to have_content "Challenge Joined."
  end

  # scenario "admin of a team is asked to select a start time after he has choosen a challenge" do
  #   challenge = create(:challenge)
  #   visit root_path
  #   click_link "Sign up"
  #   fill_in "Email",with: "ankothari@gmail.com"
  #   fill_in "User name",with: "Ankur Kothari"
  #   fill_in "Password",with: "aakk3322"
  #   click_button "Create Account"
  #   User.find_by(email: "ankothari@gmail.com").confirm!
  #   click_link "Challenges"
  #   click_link challenge.name
  #   click_link "Join Challenge"
  # end

  scenario "user is asked to select a start time after he has choosen a challenge" do
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    visit root_path
    sign_in_as_dummy_user
    click_link "Walking Challenge 7 days 80000 steps"
    click_link "Join Challenge"
    expect(page).to have_button "Start Challenge"
  end

  scenario "Current Challenges are shown on the dashboard of the user" do
    visit root_path
    sign_in_as_dummy_user
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    click_link "Choose Walking Challenge 7 days 80000 steps"
    fill_in "Start time", with: Time.now.to_date
    click_button "Start Challenge"
    if Time.now > Date.today + 5.5.hours
      expect(page).to have_content "Walking Challenge(80000) started on #{ActiveSupport::Inflector.ordinalize(Time.now.to_date.strftime("%B %d, %Y"))}"
    else
      expect(page).to have_content "Walking Challenge(80000) starts on #{ActiveSupport::Inflector.ordinalize(Time.now.to_date.strftime("%B %d, %Y"))}"
    end
  end

  scenario "My teams are shown on the dashboard of the users" do
    team = create(:team)
    visit root_path
    sign_in_as_dummy_user
    click_link "Teams"
    click_link team.name
    click_link "Join Team"
    visit dashboard_path
    expect(page).to have_css "h2", "My Teams"
    expect(page).to have_link team.name
  end

  scenario "User visits a particular challenge he has joined." do
    visit root_path
    sign_in_as_dummy_user
    click_link "Walking Challenge 7 days 80000 steps"
    click_link "Join Challenge"
    click_button "Start Challenge"
    click_link "Walking Challenge(80000)"
    expect(page).to have_content("The goal of this challenge is to walk 80000 steps in 7 days")
    expect(page).to_not have_link "Join Challenge"
  end

  scenario "User visits a joined challenge which shows him his activity" do
    Timecop.travel(2014, 01, 01)
    visit root_path
    sign_in_as_dummy_user
    click_link "Walking Challenge 7 days 80000 steps"
    click_link "Join Challenge"
    click_button "Start Challenge"
    Timecop.travel(2014, 01, 02)
    click_link "Walking Challenge(80000)"
    fill_in "#{(Time.now-1.day).strftime("%Y-%m-%d")}", with: "12000"
    click_button "add_steps_#{(Time.now-1.day).strftime("%Y-%m-%d")}"
    expect(page).to have_content "You have covered 12000 steps so far since beginning of the challenge"
  end

  scenario "User visits a joined challenge can see the status of the challenge as participating, completed or invited." do
    visit root_path
    sign_in_as_dummy_user
    click_link "Walking Challenge 7 days 80000 steps"
    click_link "Join Challenge"
    click_button "Start Challenge"
    Timecop.travel(Time.now + 1.days)
    expect(page).to have_content "Progress"
  end

  # scenario "Given user has joined a challenge. He can then invite friends to join him. Before he can ask them to join he will be asked to create a
  #   team with a name" do
  #   challenge = create(:challenge,number_of_steps:50000,number_of_days:7)
  #   visit root_path
  #   click_link "Sign up"
  #   fill_in "Email",with: "ankothari@gmail.com"
  #   fill_in "User name",with: "Ankur Kothari"
  #   fill_in "Password",with: "aakk3322"
  #   click_button "Create Account"
  #   User.find_by(email: "ankothari@gmail.com").confirm!
  #   click_link "Challenges"
  #   click_link challenge.name
  #   click_link "Join Challenge"
  #   expect(page).to have_link "Invite friends by creating a team."
  # end
end
