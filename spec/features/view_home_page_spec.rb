require 'spec_helper'

feature "User visits homepage" do
  scenario "User is unauthenticated should see registration links" do
    visit root_path
    click_link "Login"
    expect(page).to have_content "Sign up"
    expect(page).to have_content "Sign in"
    expect(page).to have_content "Sign in with Facebook"
    expect(page).to have_content "Sign in with Google"
  end
end
