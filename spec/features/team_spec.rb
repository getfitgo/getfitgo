require 'spec_helper'

feature "Team formation" do
  scenario "User cannnot see a team path unless signed in" do
    team = create(:team)
    visit root_path
    click_link "Teams"
    click_link team.name
    expect(page).to have_css "h2", text: "Sign in"
  end

  scenario "User creates a team by himself" do
    visit root_path
    sign_in_as_dummy_user
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    expect(page).to have_content "Team GetFitGo created."
    expect(page).to have_content "Now pick a challenge first or invite friends."
    expect(page).to have_link "Invite friends"
    expect(page).to have_link "Challenges"
    click_link "Invite friends"
    fill_in "contact_list", with: "murali@getfitgo.in"
    click_button "Invite!"
    expect(page).to have_content "Invitations sent"
  end

  # scenario "User first clicks on a challenge, invites friends and then forms a team" do
  #   challenge = create(:challenge)
  #    visit root_path
  #    click_link "Sign up"
  #    fill_in "Email",with: "ankothari@gmail.com"
  #    fill_in "User name",with: "Ankur Kothari"
  #    fill_in "Password",with: "aakk3322"
  #    click_button "Create Account"
  #    User.find_by(email: "ankothari@gmail.com").confirm!
  #    click_link "Challenges"
  #    click_link challenge.name
  #    click_link "Join Challenge"
  #    click_link "Invite friends by creating a team."
  #    fill_in "Name",with: "Team1"
  #    click_button "Create Team"
  #    click_link "Invite friends"
  #    fill_in "Email",with: "murali@getfitgo.in"
  #    click_button "Send an invitation"
  #    # expect(page).to have_content "Invitation sent"
  #  end

  # scenario "If user first creates a challenge, then creates a team then he should not see the challenges link" do
  #   challenge = create(:challenge)
  #   visit root_path
  #   click_link "Sign up"
  #   fill_in "Email",with: "ankothari@gmail.com"
  #   fill_in "User name",with: "Ankur Kothari"
  #   fill_in "Password",with: "aakk3322"
  #   click_button "Create Account"
  #   User.find_by(email: "ankothari@gmail.com").confirm!
  #   click_link "Challenges"
  #   click_link challenge.name
  #   click_link "Join Challenge"
  #   click_link "Invite friends by creating a team."
  #   fill_in "Name",with: "GetFitGo"
  #   click_button "Create Team"
  #   expect(page).to_not have_link "Choose Challenges"
  # end
end
