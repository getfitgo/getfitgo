require "spec_helper"

feature "Start day of the challenge" do
  scenario "user selects a date after today" do
    Timecop.freeze(Time.local(1990))
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    sign_in_as_dummy_user
    visit root_path
    click_link "Challenges"
    click_link challenge.name
    click_link "Join Challenge"
    fill_in "Start time", with: (Date.today + 2.days).strftime("%m/%d/%Y")
    click_button "Start Challenge"
    expect(page).to have_content "Challenge starts on "
  end
end
