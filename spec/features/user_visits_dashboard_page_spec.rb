require 'spec_helper'

feature "User visits dashboard page" do
  scenario "sees the badges he has earned" do
    user = create(:user)
    user.confirm!
    debutant_badge = create(:badge, name: "Debutant Badge")
    user.award_badge("Debutant Badge")
    sign_in_as(user)
    expect(page).to have_content "Badges won"
    expect(page).to have_content "Debutant Badge"
  end
end
