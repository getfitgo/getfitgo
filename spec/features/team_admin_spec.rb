require 'spec_helper'

feature "User creates a team" do
  scenario "becomes the admin of that team." do
    user = create(:user)
    user.confirm!
    visit root_path
    sign_in_as(user)
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    expect(Team.find_by(name: "GetFitGo").admin).to eq User.find_by(email: user.email)
  end

  scenario "when he chooses a challenge a new activity is created which does not have a start day" do
    challenge = create(:challenge, number_of_steps: 50000, number_of_days: 7)
    visit root_path
    sign_in_as_dummy_user
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    click_link "Challenges"
    click_link challenge.name
    click_link "Join Challenge"
    expect(page).to have_content "Challenge Joined. Please specify the start date of the challenge on the challenge page."
  end
end
