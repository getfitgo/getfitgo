require 'spec_helper'

feature "User visits team page" do
  scenario "User is asked to login when he tries to join a team" do
    team = create(:team)
    visit root_path
    click_link "Teams"
    click_link team.name
    expect(page).to have_css "h2", text: "Sign in"
  end
  scenario "sees a join team link for the team he has not created." do
    visit root_path
    sign_in_as_dummy_user
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    visit root_path
    click_link "Teams"
    click_link "GetFitGo"
    click_link "Logout"
    visit root_path
    sign_in_as_dummy_user
    click_link "Teams"
    click_link "GetFitGo"
    expect(page).to have_link "Join Team"
  end
  scenario "does not see a join team for the team he has created, he auto joins it." do
    visit root_path
    sign_in_as_dummy_user
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    visit root_path
    click_link "Teams"
    click_link "GetFitGo"
    expect(page).to_not have_link "Join Team"
  end

  scenario "can join a team only after he has clicked on the confirmation link" do
    visit root_path
    user = create(:user)
    sign_in_as(user)
    click_link "Create Team"
    fill_in "Name", with: "GetFitGo"
    click_button "Create Team"
    expect(page).to have_content "Please confirm account first"
  end
end
