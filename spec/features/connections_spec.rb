require 'spec_helper'

feature "User can send a friend request to another user" do
  before do
    @from_user = create(:user)
    @from_user.confirm!
    @to_user = create(:user)
    @to_user.confirm!
  end
  scenario "user can send a friend request to another" do
    visit root_path
    sign_in_as @from_user
    visit user_path @to_user
    expect(page).to have_link "Add Friend"
  end
  scenario "after user has sent a friend request" do
    visit root_path
    sign_in_as @from_user
    visit user_path @to_user
    click_link "Add Friend"
    expect(page).to have_content "Confirmation Pending"
  end
  scenario "On the other user page a link is shown to accept the friendship" do
    @from_user.friendships.create(:friend_id => @to_user.id)
    sign_in_as @to_user
    expect(page).to have_content @from_user.email
    expect(page).to have_link "Accept Friend"
  end
  scenario "User should see list of all friends on his dashboard" do
    @from_user.friendships.create(:friend_id => @to_user.id, confirmed: true)
    sign_in_as @from_user
    expect(page).to have_content @to_user.user_name
    expect(page).to have_link "remove"
  end

  scenario "User can remove a friend from his dashboard" do
    @from_user.friendships.create(:friend_id => @to_user.id, confirmed: true)
    sign_in_as @from_user
    click_link "remove"
    expect(page).not_to have_content @to_user.user_name
  end
  scenario "User can remove a friend from the profile page of the user" do
    @from_user.friendships.create(:friend_id => @to_user.id, confirmed: true)
    sign_in_as @from_user
    visit user_path @to_user
    click_link "remove"
    expect(page).not_to have_content @to_user.user_name
  end
end
