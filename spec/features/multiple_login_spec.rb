require 'spec_helper'

feature "User should be able to link his social accounts with a single email address", :js => true do
  scenario "User signs up with his email address and password can connect with his facebook account" do
    visit root_path
    sign_up_with "ankothari@gmail.com", "Ankur Kothari", "aakk3322"
    click_link "Social Connect"
    click_button "Link with Facebook"
    expect(page).to have_content 'Successfully linked that account!'
    expect(page).to have_content "Logged in as Ankur Kothari"
  end
  scenario "User signs up with his email address and password can connect with his twitter account" do
    visit root_path
    sign_up_with "ankothari@gmail.com", "Ankur Kothari", "aakk3322"
    click_link "Social Connect"
    click_button "Link with Google"
    expect(page).to have_content 'Successfully linked that account!'
    expect(page).to have_content "Logged in as Ankur Kothari"
  end
  scenario "User signs up with his email address and password can connect with his twitter account" do
    visit root_path
    sign_up_with "ankothari@gmail.com", "Ankur Kothari", "aakk3322"
    click_link "Social Connect"
    click_button "Link with Google"
    expect(page).to have_content 'Successfully linked that account!'
    expect(page).to have_content "Logged in as Ankur Kothari"
  end

  # scenario "User signs up facebook" do
  #   visit root_path
  #   click_link "Sign up"
  #   fill_in "Email",with: "ankothari@gmail.com"
  #    fill_in "User name",with: "Ankur Kothari"
  #    fill_in "Password",with: "aakk3322"
  #    click_button "Create Account"
  #    # click_link "Linked Accounts"
  #    click_link "Connect with facebook"
  #    expect(page).to have_content 'Successfully linked that account!'
  #    expect(page).to have_content "Logged in as Ankur Kothari"
  # end
  # scenario "User signs up with his email address and password can connect with his twitter account" do
  #   visit root_path
  #   click_link "Sign up"
  #   fill_in "Email",with: "ankothari@gmail.com"
  #    fill_in "User name",with: "Ankur Kothari"
  #    fill_in "Password",with: "aakk3322"
  #    click_button "Create Account"
  #    # click_link "Linked Accounts"
  #    click_link "Connect with twitter"
  #    expect(page).to have_content 'Successfully linked that account!'
  #    expect(page).to have_content "Logged in as Ankur Kothari"
  # end
end
