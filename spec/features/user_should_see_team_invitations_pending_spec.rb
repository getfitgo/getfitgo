require "spec_helper"

feature "Team invitations" do
  scenario "User should the team invitations on his dashboard path" do
    team = create(:team)
    user = create(:user)
    user.confirm!
    user.update_attributes(:invited_to_team_id => team.id)
    visit root_path
    click_link "Login"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Sign in"
    expect(page).to have_link "Join #{team.name}"
    click_link "Join #{team.name}"
    team.reload
    user.reload
    expect(team.members).to include user
  end
end
