require 'spec_helper'

feature "Team page" do
  scenario "Team page shows all the members who have joined" do
    team = create(:team)
  end
  scenario "Team page shows all the members whose invitation is pending" do
    team = create(:team)
  end

  scenario "should show all the teams who are taking part in the same challenge and their status" do
    challenge = create(:challenge)
    team = create(:team_with_members, name: "Getfitgo")
    team1 = create(:team_with_members, name: "Apple")
    team2 = create(:team_with_members, name: "Vodafone")
    team3 = create(:team_with_members, name: "Ebay")
    team4 = create(:team_with_members, name: "Tech Mahindra")
    team_activity1 = create(:team_activity, team: team1, challenge: challenge)
    team_activity2 = create(:team_activity, team: team2, challenge: challenge)
    team_activity3 = create(:team_activity, team: team3, challenge: challenge)
    team_activity4 = create(:team_activity, team: team4, challenge: challenge)
    team_activity = create(:team_activity, team: team, challenge: challenge)
    visit root_path
    sign_in_as_dummy_user
    click_link "Teams"
    click_link team.name
    expect(page).to have_content team.name
    expect(page).to have_content team.admin.email
    expect(page).to have_content team1.name
    expect(page).to have_content team1.admin.email
    expect(page).to have_content team2.name
    expect(page).to have_content team2.admin.email
    expect(page).to have_content team3.name
    expect(page).to have_content team3.admin.email
    expect(page).to have_content team4.name
    expect(page).to have_content team4.admin.email
    # expect(page).to have_content "[300000, 0]"
  end
end
