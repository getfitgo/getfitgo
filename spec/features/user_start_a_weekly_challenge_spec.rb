require 'spec_helper'

feature "User starts a weekly challenge" do
  before do
    [2500, 5000, 7500, 10000, 12500, 15000].each { |badge|
      file = File.open("public/#{badge.to_s}.png")
      badge = Badge.new(:name => badge.to_s, :display_name => badge.to_s)
      badge.image = file
      file.close
      badge.save
    }
    category = create(:category, name: "Warmup")
    @user = create(:user)

  end

  scenario "After sign up and confirmation" do
    @user.confirm!
    sign_in_as @user
    expect(page).to have_content "7-day warm up"
    expect(@user.current_challenge.challenge.category.name).to eq "Warmup"
  end

  scenario "After sign up and no confirmaiton", js: true do
    sign_in_as @user
    expect(page).not_to have_link "7-day warm up"
  end

  describe "enters data on 8th date", js: true do
    before do
      new_time = Time.local(2008, 9, 1, 12, 0, 0)
      @user.confirm!
      @now = Time.now
      Timecop.travel Time.now + 4.day
      sign_in_as @user
    end

  end

  describe "enters data on each date" do
    before do
      @user.confirm!
      sign_in_as @user
    end
    scenario "enters data on each day" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      @user.reload
      expect(@user.steps_covered).to eq 14000
      expect(@user.current_challenge.challenge.category.name).to eq "30 Day Challenge"
      expect(@user.current_challenge.challenge.number_of_steps).to eq 120000
      expect(@user.last_completed_challenge.total_steps_covered).to eq 14000
    end

    scenario "if the user median is less than 2000 then give him a status 1" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 1999)
      @user.reload
      expect(@user.slab).to eq 1
      expect(@user.current_challenge.challenge.number_of_steps).to eq 120000
    end

    scenario "if the user median is greater than 2000 and less than 3300 then give him a status 2" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2001)
      @user.reload
      expect(@user.slab).to eq 2
      expect(@user.current_challenge.challenge.number_of_steps).to eq 120000
    end

    scenario "if the user median is greater than 3301 and less than 4500 then give him a status 3" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 3301)
      @user.reload
      expect(@user.slab).to eq 3
      expect(@user.current_challenge.challenge.number_of_steps).to eq 150000
    end

    scenario "if the user median is greater than 4501 and less than 5800 then give him a status 4" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 4501)
      @user.reload
      expect(@user.slab).to eq 4
      expect(@user.current_challenge.challenge.number_of_steps).to eq 150000
    end

    scenario "if the user median is greater than 5800 then give him a status 5" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 5801)
      @user.reload
      expect(@user.slab).to eq 5
      expect(@user.current_challenge.challenge.number_of_steps).to eq 240000
    end

    scenario "data entered is less than 4 send update reminder email on the 9th, 10th and the 11th day" do
      Timecop.travel Time.now + 4.day
      update_activity(@now + 1.day, 5801)
      update_activity(@now + 1.day, 5801)
      update_activity(@now + 1.day, 5801)
      expect(@user.current_challenge.category.name).to eq "Warmup"
      expect(@user.current_challenge.number_of_steps).to eq nil
    end

    scenario "enters data for 4 days" do
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 1.day
      update_activity(Time.now, 2000)
      Timecop.travel Time.now + 4.day
      visit dashboard_path
      @user.reload
      expect(@user.steps_covered).to eq 8000
      expect(@user.current_challenge.challenge.category.name).to eq "30 Day Challenge"
      expect(@user.current_challenge.challenge.number_of_steps).to eq 120000
      expect(@user.last_completed_challenge.total_steps_covered).to eq 8000
    end
  end

  context "warm challenge over" do
    before do
      new_time = Time.local(2008, 9, 1, 12, 0, 0)
      @user.confirm!
      @now = Time.now
      Timecop.travel Time.now + 7.day
      sign_in_as @user
    end
  end

  scenario "He does 5000 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 5000
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. Rookie badge"
  end

  scenario "He does 7500 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 7500
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. Debutant badge"
  end

  scenario "He does 10000 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 10000
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. Breakthrough badge"
  end

  scenario "He does 12500 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 12500
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. High flyer badge"
  end

  scenario "He does 15000 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 15000
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. Big wig badge"
  end

  scenario "He does 20000 steps a day first time on getfitgo" do
    challenge = create(:challenge)
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    sign_in_as user
    fill_in "daily_activity_steps_covered", with: 20000
    click_button "Save"
    expect(page).to have_content "Congrats you just won a badge. MasterBlaster badge"
  end
end

def update_activity(date, steps)
  fill_in "daily_activity_start_time", with: date
  fill_in "daily_activity_steps_covered", with: steps
  click_button "Save"
  # find_button("Save").trigger("click")
end
