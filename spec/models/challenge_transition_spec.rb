require 'spec_helper'

describe ChallengeTransition do
  before do
    monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 309480, type:"MonthlyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 56700 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 60480 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 64260 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 68040 + 15000, type:"WeeklyChallenge")

    monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 264120, type:"MonthlyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 45360 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 49140 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 52920 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 56700 + 15000, type:"WeeklyChallenge")

    monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 218760, type:"MonthlyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 34020 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 37800 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 41580 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 45360 + 15000, type:"WeeklyChallenge")

    monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 173400, type:"MonthlyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 22680 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 26460 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 30240 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 34020 + 15000, type:"WeeklyChallenge")

    monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 131820, type:"MonthlyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 15120 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 15120 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 18900 + 15000, type:"WeeklyChallenge")
    monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 22680 + 15000, type:"WeeklyChallenge")
  end
  describe "#assign_next_challenge" do
  #   context 'when the percentage performance is less than 65 of the user' do
  #     it "assigns a challenge lesser in intensity" do
  #       user = create(:user)
  #       challenge = create(:challenge,number_of_steps: 100000)
  #       user.stub(:last_completed_challenge) { challenge }
  #       lesser_challenge = create(:challenge,number_of_steps: 90000)
  #       user.stub(:feedback) { -15 }
  #       user.stub(:profile_challenge) { challenge }
  #       user.stub(:last_activity_performance) { 65 }
  #       challenge_transition = ChallengeTransition.new(user)
  #       challenge_transition.assign_next_challenge
  #       expect(user.current_challenge.challenge.number_of_steps).to eq 90000
  #     end
  #   end
  #   context "when the percentage performance  is between 65 and 85 of the user" do
  #     it "assigns the same challenge again" do
  #       user = create(:user)
  #       challenge = create(:challenge,number_of_steps: 100000)
  #       user.stub(:last_completed_challenge) { challenge }
  #       lesser_challenge = create(:challenge,number_of_steps: 90000)
  #       user.stub(:feedback) { 10 }
  #       user.stub(:profile_challenge) { challenge }
  #       user.stub(:last_activity_performance) { 75 }
  #       challenge_transition = ChallengeTransition.new(user)
  #       challenge_transition.assign_next_challenge
  #       expect(user.current_challenge.challenge.number_of_steps).to eq 100000
  #     end
  #   end

  #   context "when the percentage performance  is between above 85" do
  #     it "assigns the same challenge again" do
  #       user = create(:user)
  #       challenge = create(:challenge,number_of_steps: 100000)
  #       user.stub(:last_completed_challenge) { challenge }
  #       higher_challenge = create(:challenge,number_of_steps: 110000)
  #       user.stub(:feedback) { 10 }
  #       user.stub(:profile_challenge) { challenge }
  #       user.stub(:last_activity_performance) { 95 }
  #       challenge_transition = ChallengeTransition.new(user)
  #       challenge_transition.assign_next_challenge
  #       expect(user.current_challenge.challenge.number_of_steps).to eq 110000
  #     end
  #   end
    context 'Warmup over' do
      it "calculates the next challenge based on what he has done in the warmup and based on his profile assigns him a challenge" do
        user = create(:user)
        user.confirm!
        user.current_challenge.daily_activities.each do |activity|
          activity.update_attributes(steps_covered:7000)
        end
        expect(user.current_challenge.total_steps_covered).to eq 49000
        user.current_challenge.update_attributes(status:"Completed")
        user.stub(:profile_challenge) { Challenge.where(type:"MonthlyChallenge").last }
        ChallengeTransition.new(user).assign_next_challenge({ target_range: { lower:65, upper: 85 }, weighted_average: 1.0/3} )
        expect(user.current_challenge.status).to eq "Progress"
        expect(user.current_challenge.challenge.number_of_days).to eq 28
        expect(user.current_challenge.challenge.number_of_steps).to eq 218760
      end

      describe "if the difference of levels between profile challenge and suggested challenge is more than or equal to 4" do
        it "takes the 2-3rd of the profile challenge and 1-3rd of the suggest challenge if total_steps_covered is 14000 in the warmup period" do
          user = create(:user)
          user.confirm!
          user.current_challenge.daily_activities.each do |activity|
            activity.update_attributes(steps_covered:2000)
          end
          expect(user.current_challenge.total_steps_covered).to eq 14000
          user.current_challenge.update_attributes(status:"Completed")
          user.stub(:profile_challenge) { Challenge.where(type:"MonthlyChallenge").last }
          ChallengeTransition.new(user).assign_next_challenge({ target_range: { lower:65, upper: 85 }, weighted_average: 1.0/3} )
          expect(user.current_challenge.status).to eq "Progress"
          expect(user.current_challenge.challenge.number_of_days).to eq 28
          expect(user.current_challenge.challenge.number_of_steps).to eq Challenge.where(type:"MonthlyChallenge").order(:number_of_steps).first.number_of_steps
        end
      end
    end
  end
end
