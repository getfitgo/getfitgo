require 'spec_helper'

describe "Milestone Badge","#award_badge_to_user" do
   before do
    [[{"days"=>"1", "steps"=>"3000"}, "Starter"], [{"days"=>"1", "steps"=>"5000"}, "Rookie"], [{"days"=>"1", "steps"=>"7500"}, "Debutant"], [{"days"=>"1", "steps"=>"10000"}, "Breakthrough"], [{"days"=>"1", "steps"=>"12500"}, "High Flyer"], [{"days"=>"1", "steps"=>"15000"}, "Big Wig"], [{"days"=>"1", "steps"=>"20000"}, "Master Blaster"]]
  end
  it "should give the right badge to the user" do
    user = create(:user)
    user.confirm!
    Timecop.travel(Time.now + 1.day)
    daily_activity = create(:daily_activity,steps_covered:10000,start_time: Time.now)
    badge_awarded = MilestoneBadge.award_badge_to_user(user,daily_activity)
    expect(badge_awarded).to match_array([])
  end

  it "should give the Milestone badge only the first time" do
    user = create(:user)
    user.confirm!
    user.badges << MilestoneBadge
    daily_activity = create(:daily_activity,steps_covered:10000,start_time: Time.now)
    badge_awarded = MilestoneBadge.award_badge_to_user(user,daily_activity)
    expect(badge_awarded).to match_array([])
  end

  it "user be given Starter badge when he does 3000 steps" do
  end

  it "user be given Rookie badge when he does 5000 steps" do
  end

  it "user be given Debutant badge when he does 7500 steps" do
  end

  it "user be given Breakthrough badge when he does 10000 steps" do
  end

  it "user be given Master Blaster badge when he does 20000 steps" do
  end

  it "user be given Big Wig badge when he does 15000 steps" do
  end

  it "user be given High Flyer badge when he does 12500 steps" do
  end

  it "can be given on any day of the user" do
  end
end
