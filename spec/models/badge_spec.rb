require 'spec_helper'

describe "Badge",".award_badge_to_user" do
  it "returns the badge awarded to the user" do
    user = create(:user)
    user.confirm!
    daily_activity = create(:daily_activity)
    ap daily_activity
    badge_awarded = Badge.award_badge_to_user(user,daily_activity)
    expect(badge_awarded).to match_array([])
  end
end
