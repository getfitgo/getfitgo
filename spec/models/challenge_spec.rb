require 'spec_helper'

describe Challenge do
  it { should respond_to(:name) }
  it { should respond_to(:visibility) }
  it { should belong_to(:category) }
end
