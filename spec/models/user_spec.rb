require 'spec_helper'

describe User do
  before do
    Point.any_instance.stub(:assign_level) 
  end
  it { should have_many(:identities) }
  it { should have_many(:memberships) }
  it { should have_attached_file(:photo) }
  it { should respond_to(:points) }
  it { should respond_to(:badges) }
  it { should respond_to(:gender) }
  it { should respond_to(:organisation) }
  it { should respond_to(:city) }
  it { should respond_to(:birth_date) }
  it { should respond_to(:score) }
  it { should respond_to(:posts) }
  describe "#weekly_challenge_over?" do
    it "returns a true if the challenge is Completed" do
      challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
      user_activity = create(:user_activity, status: "Completed", steps_covered: 12001, challenge: challenge)
      expect(user_activity.user.weekly_challenge_over?).to be_true
    end

    it "returns a false if the challenge is not Completed" do
      challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
      user_activity = create(:user_activity, status: "Progress", steps_covered: 12001, challenge: challenge)
      expect(user_activity.user.weekly_challenge_over?).to be_false
    end
  end

  describe "#award_points(points)" do
    it "adds the points to the user's points" do
      user = create(:user)
      user.award_points(50,"You just won 50 points")
      expect(user.score).to eq 50
      expect(user.activities.first.name).to eq "You just won 50 points"
    end
  end

  describe "#last_completed_challenge" do
    it "return the last completed challenge" do
      user = create(:user)
      challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
      user_activity = create(:user_activity, status: "Completed", steps_covered: 12001, challenge: challenge, user: user)
      expect(user.last_completed_challenge).to eq user_activity
    end
  end

  describe "#male?" do
    it "returns true if the user is a mail" do
      user = create(:user,gender:"male")
      expect(user.male?).to be_true
    end

    it "returns true if the user is a mail" do
      user = create(:user,gender:"female")
      expect(user.male?).to be_false
    end
  end

  describe "#samosas_burnt" do
    it "returns the samosas burnt due to user's activity" do
      User.any_instance.stub(:current_steps) { 10000 }
      user = create(:user,weight:100)
      expect(user.samosas_burnt).to eq 1
    end

    it "returns the samosas burnt due to user's activity" do
      User.any_instance.stub(:current_steps) { 10000 }
      user = create(:user)
      expect(user.samosas_burnt).to eq 1
    end
  end

  describe "#times_full_marathon" do
    it "returns the full marathons the user has done based on his activity" do
      User.any_instance.stub(:current_steps) { 10000 }
      user = create(:user)
      expect(user.times_full_marathon).to eq 0.2
    end
  end

  describe "#update_fitness" do
    before do
      Fitness.create(name:"SUPER FIT",point:10,number_of_stars:5)
      Fitness.create(name:"GOING STRONG",point:7,number_of_stars:4)
      Fitness.create(name:"FIT",point:4,number_of_stars:3)
      Fitness.create(name:"GOING THERE",point:2,number_of_stars:2)
      Fitness.create(name:"LOTS TO WORK ON",point:0,number_of_stars:1)
    end
    it "updates the fitness level of the user based on his current score" do
      user = create(:user)
      user.update_fitness(11)
      expect(user.fitness_level.fitness.name).to eq "SUPER FIT"
      expect(user.fitness_level.fitness.number_of_stars).to eq 5
    end
    it "updates the fitness level of the user based on his current score" do
      user = create(:user)
      user.update_fitness(8)
      expect(user.fitness_level.fitness.name).to eq "GOING STRONG"
      expect(user.fitness_level.fitness.number_of_stars).to eq 4
    end
    it "updates the fitness level of the user based on his current score" do
      user = create(:user)
      user.update_fitness(5)
      expect(user.fitness_level.fitness.name).to eq "FIT"
      expect(user.fitness_level.fitness.number_of_stars).to eq 3
    end

     it "updates the fitness level of the user based on his current score" do
      user = create(:user)
      user.update_fitness(2)
      expect(user.fitness_level.fitness.name).to eq "GOING THERE"
      expect(user.fitness_level.fitness.number_of_stars).to eq 2
    end

     it "updates the fitness level of the user based on his current score" do
      user = create(:user)
      user.update_fitness(0)
      expect(user.fitness_level.fitness.name).to eq "LOTS TO WORK ON"
      expect(user.fitness_level.fitness.number_of_stars).to eq 1
    end
  end

   describe "post associations" do

    before { @user = create(:user) }
    let!(:older_micropost) do
      FactoryGirl.create(:micropost, user: @user, created_at: 1.day.ago)
    end
    let!(:newer_micropost) do
      FactoryGirl.create(:micropost, user: @user, created_at: 1.hour.ago)
    end

    it "should have the right microposts in the right order" do
      expect(@user.microposts.recent_first.to_a).to eq [newer_micropost, older_micropost]
    end
  end

  it "should destroy associated posts" do
      posts = @user.posts.to_a
      @user.destroy
      expect(posts).not_to be_empty
      posts.each do |post|
        expect(Post.where(id:post.id)).to be_empty
      end
    end

 
end
