require 'spec_helper'

describe "Streak Badge","#award_badge_to_user" do
  it "should give the right badge to the user" do
    user = create(:user)
    user.confirm!
    daily_activity = create(:daily_activity)
    badge_awarded = StreakBadge.award_badge_to_user(user,daily_activity)
    expect(badge_awarded).to match_array([])
  end
end
