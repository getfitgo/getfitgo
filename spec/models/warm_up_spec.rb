require 'spec_helper'
describe Warmup do
  context ".find_slab_and_next_challenge" do
    it "finds the slab and the next challenge for the user once the warmup is over and the user has done between 0 and 2000 steps" do
      median = 1500
      challenge, slab = Warmup.find_slab_and_next_challenge(median)
      expect(challenge.number_of_steps).to eq 80000
      expect(slab).to eq 1
    end

    it "finds the slab and the next challenge for the user once the warmup is over and the user has done between 2001 and 3300 steps" do
      median = 2001
      challenge,slab = Warmup.find_slab_and_next_challenge(median)
      expect(challenge.number_of_steps).to eq 90000
      expect(slab).to eq 2
    end

    it "finds the slab and the next challenge for the user once the warmup is over and the user has done between 2001 and 3300 steps" do
      median = 3301
      challenge,slab = Warmup.find_slab_and_next_challenge(median)
      expect(challenge.number_of_steps).to eq 120000
      expect(slab).to eq 3
    end

    it "finds the slab and the next challenge for the user once the warmup is over and the user has done between 2001 and 3300 steps" do
      median = 4501
      challenge,slab = Warmup.find_slab_and_next_challenge(median)
      expect(challenge.number_of_steps).to eq 150000
      expect(slab).to eq 4
    end

    it "finds the slab and the next challenge for the user once the warmup is over and the user has done between 2001 and 3300 steps" do
      median = 5801
      challenge,slab = Warmup.find_slab_and_next_challenge(median)
      expect(challenge.number_of_steps).to eq 180000
      expect(slab).to eq 5
    end
  end

  context ".calculate_next_challenge" do
    it "creates the next challenge for the user" do
      user = create(:user_activity_with_daily_activity_with_steps)
      Warmup.calculate_next_challenge(3001,user)
      expect(user.user_activities.count).to eq 2
      expect(user.user_activities.last.status).to eq "Progress"
      expect(user.user_activities.last.challenge.number_of_days).to eq 30
    end
  end
  context "#send_notifications" do
    it "sends notification to each user who are currently doing the warm and have entered 0 data yet" do
      Timecop.freeze(Time.now)
      user = create(:user)
      user.confirm!
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
    end
    it "sends notification to each user who are currently doing the warm and have entered 3 data yet" do
      Timecop.freeze(Time.now)
      user = create(:user)
      user.confirm!
      daily_activities = user.current_challenge.daily_activities.order("start_time")
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities.first.update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities[1].update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities[2].update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
    end

    it "sends notification to each user who are currently doing the warm and have entered 4 data yet" do
      Timecop.freeze(Time.now)
      user = create(:user)
      user.confirm!
      daily_activities = user.current_challenge.daily_activities.order("start_time")
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities.first.update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities[1].update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities[2].update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      daily_activities[3].update_attributes(steps_covered: 10000)
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 1
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
      Timecop.travel(Time.now + 1.day)
      expect { Warmup.send_notifications }.to change { ActionMailer::Base.deliveries.count }.by 0
    end
  end
end
