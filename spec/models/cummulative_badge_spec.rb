require 'spec_helper'

describe "Cummulative Badge","#award_badge_to_user" do
  before do
    [["200k", {"total_steps"=>"200000"}, 100], ["500k", {"total_steps"=>"500000"}, 150], ["1Million", {"total_steps"=>"1000000"}, 200], ["100k", {"total_steps"=>"100000"}, 50]]
  end
  it "should give the right badge to the user" do
    user = create(:user)
    user.confirm!
    daily_activity = create(:daily_activity)
    badge_awarded = CummulativeBadge.award_badge_to_user(user,daily_activity)
    expect(badge_awarded).to match_array([])
  end

  it "should be given 100k badge and 50 points" do
  end

  it "should be given 500k badge and 150 points" do
  end

  it "should be given 1Million badge and 200 points" do
  end

  it "should be given 200k badge and 100 points" do
  end

  it "should not be given the cummulative badge once awarded" do
  end
end
