require 'spec_helper'

describe Achievement do
  it { should belong_to(:user) }
  it { should belong_to(:badge) }
end
