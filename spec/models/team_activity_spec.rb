require 'spec_helper'

describe TeamActivity do
  it { should belong_to(:team) }
  it { should belong_to(:challenge) }
  it { should respond_to(:start_time) }
  it { should validate_uniqueness_of(:team_id).scoped_to(:challenge_id) }
  it { should respond_to(:goal) }
  it { should respond_to(:progress) }
  it { should validate_presence_of(:team) }
  it { should validate_presence_of(:challenge) }
  it { should respond_to(:start_challenge) }
  it 'should respond to ::with_rankings' do
    TeamActivity.should respond_to(:with_rankings)
  end
  before do
    redis_instance = MockRedis.new
    Redis.stub(:new).and_return(redis_instance)
    Redis::Store.stub(:new).and_return(redis_instance)
  end
  describe "#goal" do
    context "before the challenge was choosen" do
      it "returns the team's goals" do
        new_time = Time.local(2008, 9, 1, 12, 0, 0)
        Timecop.freeze(new_time)
        UserActivity.any_instance.stub(:start_time).and_return(Time.now)
        team = create(:team)
        challenge = create(:challenge)
        users = create_list(:user, 10)
        team.members << users
        team_activity = create(:team_activity, team: team, challenge: challenge)
        team_activity.start_challenge
        team_goal = team_activity.goal
        expect(team_goal).to eq 1500000
      end
    end
  end

  describe "#progess" do
    it "returns the total progress of the team" do
      new_time = Time.local(2008, 9, 1, 12, 0, 0)
      Timecop.freeze(new_time)
      UserActivity.any_instance.stub(:start_time).and_return(Time.now)
      team = create(:team)
      challenge = create(:challenge)
      users = create_list(:user, 10)
      team.members << users
      team_activity = create(:team_activity, team: team, challenge: challenge)
      team_activity.start_challenge
      progress = team_activity.progress
      expect(progress).to eq [300000, 0]
    end
  end
end
