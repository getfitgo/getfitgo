require 'spec_helper'

describe UserFeedback do
	describe "#feedback" do
    it "returns 0 when nothing has been entered" do
     user = create(:user)
      user.confirm!
      user.daily_activities.each do |daily_activity|
      	daily_activity.update_attributes(:steps_covered => 10000)
      end
      user.current_challenge.update_attributes(status:"Completed")
      expect(UserFeedback.new(user).feedback).to eq 0
    end

    it "returns 0 when feedback has been entered only for 3 or more daily activities" do
    	user = create(:user)
      user.confirm!
      user.daily_activities.each do |daily_activity|
      	daily_activity.update_attributes(:steps_covered => 10000)
      end
      user.current_challenge.update_attributes(status:"Completed")
      user.daily_activities.first.update_attributes(feel:"Awesome")
      expect(UserFeedback.new(user).feedback({ Awesome: 10, Relaxed: 0, Tired: -15, number_of_entries: 5, limit: 0.8} )).to eq 0
    end

    it "does returns a proper score only when the number of entries of feel are more than or equal to 5" do
    	user = create(:user)
      user.confirm!
      user.daily_activities.each do |daily_activity|
      	daily_activity.update_attributes(:steps_covered => 10000)
      end
      user.current_challenge.update_attributes(status:"Completed")
      user.daily_activities.first.update_attributes(feel:"Awesome")
      user.daily_activities[1].update_attributes(feel:"Awesome")
      user.daily_activities[2].update_attributes(feel:"Awesome")
      user.daily_activities[3].update_attributes(feel:"Awesome")
      user.daily_activities[4].update_attributes(feel:"Awesome")
        expect(UserFeedback.new(user).feedback({ Awesome: 10, Relaxed: 0, Tired: -15, number_of_entries: 5, limit: 0.8} )).to eq 0
    end

    it "returns the number of values of the highest felt feeling of the user" do
    	user = create(:user)
      user.confirm!
      user.daily_activities.each do |daily_activity|
      	daily_activity.update_attributes(:steps_covered => 10000)
      end
      user.current_challenge.update_attributes(status:"Completed")
      user.daily_activities.first.update_attributes(feel:"Awesome")
      user.daily_activities[1].update_attributes(feel:"Tired")
      user.daily_activities[2].update_attributes(feel:"Awesome")
      user.daily_activities[3].update_attributes(feel:"Awesome")
      user.daily_activities[4].update_attributes(feel:"Awesome")
      user.daily_activities[5].update_attributes(feel:"Awesome")
      user.daily_activities[6].update_attributes(feel:"Awesome")
      expect(UserFeedback.new(user).feedback({ Awesome: 10, Relaxed: 0, Tired: -15, number_of_entries: 5, limit: 0.8} )).to eq 10
    end
	end
end