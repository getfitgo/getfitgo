require 'spec_helper'

describe Team do
  it { should have_many(:members) }
  it { should have_many(:memberships) }
  it { should belong_to(:admin) }
  it { should validate_presence_of(:name) }
  it { should have_many(:team_activities) }
  it { should have_many(:challenges) }
  it { should respond_to(:slogan) }
  it { should have_attached_file(:photo) }
  it { should respond_to(:users_invited) }
  it { should respond_to(:members_joined) }
  it { should respond_to(:user_invitations_pending) }

  describe "#users_invited" do
    it "returns the users who I have invited as an admin" do
      team = create(:team)
      users = create_list(:user, 1)
      users.each do |user|
        user.invited_to_team_id = team.id
        user.save
      end
      expect(team.users_invited).to eq users
    end
  end
  describe "#members_joined" do
    it "returns the users who have joined the team" do
      team = create(:team)
      users = create_list(:user, 10)
      team.members << users
      expect(team.members_joined).to eq ([users, team.admin].flatten)
    end
  end
  describe "#user_invitations_pending" do
    it "returns the number of users who have not yet accepted the invitation" do
      team = create(:team)
      users = create_list(:user, 10)
      users.each do |user|
        user.invited_to_team_id = team.id
        user.save
      end
      expect(team.user_invitations_pending).to eq 10
    end
  end
end
