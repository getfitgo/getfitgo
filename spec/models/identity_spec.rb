require 'spec_helper'

describe Identity do
  it { should belong_to(:user) }
  it { should respond_to(:provider) }
  it { should respond_to(:uid) }
end
