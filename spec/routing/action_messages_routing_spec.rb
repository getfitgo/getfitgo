require "spec_helper"

describe ActionMessagesController do
  describe "routing" do

    it "routes to #index" do
      get("/action_messages").should route_to("action_messages#index")
    end

    it "routes to #new" do
      get("/action_messages/new").should route_to("action_messages#new")
    end

    it "routes to #show" do
      get("/action_messages/1").should route_to("action_messages#show", :id => "1")
    end

    it "routes to #edit" do
      get("/action_messages/1/edit").should route_to("action_messages#edit", :id => "1")
    end

    it "routes to #create" do
      post("/action_messages").should route_to("action_messages#create")
    end

    it "routes to #update" do
      put("/action_messages/1").should route_to("action_messages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/action_messages/1").should route_to("action_messages#destroy", :id => "1")
    end

  end
end
