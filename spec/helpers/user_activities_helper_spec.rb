require 'spec_helper'

describe UserActivitiesHelper do
  describe ".show_two_challenges_to_choose_from" do
    it "returns the next 2 challenges for the user after he has finished the weekly challenge with 13000" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = create(:user)
      user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the confirmable module
      sign_in_as user
      challenge = create(:challenge, number_of_steps: 13000, number_of_days: 7)
      user_activity = create(:user_activity, user: user, status: "Completed", steps_covered: 12001, challenge: challenge)
      user_activities = show_next_challenge(user)
      expect(user_activities.count).to eq(1)
      expect(user_activities.first.goal).to eq(300000)
    end

    describe ".get_target_for_user" do
      it "returns [400000,500000] for 13000 input" do
        expect(get_target_for_user(13000)).to eq [400000, 500000]
      end

      it "returns [300000,400000] for 12001 input" do
        expect(get_target_for_user(12001)).to eq [300000, 400000]
      end

      it "returns [100000,200000] for 4000 input" do
        expect(get_target_for_user(4000)).to eq [100000, 200000]
      end

      it "returns [200000,300000] for 7000 input" do
        expect(get_target_for_user(7000)).to eq [200000, 300000]
      end

      it "returns [270000,300000] for 9000 input" do
        expect(get_target_for_user(9000)).to eq [270000, 300000]
      end
    end
  end
end
