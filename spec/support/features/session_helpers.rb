# spec/support/features/session_helpers.rb
module Features
  module SessionHelpers
    def sign_up_with(email, username, password)
      visit new_user_registration_path
      fill_in 'Email', with: email
      fill_in 'user_user_name', with: username
      fill_in 'Password', with: password
      check('tnc')
      click_button 'create_account'
      User.find_by_email(email).confirm!
      User
    end

    def sign_up_but_no_confirmation(email, username, password)
      visit new_user_registration_path
      fill_in 'Email', with: email
      fill_in 'user_user_name', with: username
      fill_in 'Password', with: password
      check('tnc')
      click_button 'Create Account'
    end

    def sign_in_as_dummy_user
      user = create(:user)
      user.confirm!
      warm_up_user user
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Sign in'
    end

    def sign_in_as(user)
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Sign in'
    end

    def warm_up_user(user)
      challenge = Challenge.create(name: "7-day warm up", category_id: 1, number_of_days: 7)
      UserActivity.create(user: user, challenge: challenge, start_time: Time.now)
    end
  end
end
