# require 'spec_helper'

# describe TeamActivitiesController do
#   describe "#create" do
#     it "should create a new team activity" do
#       team = create(:team)
#       challenge = create(:challenge)
#       post :create, {team_id: team, challenge_id: challenge}
#       expect(assigns(:team_activity)).to eq(TeamActivity.find_by(team: team, challenge: challenge))
#     end
#   end

#   describe "#update" do
#     it "should start the activity for the team members" do
#       new_time = Time.local(2008, 9, 1, 12, 0, 0)
#       Timecop.freeze(new_time)
#       team_activity = create(:team_activity)
#       put :update, {id: team_activity, team_activity: {start_time: Time.now}}
#       team_activity.reload
#       expect(team_activity.start_time.day).to eq Time.now.day
#       expect(UserActivity.count).to eq team_activity.team.members.count
#     end
#   end
# end
