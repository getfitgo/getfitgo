require 'spec_helper'

describe FriendshipsController do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end
  context "Add friend" do
    it "sends a friend request to the user" do
      user = create(:user)
      friend = create(:user)
      friend.confirm!
      user.confirm!
      sign_in user
      xhr :post, :create,friend_id: friend.id
      expect(Friendship.find_by(user:user,friend:friend).confirmed?).to be_false
    end
  end

  context "Accept friend" do
    it "Accepts the friend request and makes both the users friends" do
      user = create(:user)
      friend = create(:user)
      friend.confirm!
      user.confirm!
      sign_in user
      friendship = Friendship.create(user:user,friend:friend)
      expect(user.friends).not_to include(friend)
      xhr :get, :edit,id: friendship.id
      friendship.reload
      user.reload
      expect(friendship.confirmed?).to be_true
      expect(user.friends).to include(friend)
    end
  end

   context "Unfriend" do
    it "Rejects the friend request" do
      user = create(:user)
      friend = create(:user)
      friend.confirm!
      user.confirm!
      sign_in user
      friendship = Friendship.create(user:user,friend:friend)
      xhr :get, :edit,id: friendship.id
      xhr :post, :destroy,id: friendship.id
      user.reload
      expect(friendship.confirmed?).to be_false
      expect(user.friends).not_to include(friend)
    end
  end
end
