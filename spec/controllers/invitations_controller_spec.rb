require 'spec_helper'

describe InvitationsController do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end
  describe "#batch_invite" do
    context "User Confirmed" do
      it "Sends an invitation to all the users invited" do
        user = create(:user)
        user.confirm!
        sign_in user
        expect { post :batch_invite,user_emails:"a@a.com,b@b.com,c@c.com, d@d.com,e@e.com" }.to change { ActionMailer::Base.deliveries.count }.by 5
        response.should be_redirect
        expect(flash.notice).to eq "Your friend(s) have been invited to join you in your wellness journey on getfitgo."
      end

      it "Does not send invitation to 6th friend if asked to" do
        user = create(:user)
        user.confirm!
        sign_in user
        User.invite!({:email => "a@a.com"}, user)
        User.invite!({email:"b@a.com"},user)
        User.invite!({email:"c@a.com"},user)
        User.invite!({email:"d@a.com"},user)
        User.invite!({email:"e@a.com"},user)
        expect { post :batch_invite,user_emails:"f@f.com" }.to change { ActionMailer::Base.deliveries.count }.by 0
        expect(flash.notice).to eq "You have already invited 5 friends to join you in your wellness journey on getfitgo."
        response.should be_redirect
      end
    end

    context "Unconfirmed User" do
      it "Does not send invitations if the user is unconfirmed" do
        user = create(:user)
        sign_in user
        expect { post :batch_invite,user_emails:"a@a.com" }.to change { ActionMailer::Base.deliveries.count }.by 0
        expect(flash.alert).to eq "Please confirm account first!"
        response.should be_redirect
      end
    end
  end

  describe "#new" do
    it "tells the user that he can invite only a max of 5 users" do
      user = create(:user)
      user.confirm!
      sign_in user
      get :new
      expect(flash.notice).to eq "You can invite 5 friends to join you in your wellness journey on getfitgo."
      response.should be_success
    end

    it "tells the user that he can invite remaining invitations left" do
      user = create(:user)
      user.confirm!
      sign_in user
      User.invite!({:email => "a@a.com"}, user)
      get :new
      expect(flash.notice).to eq "You can invite 4 friends to join you in your wellness journey on getfitgo."
      response.should be_success
    end

    it "tells the user that he has already finished his limit" do
      user = create(:user)
      user.confirm!
      sign_in user
      User.invite!({:email => "a@a.com"}, user)
      User.invite!({email:"b@a.com"},user)
      User.invite!({email:"c@a.com"},user)
      User.invite!({email:"d@a.com"},user)
      User.invite!({email:"e@a.com"},user)
      expect { post :batch_invite,user_emails:"f@f.com" }.to change { ActionMailer::Base.deliveries.count }.by 0
      expect(flash.notice).to eq "You have already invited 5 friends to join you in your wellness journey on getfitgo."
      response.should be_redirect
    end
  end

  describe "#update" do
  	it "Awards 50 points to the user if his invited user joins the community" do
  		inviter = create(:user)
  		inviter.confirm!
  		invitee_email = "a@a.com"
  		User.invite!(:email => invitee_email) do |u|
        u.invited_by = inviter
      end
      @invitee = User.where(:email => invitee_email).first
      Point.any_instance.stub(:assign_level) 
      link = ActionMailer::Base.deliveries.last.body.raw_source.match(/href="(?<url>.+?)">/)[:url]
      expect { post :update,user:{:invitation_token => link.split("=")[1],password:"aaaaaaaa",user_name:"a",password_confirmation:"aaaaaaaa"} }.to change  { inviter.score }.by 50
  	  expect(inviter.activities.last.name).to eq "Your friend a has joined getfitgo. Congratulations! You’ve earned 50 points."
  	end
  end
end
