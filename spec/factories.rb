FactoryGirl.define do
  factory :user do
    sequence(:user_name) { |n| Faker::Name.name }
    sequence(:email) { |n| "person#{n}@example.com" }
    password "12345678"
    password_confirmation "12345678"
  end

  factory :post do
    content "Lorem ipsum"
    user
  end

  factory :challenge do
    name "Walking Challenge"
    number_of_steps 150000
    number_of_days 30
    visibility "Public"
    category
  end

  factory :membership do
    user
    team
  end

  factory :badge do
    name "Badge"
  end

  factory :category do
    name "Warmup"
  end

  factory :user_activity do
    start_time { (Time.now) }
    user
    challenge
    status "Participating"
    steps_covered "12000"
    factory :user_activity_with_daily_activity do

      after(:create) do |user_activity, evaluator|
        create_list(:daily_activity, user_activity.challenge.number_of_days, steps_covered: nil, user_activity: user_activity)
      end
    end

    factory :user_activity_with_daily_activity_with_steps do

      after(:create) do |user_activity, evaluator|
        create_list(:daily_activity, user_activity.challenge.number_of_days, user_activity: user_activity)
      end
    end
  end

  factory :daily_activity do
    sequence(:steps_covered, 0) { |n| n*1000 }
    sequence(:start_time, 0) { |n| Time.now + n.day }
  end

  factory :team do
    name "Getfitgo"
    association :admin, factory: :user
    factory :team_with_members do

      after(:create) do |team, evaluator|
        create_list(:membership, 5, team: team)
      end
    end
  end

  factory :team_activity do
    start_time { (Time.now + 1.days) }
    team
    challenge
    after(:create) do |team_activity|
      team_activity.team.members.each do |member|
        create(:user_activity_with_daily_activity, user: member, challenge: team_activity.challenge, start_time: team_activity.start_time)
      end
    end
  end
end
