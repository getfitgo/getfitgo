// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery.ui.datepicker
//= require jquery.ui.effect-highlight
//= require bootstrap-datepicker
//= require bootstrap
//= require turbolinks
//= require sugar
//= require local_time
//= require jquery_nested_form
//= require raphael
//= require morris
//= require turbolinks
//= require jquery.jcarousel.min
//= require lightbox.min
//= require spin.min
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require_tree .
