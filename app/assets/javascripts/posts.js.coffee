console.log(gon.count)
jQuery ->
  url = document.location.toString()

  if url.match('dashboard')
    $('#user_photo').fileupload
      dataType: "script"
      add: (e, data) ->
        $("#photo_spinner").show()
        types = /(\.|\/)(gif|jpe?g|png)$/i
        file = data.files[0]
        if types.test(file.type) or types.test(file.name)
          data.context = $(tmpl("template-upload", file))
          $("#new_painting").append data.context
          data.submit()
        else
          alert "" + file.name + " is not a gif, jpeg, or png image file"

      progress: (e, data) ->
        progress = undefined
        if data.context
          progress = parseInt(data.loaded / data.total * 100, 10)
          data.context.find(".bar").css "width", progress + "%"

