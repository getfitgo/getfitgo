var blah;
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

function toggleSubmit(value) {
  var element = document.getElementById('create_account');
  if (value)
    element.disabled = false;
  else
    element.disabled = true;
}

function connect_device(count){
  if(count > 0){
    alert("Please unlink the previous device first")
    return false
  }
  true
}


function togglePassword(value) {
  var element = document.getElementById('user_password');
  if (value)
    element.type = 'text';
  else
    element.type = 'password';
}

function nextTab(elem) {
  $(elem + ' li.active')
      .next()
      .find('a[data-toggle="tab"]')
      .click();
}

$(window).scroll(function () {
  if (document.URL == "http://" + window.location.host + '/registrations/welcome_page') {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $('#scroll_down')[0].style.display = 'none';
    }
    else
      $('#scroll_down')[0].style.display = '';
  }
});

function scrollDown() {
//  window.scrollTo(0,document.body.scrollHeight);
  $('html, body, .content').animate({scrollTop: $('#welcome_page').offset().top}, 500);

  return false;
}

function checkTnC() {
  if (!$('#tnc')[0].checked) {
    alert('You need to read and accept the User Agreement and Terms and Conditions to start using the site.')
  }
}

if (document.URL == "http://" + window.location.host + '/dashboard') {
  function resizeHandler() {
    $("#steps_covered_chart").html('');
    $("#calories_chart").html('');
    drawChart('steps_covered', 'Steps Covered', '#00c8c8');
    drawChart('calories', 'Calories Burnt', '#fa9600');
  }

  if(window.addEventListener) {
    window.addEventListener('resize', resizeHandler, false);
  }
  else if(window.attachEvent) {
    window.attachEvent('onresize', resizeHandler);
  }
}

$(document).ready(function() {

  // checkScroll();
  $('#profilePicCarousel').carousel('pause');
  $('#notificationsCarousel').carousel('pause');

  $('.left.carousel-control').hide();
  $('.carousel.slide').on('slid.bs.carousel', function (ev) {
    var carouselData = $(this).data('bs.carousel');
    var currentIndex = carouselData.getActiveIndex();

    if(currentIndex >= 1) {
      $('#' + carouselData.$element[0].id + ' .left.carousel-control').show();
    }
    else {
      $('#' + carouselData.$element[0].id + ' .left.carousel-control').hide();
    }

    if(currentIndex === (carouselData.$items.length-1)) {
      $('#' + carouselData.$element[0].id + ' .right.carousel-control').hide();
      $('#' + carouselData.$element[0].id + ' .left.carousel-control').show();
    }
    else {
      $('#' + carouselData.$element[0].id + ' .right.carousel-control').show();
    }

    if(carouselData.$element[0].id == 'notificationsCarousel') {
      $.ajax({
        url: '/notifications/mark_as_read',
        method: 'PUT',
        data: { id: $('#notificationsCarousel .active').attr('id') },
        type: 'script'
      })
    }
  })

  $("#post_button").on("click",function(){
    $("#post_spinner").show()
  })
  // $("#photo_button").on("click",function(){
  //   $("#photo_spinner").show()
  // })

  var url = document.location.toString();
  if(url.match('users/login')) {
    loginResizeHandler();
    if (window.addEventListener) {
      window.addEventListener('resize', loginResizeHandler, false);
    }
    else if(window.attachEvent) {
      window.attachEvent('onresize', loginResizeHandler);
    }
  }

  if(url.match('users/invitation/accept')) {
    signUpResizeHandler();
    if (window.addEventListener) {
      window.addEventListener('resize', loginResizeHandler, false);
    }
    else if(window.attachEvent) {
      window.attachEvent('onresize', loginResizeHandler);
    }
  }

  if(url.match('registrations/welcome_page')) {
    welcomeResizeHandler();
    if (window.addEventListener) {
      window.addEventListener('resize', welcomeResizeHandler, false);
    }
    else if(window.attachEvent) {
      window.attachEvent('onresize', welcomeResizeHandler);
    }
  }

  if(url.match('/update_profile')) {
    $.get('/questionnaires/attempts/new', function(data) {
    });
    window.scrollTo(0,0);
  }

  function loginResizeHandler() {
    // $('.push-login')[0].style.height = ($(window).height() - $('.widgetBx').height() - 300) + 'px';
    $('#login-first-section')[0].style.minHeight = ($(window).height()-100) + 'px'
    $('.push-middle')[0].style.height = ($(window).height() - $('.widgetBx').height() - 300)/2 + 'px';
  }

  function signUpResizeHandler() {
    $('.push-login')[0].style.height = ($(window).height() - $('.widgetBx').height() - 300) + 'px';
  }

  function welcomeResizeHandler() {
    $('.push-welcome')[0].style.height = ($(window).height()) + 'px';
    $('#push-welcome-page-buttons')[0].style.height = ($(window).height()/2) + 'px';
  }

  $(document).on('click', '.filter-days', function(){
    var number_of_days = $(this).attr('data-days');
    $.ajax({
      url:'/users/'+ $(this).attr('data-user') + '/report',
      method:'GET',
      data:{number_of_days: number_of_days},
      type: 'script',
      complete: function() {
        $('.filter-days').removeClass('active');
        $('*[data-days='+number_of_days+']').addClass('active');
      }
    })
  })

  $(".replace").on({
    mouseenter: function () {
      $(this).html("<a class='btn btn-warning' href='/users/sign_up'>Request an invite!<br/></a>");
    },
    mouseleave: function () {
      $(this).html("<a href='#'>New to getfitgo?<br/><br/></a>");
    }
  })

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if(history.pushState) {
      history.pushState(null, null, e.target.hash);
    } else {
      window.location.hash = e.target.hash; //Polyfill for old browsers
    }
    var url = document.location.toString();
    if(url.match('dashboard') || url.match(new RegExp("users/\\d{1,5}"))) {
      resizeHandler();
    }
  })

  setTimeout(function(){$('#flash_messages').html('');},10000);

  $(document).ajaxSend(function(event, XMLHttpRequest, ajaxOptions) {
    if(!ajaxOptions.url.match('/questionnaires/attempts')) {
      // $('.spinner').show();
    }
  })

  $(document).ajaxStop(function() {
    $('a[data-toggle="tab"]').bind("shown.bs.tab", function(e) {
      if(history.pushState) {
        history.pushState(null, null, e.target.hash);
      } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
      }
      var url = document.location.toString();
      if(url.match('dashboard') || url.match(new RegExp("users/\\d{1,5}"))) {
        resizeHandler();
      }
    });

    if(window.location.hash) {
      $('.nav-tabs').find('a[href="'+window.location.hash+'"]').tab('show');
      window.scrollTo(0,0);
    }
    // setTimeout(function(){$('.spinner').hide();},500);
    setTimeout(function(){$('#flash_messages').html('');$('#welcome-flash-messages').html('');},10000);
  })

  //Matching tabbed content
  var url = document.location.toString();
  if(!url.match('dashboard') && url.match('#')) {
    $('.nav-tabs a[href=#' + url.split('#')[1]+']').tab('show');
  }

  if(url.match('welcome_page')) {
    window.scrollTo(0,0);
    setTimeout(function(){$('#welcomeModal').modal('show');},1000);
    $('#welcomeModal').on('hidden.bs.modal', function (e) {
      setTimeout(function(){$('#videoModal').modal('show');},2000);
    })
  }

  $(document).on('change', '#user_living', function() {
    if($(this).val() == 'Other') {
      $('#user_living_text_field').prop('disabled', false);
      $('#user_living_text_field').show();
    }
    else {
      $('#user_living_text_field').hide();
      $('#user_living_text_field').prop('disabled', true);
    }
  })

  if(['Do nothing', 'Employed', 'Professional', 'Businessman', 'Housewife', 'Student'].indexOf($('#user_living_text_field').val()) == -1) {
    $("#user_living").val('Other');
    $('#user_living_text_field').prop('disabled', false);
    $('#user_living_text_field').show();
  }
  else {
    $("#user_living").val($('#user_living_text_field').val());
    $('#user_living_text_field').hide();
    $('#user_living_text_field').prop('disabled', true);
  }

  $('input[id=daily_activity_start_time][data-behaviour~=datepicker]').datepicker({
    format: "dd/mm/yyyy",
    orientation: "top auto",
    todayHighlight: true,
    startDate: $('input[id=daily_activity_start_time][data-behaviour~=datepicker]').attr('data-min'),
    endDate: $('input[id=daily_activity_start_time][data-behaviour~=datepicker]').attr('data-max'),
    beforeShowDay: function(date) {
      if($('input[id=daily_activity_start_time][data-behaviour~=datepicker]').attr('data-excluded').match($.datepicker.formatDate('yy-mm-dd', date)))
        return {
          enabled: false
        };
      return;
    }
  });

  $('input[id=user_birth_date][data-behaviour~=datepicker]').datepicker({
    format: "dd/mm/yyyy",
    orientation: "top auto",
    // todayHighlight: true,
    startDate: $('input[id=user_birth_date][data-behaviour~=datepicker]').attr('data-min'),
    endDate: $('input[id=user_birth_date][data-behaviour~=datepicker]').attr('data-max')
  });

  $(document).on('input', '#contact_list', function() {
    var invite_button = $('input[type="submit"][value="Send Invites"]');
    if($('#contact_list')[0].value.split(/[\n,]/).length > invite_button.attr('data-invites')) {
      invite_button.addClass('disabled')
    }
    else {
      invite_button.removeClass('disabled');
    }

  })

  $(document).on('click', 'input[type="submit"][value="Save"]', function() {
    var steps_covered = $('#daily_activity_steps_covered');
    var calories = $('#daily_activity_calories');
    var last_week_average = $('input[type="submit"][value="Save"]').attr('data-average')
    if(steps_covered.val() > 15000 || steps_covered.val() < 2000 || steps_covered.val() > 1.5*last_week_average) {
      noty({
        text        : 'Whoa, sometimes keyboards do funny things! Have you confirmed your activity data input?',
        type        : 'confirm',
        killer      : true,
//        dismissQueue: false,
        layout: 'top',
        buttons: [
          {addClass: 'btn btn-info', text: 'Yes', onClick: function ($noty) {

            // this = button element
            // $noty = $noty element

            $noty.close();
            $('.edit_daily_activity').submit();
          }
          },
          {addClass: 'btn btn-warning', text: 'No', onClick: function ($noty) {
            $noty.close();
            noty({force: true, text: 'Please verify your activity data!', type: 'error', layout: 'top', closeWith: ['click', 'hover']});
          }
          }
        ]
      });
      return false;
    }
    else {
      $('.edit_daily_activity').submit();
    }

  })

})

function drawChart(filter, proper_filter, color) {
  new Morris.Line({
    element: filter + '_chart',
    data: $('#' + filter + '_chart').data('report'),
    xkey: 'start_time',
    ykeys: [filter],
    //ymin: 1000,
    labels: [proper_filter],
    xLabels: 'day',
    // goals: [<%#= "10000" %>],
    //goalStrokeWidth: [2],
    //goalLineColors: ["red"],
    smooth: true,
    lineColors: [color],
    hideHover: true,
    gridTextFamily: 'Flexo',
    dateFormat: function (date) {
      d = new Date(date);
      return d.getDate().ordinalize() + ' ' + month[d.getMonth()].slice(0,3) + ', ' + d.getFullYear();
    },
    xLabelFormat: function (date) {
      d = new Date(date);
      return d.getDate().ordinalize() + ' ' + month[d.getMonth()].slice(0,3) + ', ' + d.getFullYear();
    },
    hoverCallback: function (index, options) {
      var row = options.data[index];
      var date = new Date(row.start_time);
      date = date.getDate().ordinalize() + ' ' + month[date.getMonth()].slice(0,3) + ', ' + date.getFullYear();
      var feeling = '';
      if(row.feel) {
        feeling = "Feeling: <img src='/" + row.feel +".png' width='24'/>"
      }
      content = proper_filter + ": " + row[filter] + "<br/>" + feeling
      html_content = "<strong>" + date + "</strong><br/><span style='color: " + color + ";'>" + content + "</span>"
      return html_content;
    },
  });

  if($('#steps_covered_chart').data('report') == null) {
    $('#steps_covered_chart').html("<div style='font-family: Flexo Light; padding-top:70px;text-align: center;'>Click on the Update button to input your daily Steps taken & Calories burnt</div>");
    $('#calories_chart').html("<div style='font-family: Flexo Light; padding-top:70px;text-align: center;'>Click on the Update button to input your daily Steps taken & Calories burnt</div>");
  }
}

function checkInvitesLeft() {
  if ($('input[type="submit"][value="Send Invites"]').hasClass('disabled')) {
    alert('You are exceeding your invitation limit!');
  }
}

function showComments(commentable_class, commentable_id) {
  $('#' + commentable_class + '-' + commentable_id).toggle();
}

$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();
})

function validatemail($email) {
  var regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i;
  return regex.test($email);
}

// $(document).ready( function(){
//   if(validatemail()){
//     $('').addClass('has-success');
//   } else {
//     $('').addClass('has-warning');
//     alert("Please enter a valid email");
//   }
// })

$(window).resize(function () {
  if ($(window).width() < 480) {
    $("#community").insertAfter($("#activity_feed"));
  }
})

$(window).resize(function () {
  if ($(window).width() < 480) {
    if($('#steps_covered_chart[data-report="null"]')) {
        $('#user-report').css("display", "none");
    }
  }
})
$(window).resize(function () {
  if ($(window).width() < 960) {
    $("#leaderboard_training").insertAfter($("#challenge_widget_training"));
  }
})

function valid_password(pass){
  var pass = $('#Form input[type="password"]').val().length;
  if(pass < 8) {
    $('').addClass('has-error has-feedback');
    alert('Please enter a password greater than eight characters');
  } else
  $('').addClass('has-success has-feedback');
}

function pass_change() {
  if (document.getElementById('').value != document.getElementById('').value) {
    document.getElementById('').setCustomValidity('Both passwords must match.');
  }
  else {
    document.getElementById('').setCustomValidity('');
  }
}

function requestInvite() {
  $('#request-invite').toggle();
  $('#request-invite-email').fadeIn();
}

function requestInviteClose(id) {
  $('#' + id).toggle();
  $('#request-invite').fadeIn();
}

// var currentCount = 1;

// function checkScroll() {
//   if (nearBottomOfPage()) {
//     currentCount++;
//     $.get('/activities/view_more?num=' + currentCount, function(data) {
//     });
//   } else {
//     setTimeout("checkScroll()", 250);
//   }
// }

// function nearBottomOfPage() {
//   return scrollDistanceFromBottom() < 150;
// }

// function scrollDistanceFromBottom(argument) {
//   return pageHeight() - (window.pageYOffset + self.innerHeight);
// }

// function pageHeight() {
//   return Math.max(document.body.scrollHeight, document.body.offsetHeight);
// }

function updateNotifications() {
  $.ajax({
    url: '/notifications/unread',
    method: 'GET',
    type: 'script'
  })
  setTimeout(updateNotifications, 60000);
}

function refreshNotifications() {
  $.ajax({
    url: '/notifications/refresh',
    method: 'GET',
    type: 'script'
  })
}