class InviteMembersToJoinTeam
  include Interactor

  def perform
  	team_admin = User.find_by(email:context["team_admin_email"])
    team_members = context["team_member_emails"]
    team_members.each do |member|
      team.members << member
      Mailer.ask_member_to_join_team(member)
    end
  end
end