class InviteFriends
  include Interactor

  def perform
  	emails = format_emails(context[:user_emails])

  	if current_user.has_invitations_left?
      Activity.create(
          subject: current_user,
          name: "#{current_user.user_name} has invited #{emails.count} #{'friend'.pluralize(emails.count)} to join getfitgo.",
          user: current_user
      )
      emails.each do |email|
        resource = User.invite!({:email => email}, current_user)
        user = User.find_by(email:email)
        user.first_invitation_sent_on = Date.today
        user.save(:validate => false)
      end
     context[:message] = "Your friend(s) have been invited to join you in your wellness journey on getfitgo."
    else
     context[:message] = "You have already invited #{User.invitation_limit} friends to join you in your wellness journey on getfitgo."
    end
  end

  def current_user
    User.current
  end

  def format_emails(user_emails)
    if user_emails.kind_of? Array
      return user_emails
    else
      return user_emails.split(',').select { |email| email }
    end
  end
end
