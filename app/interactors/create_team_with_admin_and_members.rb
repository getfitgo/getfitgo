class CreateTeamWithAdminAndMembers
  include Interactor

  def perform
  	team_admin = User.find_by(email:context["team_admin_email"])
    unless team_admin
    	User.invite_team_admin!(context["team_admin_email"], current_user)
    end
    Mailer.send_team_admin_notification(team_admin)
    team_members = context["team_member_emails"]
    team_members.each do |member|
    	team.members << member
    	Mailer.ask_member_to_join_team(member)
    end
  end
end