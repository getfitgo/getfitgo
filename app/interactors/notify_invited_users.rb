class NotifyInvitedUsers
  include Interactor

  def perform
    User.where.not(invitation_created_at: nil).where.not(invited_by_id:nil).each do |user|
	    if user.invitation_accepted_at == nil && (user.first_invitation_sent_on.to_date == (Time.now - 4.day).to_date)
	      user.invite!(User.find(user.invited_by_id))
	    end

	    if user.invitation_accepted_at == nil && (user.first_invitation_sent_on.to_date == (Time.now - 8.day).to_date)
	      user.invite!(User.find(user.invited_by_id))
	    end
    end
  end
end
