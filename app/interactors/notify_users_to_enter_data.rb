class NotifyUsersToEnterData
  include Interactor

  def perform
    User.all.each do |user|
      ap user.email
      if user.confirmed? && !user.identities.find_by(provider:"fitbit")
        daily_activities = user.daily_activities.where.not(steps_covered: nil)
        total_daily_activities = user.daily_activities
        current_activity = user.current_challenge
        if current_activity.last_activity.present? && (Date.today - user.confirmed_at.to_date).to_i > 2
          Mailer.send_notification_ask_steps(user).deliver
        end
        if (Date.today - current_activity.start_time.to_date).to_i < 3 && (Date.today - current_activity.start_time.to_date).to_i > 0
          Mailer.send_notification_ask_steps(user).deliver
        end
        if daily_activities.count < 4
          if total_daily_activities.count == 7 && Time.now.to_date == (current_activity.start_time + 9.day).to_date
            # current_activity.extend_challenge
            Mailer.send_notification_ask_steps(user).deliver
          end

          if total_daily_activities.count == 8 && Time.now.to_date == (current_activity.start_time + 10.day).to_date
            # current_activity.extend_challenge
            Mailer.send_notification_ask_steps(user).deliver
          end

          if total_daily_activities.count == 9 && Time.now.to_date == (current_activity.start_time + 11.day).to_date
            # current_activity.extend_challenge
            Mailer.send_notification_ask_steps(user).deliver
          end
        end
      end
    end
  end
end
