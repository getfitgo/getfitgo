class ExtendChallengeOfUsers
  include Interactor

  def perform
    User.all.each do |user|
      if user.confirmed?
        current_activity = user.current_challenge
        daily_activities = user.daily_activities.where.not(steps_covered: nil)
        total_daily_activities = user.daily_activities
        if !user.profile.nil? &&  days_progressed_in_current_challenge(user) == (user.current_challenge.challenge.number_of_days  + 1)
          median = median(daily_activities, user)
          user.current_challenge.update_attributes(status:'Complete')
          ChallengeTransition.new(user).assign_next_challenge
        end
        return
      end
    end
  end

  def days_progressed_in_current_challenge(user)
    (Time.now.to_date - user.current_challenge.start_time.to_date).to_i
  end

  def check_day_and_data_inputs(current_activity, daily_activities, total_daily_activities)
    if daily_activities.count < 4 && !user.profile.nil?

      # if total_daily_activities.count == 7 && Time.now.to_date == (current_activity.start_time + 9.day).to_date
      #   current_activity.extend_challenge
      # end

      # if total_daily_activities.count == 8 && Time.now.to_date == (current_activity.start_time + 10.day).to_date
      #   current_activity.extend_challenge
      # end

      # if   total_daily_activities.count == 9 && Time.now.to_date == (current_activity.start_time + 11.day).to_date
      #   current_activity.extend_challenge
      # end
    end
  end

  def median(daily_activities, user)
    median = user.user_activities.find_by(status: "Progress").total_steps_covered/daily_activities.count
    median
  end



  def category
    Category.find_or_create_by(name: "30 Day Challenge")
  end

  def put_user_into_slab_and_start_challenge(user)
    daily_activities = user.daily_activities.where.not(steps_covered: nil)
    total_daily_activities = user.daily_activities
    if daily_activities.count == 7 && total_daily_activities.count == 7
      # This calculation is expected to change later on.
      median = user.user_activities.find_by(status: "Progress").total_steps_covered/daily_activities.count
      Warmup.calculate_next_challenge(median, user)
    end
  end
end
