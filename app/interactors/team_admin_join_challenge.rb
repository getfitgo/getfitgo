class TeamAdminJoinChallenge
  include Interactor

  def perform
  	challenge = Challenge.find_by(steps_covered:context["challenge"]["steps_covered"])
  	team = Team.find_by(admin:current_user)
  	team_activity = TeamActivity.create(team:team, challenge:challenge)
  	team.members.each do |member|
  		Mailer.send_challenge_join_notification_to_member(member)
  		Activity.create(
          subject: member,
          name: "Congratulations, Your team admin #{current_user.user_name} has just joined Challenge #{challenge.name}. It starts on #{team_activity.start_time}. All the best!",
          user: member
      )
  	end
  end
end