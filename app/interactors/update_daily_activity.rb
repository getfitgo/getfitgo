class UpdateDailyActivity
  include ActionDispatch::Routing::UrlFor
  include Rails.application.routes.url_helpers
  include Interactor

  def perform
    context["daily_activity"]["steps_covered"] = context["daily_activity"]["steps_covered"].to_i

    best_performance = current_user.best_performance.steps_covered
    daily_activity.update_attributes(steps_covered:context["daily_activity"]["steps_covered"],calories:context["daily_activity"]["calories"],feel:context["daily_activity"]["feel"])
    Notification.best_performance(current_user) if context['daily_activity']['steps_covered'] > best_performance
    Notification.slow_performance(current_user) if current_user.slow_performance?(context["daily_activity"]["start_time"].to_date)

    if !context["provider"]
      badge_awarded = award_user(context)
      context[:badge_awarded] = badge_awarded
      award_points(context)
    else
      award_points(context)
    end
   current_user.set_average_steps_covered
    # daily_activity.user_activity.update_status
   end

  def award_points(context)
    if !context["provider"]
      current_user.award_points(context["daily_activity"]["steps_covered"]/point_conversion, "#{current_user.user_name} earned #{(context["daily_activity"]["steps_covered"]/100.0).to_i} points for completing #{daily_activity.steps_covered} steps on #{daily_activity.start_time.strftime("#{daily_activity.start_time.day.ordinalize} %B, %Y")}")
    else
      current_user.award_points(context["daily_activity"]["steps_covered"]/point_conversion, "#{current_user.user_name} earned #{(context["daily_activity"]["steps_covered"]/100.0).to_i} points for completing #{daily_activity.steps_covered} steps on #{daily_activity.start_time.strftime("#{daily_activity.start_time.day.ordinalize} %B, %Y")} via  #{ActionController::Base.helpers.link_to(context['provider'].capitalize.gsub("-"," "),'http://dev.getfitgo.in/users/edit#devices')}")
    end
    current_user.award_points(10, "#{current_user.user_name} earned 10 points for entering activity data today") if Date.yesterday.eql? daily_activity.start_time
  end

  def point_conversion
      if current_user.current_challenge.challenge.is_a?(MonthlyChallenge) && current_user.current_steps > current_user.goal
        return 120.0
      else
        return 100.0
      end
  end

  def award_user(context)

    # unless current_user.profile.nil?
    #   ChallengeTransition.new(current_user).assign_next_challenge
    # end
    # Warmup.put_user_into_slab_and_find_challenge(current_user)
    badge_awarded = Badge.award_badge_to_user(current_user, daily_activity)
    badge_awarded
  end

  def award_points_to_user_if_activity_completed(user_activity)
    if user_activity.completed?
      if user_activity.total_steps_covered >= (user_activity.number_of_steps + 100000)
        points = award_points_to_user(user_activity)
        return "Congrats for finishing the challenge. You just got #{points} points."
      end
      if user_activity.total_steps_covered < (user_activity.number_of_steps + 100000)
        points = award_points_to_user(user_activity)
        return "Congrats for finishing the challenge. You just got #{points} points."
      end
    end
  end

  def daily_activity
    @daily_activity = current_user.daily_activities.find_by_start_time(context["daily_activity"]["start_time"].to_date) || current_activity.daily_activities.find_by_start_time(context["daily_activity"]["start_time"].to_date)
  end

  def current_activity
    current_user.user_activities.find_by(status: "Progress")
  end

  def current_user
    User.current || User.find(context["user_id"])
  end

  def current_activity
    current_user.user_activities.find_by(status: "Progress")
  end
end
