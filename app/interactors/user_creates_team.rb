class UserCreatesTeam
	include Interactor

  def perform
  	@team = Team.create(team_params)
    @team.admin = current_user
    @team.save
    if params[:challenge_id]
      TeamActivity.create(team: @team, challenge: Challenge.find(params[:challenge_id]))
    end
    flash.notice = "Team #{@team.name} created."
    redirect_to @team
  end
end