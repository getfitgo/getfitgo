class InvitationWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"
  # sidekiq_options retry: false

  def perform(hash)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key = ENV['TWITTER_KEY']
      config.consumer_secret = ENV['TWITTER_SECRET']
      config.access_token = hash["credentials"]["token"]
      config.access_token_secret = hash["credentials"]["secret"]
    end
    # client.update("I'm tweeting with @gem!")
    # followers = client.followers.to_a
    friends = client.friends.to_a
    friends.each do |friend|
      # InvitationWorker.perform_async(client, friend)
      client.create_direct_message(friend, "Hi")
      # p friend.screen_name
    end
    # followers.each do |follower|
    #  puts follower.screen_name
    #  # client.create_direct_message(follower,"Hi")
    #  # client.update("Inviting @#{follower.email_address}! to have a look http://getfitgo.in. Any suggestions are welcome :)")
    # end
  end
end

