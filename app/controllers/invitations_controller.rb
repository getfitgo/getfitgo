
require 'google/api_client'
# require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
class InvitationsController < Devise::InvitationsController
  before_filter :set_current_user
  before_filter :is_user_confirmed?, only: [:create]
  prepend_before_filter :authenticate_inviter!, :only => [:new, :create]
  prepend_before_filter :has_invitations_left?, :only => [:create]
  prepend_before_filter :require_no_authentication, :only => [:edit, :update, :destroy]
  prepend_before_filter :resource_from_invitation_token, :only => [:edit, :destroy]
  helper_method :after_sign_in_path_for

  # GET /resource/invitation/new
  def new
    self.resource = resource_class.new
    respond_to do |format|
      format.html {
        if current_user.has_invitations_left?
          flash.now[:notice] = "You can invite #{current_user.invitation_limit} friends to join you in your wellness journey on getfitgo."
          render :new
        else
          redirect_to dashboard_path, alert: "You have already invited #{User.invitation_limit} friends to join you in your wellness journey on getfitgo."
        end
      }
    end

  end

  def batch_invite
    if current_user.confirmed?
      result = InviteFriends.perform(params)
      redirect_to dashboard_path, notice: result.message
    else
      alert = "Please confirm account first!"
      redirect_to dashboard_path, alert: alert
    end
  end

  # POST /resource/invitation
  def create
    self.resource = invite_resource

    if resource.errors.empty?
      set_flash_message :notice, :send_instructions, :email => self.resource.email if self.resource.invitation_sent_at
      respond_with resource, :location => after_invite_path_for(resource)
    else
      respond_with_navigational(resource) { render :new }
    end
  end

  # GET /resource/invitation/accept?invitation_token=abcdef
  def edit
    resource.invitation_token = params[:invitation_token]
    render :edit
  end

  # PUT /resource/invitation
  def update
    self.resource = accept_resource
    if resource.errors.empty?
      resource.confirm_and_seed!
      invitee = invitee(self.resource)
      @friendship = invitee.friendships.create(friend: self.resource,confirmed: true)
      invitee.award_points(50, "Your friend #{self.resource.user_name} has joined getfitgo. Congratulations! You’ve earned 50 points.")
      Notification.friend_joined(invitee, self.resource)
      Notification.friend_request_accepted(self.resource, invitee)
      Mailer.welcome_mail(self.resource).deliver
      Mailer.friend_request_accepted(@friendship).deliver
      flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
      set_flash_message :notice, flash_message
      sign_in(resource_name, resource)
      respond_with resource, :location => after_accept_path_for(resource)
    else
      respond_with_navigational(resource) { render :edit, params: { invitation_token: params[:user][:invitation_token] } }
    end
  end

  # GET /resource/invitation/remove?invitation_token=abcdef
  def destroy
    resource.destroy
    set_flash_message :notice, :invitation_removed
    redirect_to after_sign_out_path_for(resource_name)
  end

  def team
    self.resource = resource_class.new
    self.resource.invited_to_team_id = params[:id]
  end

  protected

  def invite_resource
    resource_class.invite!(invite_params.merge(invited_to_team_id: params[:invited_to_team_id]), current_inviter)
  end

  def accept_resource
    resource_class.accept_invitation!(update_resource_params)
  end

  def current_inviter
    @current_inviter ||= authenticate_inviter!
  end

  def has_invitations_left?
    unless current_inviter.nil? || current_inviter.has_invitations_left?
      self.resource = resource_class.new
      set_flash_message :alert, :no_invitations_remaining
      respond_with_navigational(resource) { render :new }
    end
  end

  def resource_from_invitation_token
    unless params[:invitation_token] && self.resource = resource_class.find_by_invitation_token(params[:invitation_token], true)
      set_flash_message(:alert, :invitation_token_invalid)
      redirect_to after_sign_out_path_for(resource_name)
    end
  end

  def invite_params
    devise_parameter_sanitizer.sanitize(:invite)
  end

  def update_resource_params
    devise_parameter_sanitizer.sanitize(:accept_invitation)
  end

  def invitee resource
    @invitee ||= User.find(resource.invited_by_id)
  end
end
