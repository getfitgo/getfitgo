class CorporatesController < ApplicationController
	 # before_filter :check
	def new
		@corporate = Corporate.new
	end

	def create
	end

	def show
		corporate = Corporate.find_by(url:params[:id])
		redirect_to corporate_sign_in_path(id: "users",corporate:params[:id]) unless user_signed_in?
	end
end
