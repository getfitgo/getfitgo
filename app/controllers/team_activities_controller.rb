class TeamActivitiesController < ApplicationController
  # before_filter :authenticate_user!
  # before_filter :is_user_confirmed?
  def create
    challenge = Challenge.find(params[:challenge_id])
    team = Team.find(params[:team_id])
    @team_activity = TeamActivity.new(team: team, challenge: challenge)
    if @team_activity.save
      flash.notice = "Challenge joined. Please select when you want to start the challenge from"
      redirect_to [team, challenge]
    else
      flash.notice = "You have already joined this challenge."
    end
  end

  def update
    team_activity = TeamActivity.find(params[:id])
    team_activity.start_time = DateTime.parse(params[:team_activity][:start_time])
    team_activity.start_challenge
    team_activity.save
    members = team_activity.team.members
    members.each do |member|
      UserActivity.create(user: member, challenge: team_activity.challenge, start_time: team_activity.start_time)
    end
    redirect_to dashboard_path
  end
end
