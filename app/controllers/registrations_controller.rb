class RegistrationsController < Devise::RegistrationsController

  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy, :welcome_page, :update_profile]
  #skip_before_filter :require_no_authentication
  before_filter :authenticate_user!

  def update
    height = (params[:height_foot].to_i*12+params[:height_inches].to_i)*2.54
    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_user.id)

    respond_to do |format|
      format.html { html(account_update_params) }
      format.js { js(account_update_params) }
    end
  end

  def js(account_update_params)
    @user = current_user
    @survey =  Survey::Survey.active.first
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @attempt = @survey.attempts.new
      @attempt.participant = current_user
      @attempt.save
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
    if @user.update_attributes(account_update_params)
      if params[:user][:photo].nil? && params[:photo].present?
        @user.update_attributes(photo: File.open("public/Default Profile Pics/#{params[:photo]}"))
      end
      flash[:notice] = "Your #{params[:user][:tab].split('_')[0]} has been updated successfully!" unless @user.previous_changes.empty?
      if [:foot, :inches, :weight].any? { |x| @user.previous_changes.include? (x) }
        @user.bmi_updated_at = Time.now
        @user.save
      end
      render 'devise/shared/profile' if params[:user][:tab].eql? 'profile'
      render 'devise/shared/password' if params[:user][:tab].eql? 'password'
      render 'devise/shared/bmi' if params[:user][:tab].eql? 'BMI_manage_profile'
      render 'devise/shared/bmi_dashboard' if params[:user][:tab].eql? 'BMI_dashboard'
      render 'devise/shared/dob_dashboard' if params[:user][:tab].eql? 'DOB_dashboard'
      render 'devise/shared/photo_dashboard' if params[:user][:tab].eql? 'Photo'
    else
      render 'profile' if params[:user][:tab].eql? 'profile'
      render 'password' if params[:user][:tab].eql? 'password'
      render 'bmi' if params[:user][:tab].eql? 'BMI'
    end
  end

  def html(account_update_params)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end
  def welcome_page

  end

  def edit
    gon.count = current_user.identities.where("provider=? or provider=?","fitbit","moves").count
    @count = gon.count
    super
  end

  def introduction
    current_challenge = current_user.current_challenge
    case current_user.is_confirmed?
    when false
      @current_user_state = 'registration_incomplete'
    when true
      case current_challenge.challenge.not_warm_up?
      when true
        redirect_to challenge_path(current_challenge.challenge.id)
      when false
        case current_challenge.day_of_challenge
        when (1..5)
          @current_user_state = 'warm_up_started'
        when (6..7)
          @current_user_state = 'warm_up_in_progress'
        else
          @current_user_state = 'warm_up_ending'
        end
      end
    end
  end

  def update_profile
  end

  def resend
    current_user.send_confirmation_instructions
    respond_to do |format|
      format.html { redirect_to dashboard_path, notice: 'You will receive an email with instructions about how to confirm your account in a few minutes.' }
    end
  end

  protected

  def after_inactive_sign_up_path_for(resource)
    registrations_welcome_page_path
  end

  def after_sign_up_path_for(resource)
    registrations_welcome_page_path
  end

   def attempt_unanswered_questions
    Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def link_to_remove_field(name, f)
    f.hidden_field(:_destroy) +
    link_to_function(raw(name), "removeField(this)", :id =>"remove-attach")
  end

  def new_survey
    new_questionnaires_survey_path
  end

  def edit_survey(resource)
    edit_questionnaires_survey_path(resource)
  end

  def survey_scope(resource)
    if action_name =~ /new|create/
      questionnaires_surveys_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_survey_path(resource)
    end
  end

  def new_attempt
    new_questionnaires_attempt_path
  end

  def attempt_scope(resource)
    if action_name =~ /new|create/
     questionnaires_attempts_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_attempt_path(resource)
    end
  end

  def link_to_add_field(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object,:child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "addField(this, \"#{association}\", \"#{escape_javascript(fields)}\")",
    :id=>"add-attach",
    :class=>"btn btn-small btn-info")
  end
end
