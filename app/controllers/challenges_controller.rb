class ChallengesController < ApplicationController
  before_filter :authenticate_user!, only: [:show]

  def index
    if params[:team_id]
      @team = Team.find(params[:team_id])
    end
    if signed_in?
      if current_user.challenges.size == 0
        @challenges = Challenge.where(number_of_days: 7)
      else
        @challenges = Challenge.where(number_of_days: 30)
      end
    else
      @challenges = Challenge.all
    end
  end

  def create
    @challenge = Challenge.new(params.require(:challenge).permit(:name))
    @challenge.save
    redirect_to challenges_path
  end

  # TODO: DRY out the surveys
  def show
    @challenge = Challenge.find(params[:id])
    @participating_users = @challenge.participating_users
    leaders_ids = @challenge.user_activities.where(status: 'Progress').select { |ua| ua.updated_daily_activities.count >= 5 }.map(&:user_id)
    @leaders = User.find(leaders_ids).sort_by { |user| -user.current_challenge.average }.first(10)
    @survey =  Survey::Survey.active.first
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @attempt = @survey.attempts.new
      @attempt.participant = current_user
      @attempt.save
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
  end

  def attempt_unanswered_questions
    Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def link_to_remove_field(name, f)
    f.hidden_field(:_destroy) +
    link_to_function(raw(name), "removeField(this)", :id =>"remove-attach")
  end

  def new_survey
    new_questionnaires_survey_path
  end

  def edit_survey(resource)
    edit_questionnaires_survey_path(resource)
  end

  def survey_scope(resource)
    if action_name =~ /new|create/
      questionnaires_surveys_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_survey_path(resource)
    end
  end

  def new_attempt
    new_questionnaires_attempt_path
  end

  def attempt_scope(resource)
    if action_name =~ /new|create/
     questionnaires_attempts_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_attempt_path(resource)
    end
  end

  def link_to_add_field(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object,:child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "addField(this, \"#{association}\", \"#{escape_javascript(fields)}\")",
    :id=>"add-attach",
    :class=>"btn btn-small btn-info")
  end
end
