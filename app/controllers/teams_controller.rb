class TeamsController < ApplicationController
  before_filter :is_user_confirmed?, only: [:create]
  before_filter :authenticate_user!, only: [:show]

  def new
    @team = current_user.teams.build
    if params[:challenge_id]
      @challenge = Challenge.find(params[:challenge_id])
    end
    @survey =  Survey::Survey.active.first
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @attempt = @survey.attempts.new
      @attempt.participant = current_user
      @attempt.save
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
  end

  def create
    @team = Team.create(team_params)
    @team.admin = current_user
    @team.save
    if params[:challenge_id]
      TeamActivity.create(team: @team, challenge: Challenge.find(params[:challenge_id]))
    end
    flash.notice = "Team #{@team.name} created."
    redirect_to @team
  end

  def index
    @teams = Team.all
  end

  def show
    @team = Team.find(params[:id])
    @members = @team.members
    @team_activity = TeamActivity.find_by(team: @team)
    if @team_activity
      challenge = TeamActivity.find_by(team: @team).challenge
      @teams = challenge.teams.includes(:admin, :members)
    end
    @posts = nil
    @activities = nil
    @team.members.each do |member|
      @activities << member.activities
    end
  end

  def team_params
    params.require(:team).permit(:name,:slogan, :photo)
  end

  def request_invite
    flash.notice = "Your request has been sent to the team manager."
    manager = @team.admin
  end

  def attempt_unanswered_questions
    Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def link_to_remove_field(name, f)
    f.hidden_field(:_destroy) +
    link_to_function(raw(name), "removeField(this)", :id =>"remove-attach")
  end

  def new_survey
    new_questionnaires_survey_path
  end

  def edit_survey(resource)
    edit_questionnaires_survey_path(resource)
  end

  def survey_scope(resource)
    if action_name =~ /new|create/
      questionnaires_surveys_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_survey_path(resource)
    end
  end

  def new_attempt
    new_questionnaires_attempt_path
  end

  def attempt_scope(resource)
    if action_name =~ /new|create/
     questionnaires_attempts_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_attempt_path(resource)
    end
  end

  def link_to_add_field(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object,:child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "addField(this, \"#{association}\", \"#{escape_javascript(fields)}\")",
    :id=>"add-attach",
    :class=>"btn btn-small btn-info")
  end
end
