class NotificationsController < ApplicationController
  before_action :set_notification, only: [:mark_as_read]

  respond_to :js

  def mark_as_read
    @notification.mark_as_read(current_user)
    @notification.update_attributes(expires: 1.day.from_now) unless @notification.expires
    @notifications = current_user.new_notifications + current_user.unread_notifications
    @notifications.uniq!
    render :unread
  end

  def unread
    @notifications = current_user.new_notifications + current_user.unread_notifications
    @notifications.uniq!
  end

  def refresh
    @notifications = current_user.new_notifications + current_user.unread_notifications + current_user.unexpired_notifications
    @notifications.uniq!
    unless @notifications.empty?
      @notifications.first.mark_as_read(current_user)
      @notifications.first.update_attributes(expires: 1.day.from_now) unless @notifications.first.expires
    end
  end

  protected

  def mailbox
    @mailbox ||= current_user.mailbox
  end

  def receipts
    current_user.receipts
  end

  def notification
    @mailboxer_notification ||= Mailboxer::Notification
  end

  def set_notification
    @notification = notification.find(params[:id].to_i)
  end
end