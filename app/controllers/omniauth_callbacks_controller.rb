require 'google/api_client'
# require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
class OmniauthCallbacksController < ApplicationController
  def all
    auth = request.env["omniauth.auth"]
    identity.update_attributes({auth_token: auth['credentials']['token']})

    if signed_in?
      if identity.user == current_user
        message = "Already linked that account!"
      else
        identity.user = current_user
        identity.save()
        message =  "Successfully linked that account!"
      end
      redirect_to session[:omniauth_callback_url] + "#social", notice: message
    else
      # TODO: Uncomment below to allow signing up new users from social connect
      # @user = User.from_omniauth(auth)
      # identity.user = @user
      # identity.save
      # if @user.encrypted_password?
      #   sign_in @user
      #   redirect_to registrations_welcome_page_path
      # else
      #   sign_in_and_redirect @user
      # end
      @user = identity.user
      if @user
        sign_in_and_redirect @user
      else
        redirect_to new_user_session_path, alert: 'Please request for an invite before connecting your social account.'
      end
    end
  end

  def google_oauth2
    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
      sign_in_and_redirect @user, :event => :authentication
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def fitbit
    gon.count =  current_user.identities.where("provider=? or provider=?","fitbit","moves").count
    @count = gon.count
    auth = request.env["omniauth.auth"]
    ap auth
    if gon.count == 0
      identity.update_attributes({user: current_user, auth_token: auth['credentials']['token'],token: auth["credentials"]["token"], secret: auth["credentials"]["secret"] })
      @client = Fitgem::Client.new({
        :consumer_key => ENV["FITBIT_KEY"],
        :consumer_secret => ENV["FITBIT_SECRET"],
        :token => identity.token,
        :secret => identity.secret
      })
      # auth_token = @client.authorize(identity.token, identity.secret)
      # identity.update_attributes({token: auth_token.token, secret: auth_token.secret })
      current_user.last_synced_at = current_user.confirmed_at
      current_user.last_fitbit_synced_at = current_user.confirmed_at
      current_user.save
      sync_data(current_user.id)
      message =  "Successfully linked that account!"
    else
      message =  "Please unlink the previous device first"
    end
    redirect_to session[:omniauth_callback_url] + "#devices", notice: message
    # @client = Fitgem::Client.new({
    #   :consumer_key => ENV["FITBIT_KEY"],
    #   :consumer_secret => ENV["FITBIT_SECRET"],
    #   :token => auth["credentials"]["token"],
    #   :secret => auth["credentials"]["secret"]
    # })
    # daily_activities = current_user.current_challenge.daily_activities.where(steps_covered:nil)
    # daily_activities.each do |d|
    #   d.update_attributes(steps_covered: @client.activities_on_date(d.start_time)["summary"]["steps"])
    #   d.update_attributes(calories: @client.activities_on_date(d.start_time)["summary"]["caloriesOut"])
    # end
  end

  def moves
    gon.count =  current_user.identities.where("provider=? or provider=?","fitbit","moves").count
    @count = gon.count
    auth = request.env["omniauth.auth"]
    if gon.count == 0
      current_user.last_synced_at = current_user.confirmed_at
      current_user.last_moves_synced_at = current_user.confirmed_at
      current_user.save
      identity.update_attributes({user: current_user, auth_token: auth['credentials']['token'],token: auth["credentials"]["token"], secret: auth["credentials"]["secret"] })
      message =  "Successfully linked that account!"
      sync_data(current_user.id)
    else
      message =  "Please unlink the previous device first"
    end
    redirect_to session[:omniauth_callback_url] + "#devices", notice: message
  end

  alias_method :facebook, :all
  alias_method :twitter, :all
  alias_method :google_oauth2, :all
  alias_method :linkedin, :all

  def identity
    @identity ||= (Identity.find_with_omniauth(request.env['omniauth.auth']) ||  Identity.create_with_omniauth(request.env['omniauth.auth']))
  end
end
