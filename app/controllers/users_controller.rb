class UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:show]
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @teams_joined_by_me = @user.teams.includes(:admin)
    @get_current_challenge_of_current_user = @user.user_activities.where(status: "Progress").first
    @completed_challenges = UserActivity.where(user: @user, status: "Completed")
    @challenges_to_start = UserActivity.where(user: @user, status: "Start")
    @team_invited_to = Team.find(@user.invited_to_team_id) if @user.invited_to_team_id
    @daily_activities = @user.daily_activities.where("daily_activities.start_time < ?", Time.now.to_date)
    @leaders = User.all
    if @user.mutual_friends.count > 0
      @posts = Post.from_friends_of(@user)
      user_ids = ([@user.id] + @user.friend_ids + @user.inverse_friend_ids).join(',')
      @posts = @posts.where("creator_id IN (#{user_ids})").where("user_id IN (#{user_ids})")
    else
      @posts = @user.posts
    end
    @survey =  Survey::Survey.active.first
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
  end

  def request_invite
    unless User.find_by(email: params[:user][:email])
      user = User.invite!(:email => params[:user][:email], :skip_invitation => true)
      Mailer.request_invite(user).deliver if Rails.env.production?
    end
    respond_to do |format|
      format.html {
        redirect_to user_registration_path, notice: "Thanks. Feels greats to see people wanting to test drive us. We'll ping you when we are ready to get more people to #feelgoodnow"
      }
      format.js {
        render 'request_invite'
      }
    end
  end

  def attempt_unanswered_questions
   Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def link_to_remove_field(name, f)
    f.hidden_field(:_destroy) +
    link_to_function(raw(name), "removeField(this)", :id =>"remove-attach")
  end

  def new_survey
    new_questionnaires_survey_path
  end

  def edit_survey(resource)
    edit_questionnaires_survey_path(resource)
  end

  def survey_scope(resource)
    if action_name =~ /new|create/
      questionnaires_surveys_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_survey_path(resource)
    end
  end

  def new_attempt
    new_questionnaires_attempt_path
  end

  def attempt_scope(resource)
    if action_name =~ /new|create/
     questionnaires_attempts_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_attempt_path(resource)
    end
  end

  def link_to_add_field(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object,:child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "addField(this, \"#{association}\", \"#{escape_javascript(fields)}\")",
    :id=>"add-attach",
    :class=>"btn btn-small btn-info")
  end
end

