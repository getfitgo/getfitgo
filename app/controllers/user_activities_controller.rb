require 'rest-client'
class UserActivitiesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :is_user_confirmed?

  def create
    if params[:team_id]
      @team = Team.find(params[:team_id])
    end
    challenge = Challenge.find(params[:challenge_id])
    @user_activity = UserActivity.create(user: current_user, challenge: challenge, status: "Start")
    current_user.user_activities.where(status: nil).delete_all
    if @user_activity.save
      redirect_to challenge, notice: "Challenge Joined. Please specify the start date of the challenge on the challenge page."
    else
      redirect_to challenge, notice: "You have already joined this challenge."
    end
  end

  def update
    user_activity = UserActivity.find(params[:id])
    user_activity.start_time = DateTime.parse(params[:user_activity][:start_time])
    user_activity.save
    user_activity.number_of_days.times do |n|
      user_activity.daily_activities.create(start_time: user_activity.start_time+n.day)
    end
    flash.notice = "Challenge starts on #{user_activity.start_time}"
    redirect_to dashboard_path
  end

  def sync
    sync_data(current_user.id)
    # DataSyncWorker.perform_async(current_user.id)
    redirect_to dashboard_path
  end
end
