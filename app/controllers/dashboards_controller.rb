class DashboardsController < ApplicationController
  helper 'questionnaires/surveys'
  include ChallengesHelper
  include Questionnaires::SurveysHelper
  before_filter :authenticate_user!, only: [:show]

  def show
    @dashboard = UserDashboard.new(current_user)
    if current_user.mutual_friends.count > 0
      @posts = Post.from_friends_of(current_user)
      user_ids = ([current_user.id] + current_user.friend_ids + current_user.inverse_friend_ids).join(',')
      @posts = @posts.where("creator_id IN (#{user_ids})").where("user_id IN (#{user_ids})")
    else
      @posts = current_user.posts
    end
    @user = current_user
    @survey =  Survey::Survey.active.first
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @attempt = @survey.attempts.new
      @attempt.participant = current_user
      @attempt.save
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
    gon.count = current_user.identities.where("provider=? or provider=?","fitbit","moves").count
    @count = gon.count
    # if @attempt.nil?
    #   @attempt = @survey.attempts.new
    #   @attempt.answers.build
    #   @unanswered_questions = @survey.questions
    #   @unanswered_questions = [@unanswered_questions.first]
    # end
    # @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    # @unanswered_questions =  attempt_unanswered_questions
    # @unanswered_questions = [@unanswered_questions.first]
      # @participant = current_user # you have to decide what to do here
      # @survey =  Survey::Survey.active.first
      # @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
      # if @attempt
      #   @unanswered_questions = attempt_unanswered_questions
      # else
      #   @unanswered_questions = @survey.questions
      # end
      # @unanswered_questions = [@unanswered_questions.first]
      # unless @survey.nil?
      #   @attempt = @survey.attempts.new
      #   @attempt.answers.build
      # end
  end

  def attempt_unanswered_questions
    Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def link_to_remove_field(name, f)
    f.hidden_field(:_destroy) +
    link_to_function(raw(name), "removeField(this)", :id =>"remove-attach")
  end

  def new_survey
    new_questionnaires_survey_path
  end

  def edit_survey(resource)
    edit_questionnaires_survey_path(resource)
  end

  def survey_scope(resource)
    if action_name =~ /new|create/
      questionnaires_surveys_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_survey_path(resource)
    end
  end

  def new_attempt
    new_questionnaires_attempt_path
  end

  def attempt_scope(resource)
    if action_name =~ /new|create/
     questionnaires_attempts_path(resource)
    elsif action_name =~ /edit|update/
      questionnaires_attempt_path(resource)
    end
  end

  def link_to_add_field(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object,:child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "addField(this, \"#{association}\", \"#{escape_javascript(fields)}\")",
    :id=>"add-attach",
    :class=>"btn btn-small btn-info")
  end
end
