class MonthlyChallengesController < ApplicationController
	before_filter :authenticate_user!, only: [:show]

  def index
    if params[:team_id]
      @team = Team.find(params[:team_id])
    end
    if signed_in?
      if current_user.challenges.size == 0
        @challenges = MonthlyChallenge.where(number_of_days: 7)
      else
        @challenges = MonthlyChallenge.where(number_of_days: 30)
      end
    else
      @challenges = MonthlyChallenge.all
    end
  end

  def create
    @challenge = MonthlyChallenge.new(params.require(:challenge).permit(:name))
    @challenge.save
    redirect_to challenges_path
  end

  def show
    @challenge = MonthlyChallenge.find(params[:id])
    @participating_users = @challenge.participating_users
    leaders_ids = @challenge.user_activities.where(status: 'Progress').select { |ua| ua.updated_daily_activities.count >= 5 }.map(&:user_id)
    @leaders = User.find(leaders_ids).sort_by { |user| -user.current_run_rate }.first(10)
  end
end