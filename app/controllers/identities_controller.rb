class IdentitiesController < ApplicationController
  before_action :set_identity, only: [:destroy]

  def destroy
    gon.count =  current_user.identities.where("provider=? or provider=?","fitbit","moves").count
    @count = gon.count
    provider = @identity.provider.split('_').first
    destroyed = @identity.destroy if @identity.user.encrypted_password.present?
    respond_to do |format|
      format.html {
        if destroyed
          if gon.count == 0
            env["HTTP_REFERER"] += '#social'; redirect_to :back, notice: "#{provider.camelcase} account has been delinked."
          else
            env["HTTP_REFERER"] += '#devices'; redirect_to :back, notice: "#{provider.camelcase} account has been delinked."
          end
        else
          env["HTTP_REFERER"] += '#social'; redirect_to :back, notice: "Primary #{provider.camelcase} account cannot be delinked." # TODO: Check requirement of env variable
        end
        }
      format.json { head :no_content }
    end
  end

  private
  def set_identity
    @identity = Identity.find(params[:id])
  end

  def identity_params
    params[:identity]
  end
end
