class ActionMessagesController < ApplicationController

  def new
    @identity = Identity.new
  end

  def import
    if params[:password].eql? ENV['ADMIN_PASSWORD']
      Action.import(params[:file])
      redirect_to dashboard_path, notice: 'Messages were added successfully.'
    else
      redirect_to dashboard_path, alert: 'Unauthorized request!'
    end
  end
end

