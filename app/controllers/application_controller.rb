require 'modules/sync_data'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  after_filter :discard_flash_if_xhr
  before_filter :force_html

  include SyncData
  has_mobile_fu
  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end # CanCan workaround hack to support Strong Parameters 4

  include Questionnaires::SurveysHelper

  def set_current_user
    User.current = current_user
  end

  def current_user
    @current_user ||= super
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to '/', alert: exception.message
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :user_name
    devise_parameter_sanitizer.for(:accept_invitation) do |u|
      u.permit(:user_name, :password, :password_confirmation, :invitation_token)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:first_name, :last_name, :user_name, :email, :password, :password_confirmation,:photo,:city,:organisation,:weight,:foot,:inches,:birth_date, :gender, :living)
    end
  end

  def after_accept_path_for(user)
    if current_user.invited_to_team_id
      Team.find(current_user.invited_to_team_id)
    else
      registrations_welcome_page_path
    end
  end

  def after_sign_in_path_for(resource)
    dashboard_path
  end

  def after_update_path_for(resource)

  end

  def is_user_confirmed?
    if current_user.confirmed?
    else
      flash.notice = "Please confirm account first"
      redirect_to dashboard_path
    end
  end

  def discard_flash_if_xhr
    flash.discard if request.xhr?
  end
  def force_html
    session[:mobile_view] = false
    session[:tablet_view] = false
  end
end
