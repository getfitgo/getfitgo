class FriendshipsController < ApplicationController
  respond_to :html, :js
  def create
    friend = User.find(params[:friend_id])
    @friendship = friend.friendships.find_by(friend: current_user)
    if friend.friendships.find_by(friend: current_user)
      @friendship.update_attributes(:confirmed => true)
      Mailer.friend_request_accepted(@friendship).deliver
    else
      @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
      Mailer.send_friend_request(@friendship).deliver
      @friendship.save 
    end
  end

  def destroy
    @friendship = Friendship.find(params[:id])
    @friendship.destroy
  end

  def edit
    @friendship = Friendship.find(params[:id])
    @friendship.update_attributes(:confirmed => true)
    Mailer.friend_request_accepted(@friendship).deliver
  end
end
