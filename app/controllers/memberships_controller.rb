class MembershipsController < ApplicationController
  before_filter :authenticate_user!

  def create
    team = Team.find(params[:team_id])
    flash.notice = "Team Joined."
    redirect_to team
  end

  def destroy
  	team = Team.find(params[:team_id])
  	user = User.find(id)
  	team.members - user
    flash.notice = "You have been removed from the team."
    redirect_to dashboard_path
  end
end
