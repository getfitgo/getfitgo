class SocialSharingController < ApplicationController
  before_filter :authenticate_user!

  def post_registrations
    post_message "#{current_user.user_name} have started a new experience to #feelgoodnow on getfitgo."
    redirect_to dashboard_path
  end

  def post_badge_won
    badges = Badge.where(id: params[:badge_ids])
    message = prepare_badge_won_message(badges)
    post_message message, badges.max_by(&:achievement_order).image.url
    respond_to do |format|
      format.html {
        redirect_to dashboard_path, notice: 'Your achievement was shared through the connected social account.'
      }
      format.js {
        flash.notice = 'Your achievement was shared through the connected social account.'
        render 'post_share'
      }
    end
  end

  def post_challenge_join
    challenge = Challenge.find(params[:id])
    post_message "I have started a new Activity #{challenge.name} on getfitgo. This should be fun. Would love your support and motivation. "
    redirect_to dashboard_path
  end

  def post_challenge_completion
    challenge = Challenge.find(params[:id])
    post_message "#{first_name} just completed a challenge to walk #{challenge.number_of_steps} steps in #{challenge.number_of_days/7} weeks.", "#{ENV["host"]}/walk_man_round.png"
    respond_to do |format|
      format.html {
        redirect_to dashboard_path, notice: 'Your achievement was shared through the connected social account.'
      }
      format.js {
        flash.notice = 'Your achievement was shared through the connected social account.'
        render 'post_share'
      }
    end
  end

  private

  def graph
    Koala::Facebook::API.new(current_user.identities.find_by(provider: "facebook").auth_token)
  end

  def post_message(message, picture = nil, description = nil)
    options = {
        :message => message,
        :link => "http://alpha.getfitgo.in",
        :picture => "#{ENV["host"]}#{picture}"
    }
    begin
      graph.put_object("me", 'feed', options)
    rescue Koala::Facebook::APIError => exc
      logger.error("Problems posting to Facebook Wall..." + self.inspect + " " + exc.message)
    end
  end

  def first_name
    current_user.user_name.split(' ').first + ' has' rescue 'I have'
  end

  def prepare_badge_won_message badges
    first_name + ' earned ' + badges.map { |badge|
      "the '#{badge.name}' badge #{badge.eligibility_criteria}"
    }.join(' and ') + '.'
  end
end
