class DailyActivitiesController < ApplicationController
  include ActionView::Helpers::AssetTagHelper
  before_filter :set_current_user
  def update
    result = UpdateDailyActivity.perform(params)
    flash.notice = "Thanks for entering your steps." #calculate_message(result.badge_awarded)
    unless result.badge_awarded.empty?
      session[:achievements_unlocked] = { badges_id: result.badge_awarded.map(&:id), overlay: true }
      session[:achievements_unlocked][:badges_id].each { |badge_id|
        Notification.badge_earned(current_user, badge_id)
      }
    end
    respond_to do |format|
      format.html { redirect_to dashboard_path }
      format.js { render 'update' }
    end
  end

  private

  def current_activity
    current_user.user_activities.find_by(status: "Progress")
  end

  def calculate_message(badge_awarded)
    notice = "Thanks for entering your steps. "
    notice << "Congrats, you just won #{view_context.pluralize(badge_awarded.count, 'badge')}!<ul>#{badge_awarded.each.map { |badge| "<li>#{badge.name} #{image_tag badge.image, :size => '20x20'}</li>" }.join('')}</ul>".html_safe unless badge_awarded.empty?
    #notice << "You already won the following #{view_context.pluralize(already_owned.count, 'badge')}.<ul>#{already_owned.each.map {|badge| "<li>#{badge} #{image_tag Badge.find_by_name(badge).image, :size => '20x20'}</li>" }.join('')}</ul>".html_safe unless already_owned.count.eql? 0
    notice
  end

end
