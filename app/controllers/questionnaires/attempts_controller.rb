class Questionnaires::AttemptsController < ApplicationController

  helper 'questionnaires/surveys'

  before_filter :load_active_survey
  before_filter :normalize_attempts_data, :only => [:create,:update]

  before_filter :authenticate_user!
  respond_to :html, :js

  def new
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    if @attempt
      @unanswered_questions = attempt_unanswered_questions
    else
      @attempt = @survey.attempts.new
      @attempt.participant = current_user
      @attempt.save
      @unanswered_questions = @survey.questions
    end
    @unanswered_questions = [@unanswered_questions.first]
    render_thank_you
    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
  end

  def edit
    @participant = current_user
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    @unanswered_questions =  attempt_unanswered_questions
    @unanswered_questions = [@unanswered_questions.first]
    respond_to do |format|
      format.html { redirect_to (edit_user_registration_path + "#survey") }
      format.js { render_thank_you }
    end
  end

  def update
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    @participant = current_user
    @attempt.update_attributes(attempt_params)
    @attempt.update_attributes(score: calculated_score)
    current_user.update_fitness(@attempt.score)
    respond_to do |format|
       format.html { redirect_to (edit_user_registration_path + "#survey") }
       format.js {  redirect_to edit_questionnaires_attempt_path(@attempt)  }
    end
  end

  def create
    @attempt = Survey::Attempt.all.find_by_participant_id(current_user.id)
    @participant = current_user
    @attempt.update_attributes(attempt_params)
    @attempt.update_attributes(score: calculated_score)
    current_user.update_fitness(@attempt.score)
    respond_to do |format|
       format.html { redirect_to (edit_user_registration_path + "#survey") }
       format.js {  redirect_to edit_questionnaires_attempt_path(@attempt)  }
    end
  end

  private

  def load_active_survey
    @survey =  Survey::Survey.active.first
  end

  def normalize_attempts_data
    normalize_data!(params[:survey_attempt][:answers_attributes])
  end

  def normalize_data!(hash)
    multiple_answers = []
    last_key = hash.keys.last.to_i

    hash.keys.each do |k|
      if hash[k]['option_id'].is_a?(Array)
        if hash[k]['option_id'].size == 1
          hash[k]['option_id'] = hash[k]['option_id'][0]
          next
        else
          multiple_answers <<  k if hash[k]['option_id'].size > 1
        end
      end
    end
    multiple_answers.each do |k|
      hash[k]['option_id'][1..-1].each do |o|
        last_key += 1
        hash[last_key.to_s] = hash[k].merge('option_id' => o)
      end
      hash[k]['option_id'] = hash[k]['option_id'].first
    end
  end

  def attempt_params
    rails4? ? params_whitelist : params[:survey_attempt]
  end

  def params_whitelist
    params.require(:survey_attempt).permit(Survey::Attempt::AccessibleAttributes)
  end

  def render_thank_you
    if @unanswered_questions.first.nil?
      render "thank_you"
    end
  end

  def attempt_unanswered_questions
    Survey::Question.all.find(unanswered_questions_ids)
  end

  def unanswered_questions_ids
    all_questions.pluck(:id) - question_answered.pluck(:question_id)
  end

  def question_answered
    Survey::Attempt.all.find_by_participant_id(current_user.id).answers
  end

  def all_questions
    Survey::Attempt.all.find_by_participant_id(current_user.id).survey.questions
  end

  def calculated_score
    score = 0
    question_id = Survey::Question.find_by_text("My favourite activities include (we're sure you have more than one favourite):").id
    @attempt.answers.each do |answer|
      if answer.question_id == question_id
      else
        score += answer.value || 0
      end
    end
    if @attempt.answers.where(question_id: question_id).count > 4
      score += 4
    else
      score += @attempt.answers.where(question_id: question_id).count
    end
    score
  end
end
