class ReportsController < ApplicationController
  def show
    respond_to do |format|
      @user = User.find(params[:user_id])
      format.js {
        render 'users/show'
      }
    end
  end
end
