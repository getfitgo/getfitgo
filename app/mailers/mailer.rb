class Mailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default from: "The getfitgo Team <#{ENV['SENDER']}>"
  default :content_type => 'text/html'

  def team_admin_invitation_instructions(record, opts={})
    devise_mail(record, :team_admin_invitation_instructions, opts)
  end

  def new_acceptance_notifier(invited, inviter)
    @invited = invited
    @inviter = inviter
    mail to: @inviter.email, :content_type => 'text/html', subject: 'Invitation Accepted'
  end

  def send_notification_ask_steps(user)
    @user = user
    mail to: user.email,:content_type => 'text/html', subject: "Have you updated your Daily Activity on getfitgo"
  end

  def welcome_mail(user)
    @user = user
    mail to: user.email,:content_type => 'text/html', subject: "Welcome to getfitgo!"
  end

  def friend_request_accepted(friendship)
    @friendship = friendship
    mail to: @friendship.user.email,:content_type => 'text/html', subject: "#{@friendship.friend.user_name} has accepted your friend request."
  end

  def send_friend_request(friendship)
    @friendship = friendship
    mail to: @friendship.friend.email,:content_type => 'text/html', subject: "#{@friendship.user.user_name} wants to connect with you on getfitgo"
  end

  def invitation(invitation, signup_url)
    mail(subject: "Reminder: #{current_user.user_name} has invited you to join getfitgo!",
         recipients: invitation.recipient_email,:content_type => 'text/html',
         body: {:invitation => invitation, :signup_url => signup_url})
    invitation.update_attribute(:sent_at, Time.now)
  end

  def send_challenge_start_notifier(user)
    @user = user

    mail to: user
  end

  def send_email_to_all_team_members(user)
    @user = user

    mail to: user
  end

  def set_email_notification_to_be_sent(user)
    @user = user

    mail to: user
  end

  def request_invite requestor
    @user = requestor
    mail to: 'hello@getfitgo.in', subject: "#{@user.email} requests an invite"
  end

  def fitbit_reconnect_notifier(user)
    @user = user
    mail to: @user.email, subject: "Please reconnect your fitbit device."
  end

  def weekly_update_csv csv_path
    attachments['weekly_update.csv'] = File.read(csv_path)
    mail to: 'nikhil@getfitgo.in', cc: 'tanay@getfitgo.in', subject: "Weekly Update Data: #{1.week.ago.to_date} - #{Date.yesterday}"
  end

  protected

  def subject_for(key)
    return super unless key == :invitation_instructions

    I18n.t('devise.mailer.invitation_instructions.subject',
           :invited_by => resource.invited_by.user_name)
  end

end
