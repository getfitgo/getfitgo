class Qualification < ActiveRecord::Base
  include RuleEngine
  has_and_belongs_to_many :insights

  def self.profile user, facts={}, before={}, after={}, range={}
    challenge_days = user.current_challenge.challenge.number_of_days

    qualification = self.rule_engine('qualification', {}, {}, { :challenge_days => challenge_days }, {})

    if qualification.is_a?(Class) || qualification.empty?
      user_tag_type = user.tag_type
      Action.where(:tag => user_tag_type).map {|a| a.message}.sample
    else
      current_challenge = user.current_challenge
      user_activity_id = current_challenge.id
      facts, before, after, range = { target_achieved: current_challenge.target_achieved?, activity_data: current_challenge.updated_daily_activities.count, day_of_challenge: (Date.today - current_challenge.start_time.to_date).to_i + 1 }, { activity_data: current_challenge.updated_daily_activities.count }, { day_of_challenge: (Date.today - current_challenge.start_time.to_date).to_i + 1 }, { progress: user.pro_rated_target_progress, completed: user.progress }
      Action.dynamic_feedback_processing(Insight.message(facts, before, after, range, user_activity_id), user)
    end
  end
end
