class Notification < Mailboxer::Notification

  # TODO: Should be made DRYer and intelligent in itself by keeping logic itself the class
  def self.badge_earned user, badge_id
    badge = Badge.find(badge_id)
    File.open("public/Notch Badges/#{badge.name}.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You've earned the '#{badge.name}' badge. Here's something to be proud of.", 'Congratulations!', true, file)
      receipt.notification.update_attributes(priority: 1)
    end
  end

  def self.post_created user, body
    File.open("public/notifications/data_not_updated.png", 'r') do |file|
      receipt = sender_user.send_message(user.mutual_friends, body, "#{user.first_name} just posted.")
      receipt.notification.update_attributes(header: true)
    end
  end

  def self.day_2_of_challenge user
    File.open("public/notifications/day_2_of_challenge.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You've started an exciting 4-week training plan. Ensure you cover around <strong>#{user.current_challenge.daily_goal} steps a day</strong> to meet your overall target. This means <strong>only #{user.current_challenge.challenge.number_of_minutes} of walking daily</strong>. We're sure you can do it. All the best!", 'Hey,', true, file)
      receipt.notification.update_attributes(priority: 3)
    end
  end

  def self.warm_up_started user
    File.open("public/notifications/warm_up_started.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You've started your journey towards a fitter and healthier lifestyle. Time to <strong>#feelgoodnow</strong>.<br/><br/>Warm up with <span style='color:#00c8c8'>get</span><span style='color:#fa9600'>fit</span><span style='color:#00c8c8'>go</span> over the next 7 days. Get used to tracking your activities daily and we'll keep sharing feedback on how you are doing. Good luck!", "Welcome to <span class='get'>get</span><span class='fit'>fit</span><span class='go'>go</span>!", true, file)
      receipt.notification.update_attributes(priority: 2, expires: 3.days.from_now)
    end
  end

  def self.day_4_of_warm_up user
     File.open("public/notifications/warm_up_started.png", 'r') do |file|
       receipt = sender_user.send_message(user, "Hope you are getting familiar with <span class='get'>get</span><span class='fit'>fit</span><span class='go'>go</span> now. If you have any questions, you can always reach us at <a href='mailto:hello@getfitgo.in' class='colored-link'>hello@getfitgo.in</a>.<br/><br/>You will start an exciting 4-week training plan on <strong>#{user.first_transition_day.strftime("#{user.first_transition_day.day.ordinalize} %B")}</strong>, custom designed for your profile by our coaches. Ensure your profile and activities are updated.", "You've been warming up for 4 days.", true, file)
       receipt.notification.update_attributes(priority: 2)
     end
  end

  def self.update_profile user
    File.open("public/notifications/update_profile.png", 'r') do |file|
      receipt = sender_user.find_or_send_message(user, "We've noticed your profile is not updated. Our coaches need to know more about you to recommend your 4-week training plan scheduled to start on <strong>#{user.first_transition_day.strftime("#{user.first_transition_day.day.ordinalize} %B")}</strong>. Your training plan will only start after you update your profile.<br/><br/><a href='/update_profile' target='_blank' class='btn btn-branding'>Update Profile</a>", 'Hey,', false, file)
      receipt.notification.update_attributes(priority: 1)
    end
  end

  def self.day_7_profile_updated user
    File.open("public/notifications/warm_up_started.png", 'r') do |file|
      receipt = sender_user.find_or_send_message(user, "You will start an exciting 4-week training plan designed by our coaches on <strong>#{user.first_transition_day.strftime("#{user.first_transition_day.day.ordinalize} %B")}</strong>. Get Set!", 'You are doing great.', true, file)
      receipt.notification.update_attributes(priority: 1, expires: 1.day.from_now)
    end
  end

  def self.day_after_challenge_completion user
    File.open("public/notifications/consistent_performance.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You've completed your 4-week training plan. You have walked for <strong>#{user.last_completed_challenge.total_steps_covered} kms.</strong> and in the process won <strong>#{user.last_completed_challenge.achievements_unlocked.count} badges</strong> and <strong>#{user.last_completed_challenge.point_earned} points</strong>.", 'Congratulations!', true, file)
      receipt.notification.update_attributes(priority: 1)
    end
  end

  def self.data_not_updated user
    File.open("public/notifications/data_not_updated.png", 'r') do |file|
      receipt = sender_user.find_or_send_message(user, "We havn't seen you update your activities recently. Hope all is well.<br/><br/>In case you need help, please reach out to us <a href='http://support.getfitgo.in' target='_blank' class='colored-link'>support@getfitgo</a> or email <a href='mailto:hello@getfitgo.in' class='colored-link'>hello@getfitgo.in</a>. It'll be great to see you join all the others who are on the <strong>#feelgoodnow</strong> journey.", 'We are missing you.', false, file)
      receipt.notification.update_attributes(priority: 1)
    end
  end

  def self.best_performance user
    File.open("public/notifications/best_performance.png", 'r') do |file|
      receipt = sender_user.find_or_send_message(user, "You have done a personal best - <strong>#{user.best_performance.steps_covered} steps</strong> on <strong>#{user.best_performance.start_time.strftime("#{user.best_performance.start_time.day.ordinalize} %B")}</strong>.<br/><br/>We know how good it feels, soak it in and treat yourself to something good today and, of course, plan on breaking the record once again <strong>;-)</strong>", 'Awesome!', true, file)
      receipt.notification.update_attributes(priority: 2)
    end
  end

  def self.consistent_performance user
    File.open("public/notifications/best_performance.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You have been one consistent machine the past week. Keep it up. You should be proud of yourself.", 'Wonderful!', true, file)
      receipt.notification.update_attributes(priority: 2)
    end
  end

  def self.slow_performance user
    File.open("public/notifications/slow_performance.png", 'r') do |file|
      receipt = sender_user.send_message(user, "We noticed you've had a few slow days. That’s not you! <strong>We hope all is well.</strong><br/><br/>Consistency is the key in creating a new habit, so be sure that you get something in at least 5 days a week.", "Hey #{user.first_name}", true, file)
      receipt.notification.update_attributes(priority: 1)
    end
  end

  def self.friend_joined user, friend
    File.open("public/notifications/update_profile.png", 'r') do |file|
      receipt = sender_user.send_message(user, "Your friend <a href='users/#{friend.id}' target='_blank' class='colored-link'>#{friend.first_name}</a> has joined getfitgo. Enjoy this fitness journey together.", "Hey,", false, file)
      receipt.notification.update_attributes(header: true)
    end
  end

  def self.friend_request_accepted user, friend
    File.open("public/notifications/update_profile.png", 'r') do |file|
      receipt = sender_user.send_message(user, "You are now friends with <a href='users/#{friend.id}' target='_blank' class='colored-link'>#{friend.first_name}</a>.", "Hey,", false, file)
      receipt.notification.update_attributes(header: true)
    end
  end

  def self.connection_started_challenge user
    File.open("public/notifications/warm_up_started.png", 'r') do |file|
      receipt = sender_user.send_message(user.mutual_friends, "Your friend <a href='users/#{user.id}' target='_blank' class='colored-link'>#{user.first_name}</a> has started a new challenge to walk #{user.current_challenge.goal} steps in #{user.current_challenge.challenge.number_of_days/7} weeks on <span class='get'>get</span><span class='fit'>fit</span><span class='go'>go</span>. Wish them luck.", "Hey,", false, file)
      receipt.notification.update_attributes(header: true)
    end
  end

  def self.sender_user
    @sender_user ||= User.first
  end
end