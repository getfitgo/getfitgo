class Warmup
  def self.put_user_into_slab_and_find_challenge(user)
    daily_activities = user.daily_activities.where.not(steps_covered: nil)
    # total_daily_activities = user.daily_activities
    # if daily_activities.count == 7 && total_daily_activities.count == 7
      # This calculation is expected to change later on.
      # user.current_challenge.update_attributes(status:"Completed")
      median = user.last_completed_challenge.total_steps_covered/daily_activities.count
      self.calculate_next_challenge(median, user)
    # end
  end

  def self.calculate_next_challenge(median, user)
    next_challenge = find_next_challenge(median)
    # user_activity = UserActivity.create(user: user, challenge: next_challenge, start_time: Time.now, status: "Progress")
    # user_activity.number_of_days.times do |n|
    #   user_activity.daily_activities.create(start_time: user_activity.start_time+n.day)
    # end
    # next_challenge
  end

   def self.find_next_challenge(median)
    # challenges = Challenge.where(type:"MonthlyChallenge").order(:number_of_steps)
    # case median
    # when 0..2000
    #   challenge = challenges[0]
    # when 2001..3300
    #   challenge = challenges[1]
    # when 3301..4500
    #   challenge = challenges[2]
    # when 4501..5800
    #   challenge = challenges[3]
    # else
    #   challenge = challenges[4]
    # end
    # return challenge
    next_challenge = MonthlyChallenge.where('number_of_steps < ?', median * 28).order(:number_of_steps)
    next_challenge.empty? ? MonthlyChallenge.order(:number_of_steps).first : next_challenge.last
  end

   def self.category
    Category.find_or_create_by(name: "30 Day Challenge")
  end
end