class Activity < ActiveRecord::Base
  belongs_to :user
  belongs_to :subject, polymorphic: true
  acts_as_votable
  acts_as_commentable

  def photo
    subject.photo
    # begin
    #   if subject.is_a?(Achievement)
    #     subject.badge.photo
    #   else
    #     subject.photo
    #   end
    # rescue
    #   user.photo
    # end
  end

  def user_to_link_to
    subject
  end
end
