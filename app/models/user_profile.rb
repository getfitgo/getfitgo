class UserProfile < ActiveRecord::Base
  belongs_to :user
  belongs_to :profile

  # TODO: Make activity feedable perhaps?

  def self.reprofile_user user, profile
    user_profile = find_by(user_id: user.id, profile_id: profile.id)
    profile = Profile.profile(user)
    profile.nil? ? user_profile.destroy : user_profile.profile = profile; user_profile.save
    profile
  end

  def self.profile_user user
    profile = Profile.profile(user)
    profile.nil? ? nil : create(user_id: user.id, profile_id: profile.id)
    profile
  end
end
