class DailyActivity < ActiveRecord::Base
  belongs_to :user_activity
  has_one :achievement
  delegate :challenge, to: :user_activity
end
