class Friendship < ActiveRecord::Base
  include ActionDispatch::Routing::UrlFor
  include Rails.application.routes.url_helpers
  belongs_to :user
  belongs_to :friend, :class_name => 'User'
  # after_create :create_friend
  delegate :user_name, to: :friend
  after_update :make_friend

  def photo
    subject.photo
  end

  def create_friend
    Activity.create(
        subject: user,
        name: "#{helper.link_to user.user_name, user} sent a request to #{helper.link_to friend.user_name, user_path(friend.id)}",
        user: user
    )
  end

  def make_friend

    friends_between(user,friend).each do |mutual_friend|
      Activity.create(
        subject: mutual_friend,
        name: "Your friends #{ActionController::Base.helpers.link_to(friend.user_name, user_path(friend.id))} and #{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))} are now connected",
        user: mutual_friend
    )
    end
    Activity.create(
        subject: user,
        name: "#{ActionController::Base.helpers.link_to(friend.user_name, user_path(friend.id))} is now connected to #{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))}",
        user: user
    )
    (user.mutual_friends - [friend] - friends_between(user,friend)).each do |mutual_friend|
       Activity.create(
        subject: friend,
        name: "#{ActionController::Base.helpers.link_to(friend.user_name, user_path(friend.id))} is now connected to #{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))}",
        user: mutual_friend
    )
    end
    Activity.create(
        subject: friend,
        name: "#{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))} is now connected to #{ActionController::Base.helpers.link_to(friend.user_name, user_path(friend.id))}",
        user: friend
    )
    (friend.mutual_friends - [user] - friends_between(user,friend)).each do |mutual_friend|
       Activity.create(
        subject: user,
        name: "#{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))} is now connected to #{ActionController::Base.helpers.link_to(friend.user_name, user_path(friend.id))}",
        user: mutual_friend
    )
    end
  end

  def friends_between(user, friend)
    user.mutual_friends&friend.mutual_friends
  end
end