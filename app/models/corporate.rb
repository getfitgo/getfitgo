class Corporate < ActiveRecord::Base
	extend FriendlyId
  friendly_id :url, use: :slugged
  has_many :corporate_challenges
  has_many :challenges, through: :corporate_challenges
  # has_many :corporate_challenges
  # has_many :challenges, through: :corporate_challenges
  has_many :teams
  has_many :corporate_members, dependent: :destroy
  has_many :employees, through: :corporate_members, source: :user, dependent: :destroy
  validates :name, presence: true
  validates :end_date, presence: true
  validates :start_date, presence: true
  validates :url, length: { maximum: 10 }
	has_attached_file :photo, :styles => {:medium => "300x300>", :thumb => "100x100>"},
	:storage => :s3,
	:s3_credentials => {
	:access_key_id => "AKIAJF255ZNBUOTPZRRA",
	:secret_access_key => "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"
	},
	:bucket => ENV['S3_BUCKET_NAME'],
	:path => ":attachment/:id"
end
