require "open-uri"
class Post < ActiveRecord::Base
  auto_html_for :name do
    html_escape
    image
    youtube(:width => 400, :height => 250, :autoplay => true)
    link :target => "_blank", :rel => "nofollow"
    simple_format
  end
  validates :user_id, presence: true
  belongs_to :user
  scope :recent_first, -> { order('created_at DESC') }
  validates :user_id, presence: true
  acts_as_votable
  has_attached_file :image, :styles => { :large => "500x500", :thumb => "200x200" },
  :storage => :s3,
  :s3_domain_url => "s3-website-ap-southeast-1.amazonaws.com",
  :s3_credentials => {
    :access_key_id => "AKIAJF255ZNBUOTPZRRA",
    :secret_access_key => "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"
    },
    :bucket => ENV['S3_BUCKET_NAME']
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  belongs_to :creator,class_name: "User"

  acts_as_commentable


  def photo
    # begin
    #   if subject.is_a?(Achievement)
    #     subject.badge.photo
    #   else
    #     subject.photo
    #   end
    # rescue
    creator.photo
  end

  def picture_from_url(url)
    self.image = URI.parse(url)
  end

  def user_to_link_to
    creator
  end

  def self.from_friends_of(user)
    friend_ids = (user.friend_ids + user.inverse_friend_ids).join(',')
    where("user_id IN (#{friend_ids}) OR user_id = :user_id", user_id: user.id)
  end
end
