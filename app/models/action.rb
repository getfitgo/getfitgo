class Action < ActiveRecord::Base
  has_many :insights

  def self.import file
    CSV.foreach(file.path, headers: true) { |row|
      Action.create!(:message => row[0], :tag => row[1])
    }
  end

  # TODO: Transfer these methods to user_activity or challenge respectively
  def self.dynamic_feedback_processing message, user
    message.sub('f33lg00dn@w_minutes', user.steps_to_minutes.to_s).sub('f33lg00dn@w_kms', user.steps_to_kms({marathon:true}).to_s).sub('f33lg00dn@w_best', user.best_average_steps.to_s).sub('f33lg00dn@w_repeat', user.best_average_steps_times.to_s).sub('f33lg00dn@w_target', user.current_target.to_s).sub('f33lg00dn@w_marathon', user.times_full_marathon?.to_s).sub('f33lg00dn@w_samosas', user.samosas_burnt?.to_s).sub('f33lg00dn@w_leaderboard_position', user.leaderboard_position?.to_s).sub('f33lg00dn@w_participants_count', user.number_of_participating_users?.to_s)
  end
end
