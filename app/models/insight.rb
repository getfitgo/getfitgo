class Insight < ActiveRecord::Base
  include RuleEngine
  belongs_to :action
  has_and_belongs_to_many :qualifications
  has_and_belongs_to_many :user_activities, join_table: 'insight_user_activities'

  #store_accessor :rule, :data_points_min, :data_points_max

  # convert string to integer. hstore only stores strings
  #def data_points_min
  # self.rule['data_points_min'].to_i
  #end

  def self.message facts={}, before={}, after={}, range={}, user_activity_id
    insight = self.rule_engine('rule', facts, before, after, range)

    if insight.count.eql? 1
      insights_with_same_feedback_tag = InsightUserActivity.joins(:action).where('actions.tag LIKE ?', "%#{insight.first.action.tag.split('_',3).first(2).join('_')}%").count
      insight_user_activity = InsightUserActivity.find_or_create_by!(:insight_id => insight.first.id, :user_activity_id => user_activity_id)  # TODO: Limit feedback 1 message to only once out of 5/6/7 data points

      if (insight_user_activity.created_at.to_date.eql? Date.today) && (insights_with_same_feedback_tag.eql? 1)
        insight.first.action.message rescue nil
      else
        Action.where(:tag => UserActivity.find(user_activity_id).user.tag_type).map {|a| a.message}.sample # TODO: Need heavy caching to reduce page refresh load; can be cached for 10 hours
      end
    else
      Action.where(:tag => UserActivity.find(user_activity_id).user.tag_type).map {|a| a.message}.sample
    end
  end
end
