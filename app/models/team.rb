class Team < ActiveRecord::Base
  has_many :badges, :through => :levels
  has_many :levels
  delegate :email, to: :admin
  has_many :posts

  has_attached_file :photo, :styles => {:medium => "300x300>", :thumb => "100x100>"},
  :default_url => "/tshirt blue.png",
  :storage => :s3,
  :s3_credentials => {
  :access_key_id => "AKIAJF255ZNBUOTPZRRA",
  :secret_access_key => "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"
},
  :bucket => ENV['S3_BUCKET_NAME']
  validates :name, presence: true
  belongs_to :admin, :class_name => "User", :foreign_key => "user_id"
  has_many :memberships
  has_many :members, through: :memberships, source: :user
  has_many :team_activities
  has_many :challenges, :through => :team_activities
  def admin?(user)
    if self.user_id == user.id
      return true
    else
      return false
    end
  end

  def set_max_members(maximum)
    update_attributes(max_members: members)
  end

  def self.ranked(challenge)
    # team_ids = Challenge.where(name:"Walking Challenge").joins(:team_activities).pluck(:team_id)
    # teams = Team.where({:id => [1,2,3,4,5] })

    teams = challenge.teams
    teams.sort_by do |team1, team2|
      TeamActivity.find_by(team: team1, challenge: challenge).progress[1] <=> TeamActivity.find_by(team: team1, challenge: challenge).progress[1]
    end
  end

  def users_invited
    User.where(invited_to_team_id: self.id)
  end

  def members_joined
    [members, admin].flatten
  end

  def user_invitations_pending
    pending = 0
    users_invited.each do |user|
      if members.include?(user)
      else
        pending += 1
      end
    end
    return pending
  end
end
