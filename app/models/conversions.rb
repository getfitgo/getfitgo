class Conversions
   def steps_to_minutes(user)
    extra_steps = user.required_run_rate - user.current_run_rate
    extra_steps = 7571 if extra_steps > 7571
    extra_minutes = extra_steps/126.2
    if extra_minutes % 5
      extra_minutes = extra_minutes.round(-1)
    end
    "#{extra_minutes+5}-#{extra_minutes+10}"
  end

  def steps_to_kms options={}
    kms = (user.current_steps * 0.7 / 1000).round(0)
    options[:marathon] ? "#{(kms/42.195*100).round(0)}%" : kms
  end

  def total_steps_to_kms options={}
    (steps_covered(options) * 0.7 / 1000).round(0)
  end
end
