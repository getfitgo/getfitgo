class WeeklyChallenge < Challenge
	has_many :monthly_challenges, class_name: "MonthlyChallenge",:order => 'challenge_associations.position'
end