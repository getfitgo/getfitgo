class UserFeedback
	def initialize(user)
		@user = user
    @settings = BusinessSetting.user_feedback
	end

  def feedback
		if (@user.current_challenge.challenge.type.eql? 'WeeklyChallenge')
		  return 0
		else
			feelings_hash = @user.current_challenge.daily_activities.where.not(feel: nil).where('steps_covered > ?', @user.current_challenge.goal.to_f/28*@settings.target_achievement).pluck(:feel).inject(Hash.new(0)) { |count, feel| count[feel] += 1 ;count}
      feelings_hash.delete_if { |k,v| v < @settings.minimum_number_of_entries }
      max_count = feelings_hash.values.max
      feelings_applicable = Hash[feelings_hash.select { |k, v| v == max_count}]
      case feelings_applicable.count
      when 0
        0
      when 1
        @settings.send(feelings_applicable.keys.first)
      when 2
        feelings_applicable['Awesome'].nil? ? @settings.good : @settings.awesome
      else
        @settings.awesome
      end
    end
	end
end