class InsightUserActivity < ActiveRecord::Base
  belongs_to :insight
  belongs_to :user_activity
  has_one :action, through: :insight
end
