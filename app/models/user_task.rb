class UserTask < ActiveRecord::Base
  belongs_to :user

  after_create :create_activities

  def create_activities
    Activity.create(
        subject: user,
        name: description,
        user: user
    )
  end
end
