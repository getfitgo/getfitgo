module ProfileEnum
  BMI = { 'Normal' => 0, 'Overweight' => 1, 'Obese' => 2 }.freeze
  LIFESTYLE_SURVEY = { 'High' => 0, 'Medium' => 1, 'Low' => 2 }.freeze
  LIFESTYLE_SURVEY_QUESTIONS = { current: ['On a typical day do you','On average, how many days each week do you accumulate at least 30 minutes of physical activity / exercise over the day?','How long do you normally exercise?','If a friend gave you five free sessions at a gym / dance class / sporting club, would you']}.freeze
end