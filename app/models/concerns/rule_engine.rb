module RuleEngine
  extend ActiveSupport::Concern

  module ClassMethods
    def rule_engine rule_column, facts={}, before={}, after={}, range={}
      rules = self
      facts.each { |k,v|
        previous = rules
        rules = rules.where("(#{rule_column} -> '#{k.to_s}')::int = #{v}") unless v.nil?
        rules = previous if rules.count.eql? 0
      }

      range.each { |k,v|
        previous = rules
        rules = rules.where("(#{rule_column} -> '#{k.to_s}_min')::int <= #{v}").where("(#{rule_column} -> '#{k.to_s}_max')::int >= #{v}") unless v.nil?
        rules = previous if rules.count.eql? 0
      }

      before.each { |k,v|
        previous = rules
        rules = rules.where("(#{rule_column} -> '#{k.to_s}_max')::int >= #{v}") unless v.nil?
        rules = previous if rules.count.eql? 0
      }

      after.each { |k,v|
        previous = rules
        rules =  rules.where("(#{rule_column} -> '#{k.to_s}_min')::int <= #{v}") unless v.nil?
        rules = previous if rules.count.eql? 0
      }

      rules
    end
  end
end
