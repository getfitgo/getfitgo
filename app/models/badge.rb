class Badge < ActiveRecord::Base
  include RankedModel
  ranks :achievement_order

  has_attached_file :image, :styles => {:medium => "300x300>", :thumb => "100x100>"},
  :storage => :s3,
  :s3_domain_url => "s3-website-ap-southeast-1.amazonaws.com",
  :s3_credentials => {
    :access_key_id => "AKIAJF255ZNBUOTPZRRA",
    :secret_access_key => "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"
    },
    :bucket => ENV['S3_BUCKET_NAME']
  has_many :teams, :through => :levels
  has_many :users, :through => :levels
  has_many :achievements, :dependent => :destroy
  has_one :previous_badge, :class_name => 'Badge', :foreign_key => 'next_badge_id'
  has_one :next_badge, :class_name => 'Badge', :foreign_key => 'previous_badge_id'
  has_one :previous_order_badge, :class_name => 'Badge', :foreign_key => 'next_order_badge_id'
  has_one :next_order_badge, :class_name => 'Badge', :foreign_key => 'previous_order_badge_id'

  validates :name, :presence => true

  def photo
    self.image
  end

  def self.get_first_badge
    Badge.where("(eligibility -> 'days')::int = ?", minimum("(eligibility -> 'days')::int").to_i).order("(eligibility -> 'steps')::int").first
  end

  def awarded_date user
    Achievement.where(user_id: user.id, badge_id: self.id).first.created_at rescue Date.tomorrow #Hack for streak counter
  end

  def self.award_badge_to_user(current_user, daily_activity)
    badge_awarded, already_owned = [], []
    ["MilestoneBadge", "CummulativeBadge","StreakBadge"].each do |badge|
      badge_allocation = badge.constantize.award_badge_to_user(current_user, daily_activity)
      badge_awarded.push(badge_allocation.first) unless badge_allocation.first.blank?
      already_owned.push(badge_allocation.last) unless badge_allocation.last.blank?
    end
    badge_awarded.flatten!
    already_owned.flatten!
    badge_awarded # TODO: Fix this at source
  end

  def self.award_badge(badge_id, points,user, daily_activity_id)
    if user.award_badge(badge_id, daily_activity_id)
      # user.award_points(points, "Covered #{badge.name} first time")
      return true
    else
      return false
    end
  end

  def eligibility_criteria
    case type
    when 'CummulativeBadge'
      "for completing #{eligibility['total_steps']} steps"
    when 'MilestoneBadge'
      "for completing #{eligibility['steps']} in a single day"
    when 'StreakBadge'
      "for completing #{eligibility['steps']} steps for #{eligibility['days']} days consecutively"
    end
  end
end

