class Point < ActiveRecord::Base
  belongs_to :user

  after_create :create_activities, :assign_level

  def create_activities
     Activity.create(
        subject: user,
        name: self.description,
        user: user
      )
  end

  def assign_level
    UserLevel.assign_level_to(user, user.points.sum(:score))
  end
end
