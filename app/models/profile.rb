class Profile < ActiveRecord::Base
  include RuleEngine
  include ProfileEnum
  belongs_to :challenge
  has_many :user_profiles
  has_many :users, through: :user_profiles

  def self.profile user
    bmi_score, lifestyle_survey_score, age_score = BMI[bmi_grouping(user)], LIFESTYLE_SURVEY[lifestyle_survey_grouping(user)], age_grouping(user)
    if (!bmi_score.nil?) && (!lifestyle_survey_score.nil?) && (!age_score.nil?)
      facts, before, after, range = { bmi: bmi_score, lifestyle_survey: lifestyle_survey_score }, {}, {}, { age: age_score }
      self.rule_engine('assessment', facts, before, after, range).first
    else
      nil
    end
  end

  def self.challenge facts={}, before={}, after={}, range={}
    profile = self.rule_engine('assessment', facts, before, after, range)
    profile.challenge
  end

  private

  # TODO: Rescue from individual methods intelligently to support showing why your profile is incomplete
  def self.bmi_grouping user
    bmi = user.bmi({ true: true }) rescue nil
    (bmi.eql? 'Incomplete') ? bmi : bmi >= 30 ? 'Obese' : (bmi >= 25 ? 'Overweight' : 'Normal')
  end

  def self.age_grouping user
    Date.today.year - user.birth_date.to_date.year rescue nil
  end

  def self.lifestyle_survey_grouping user
    lifestyle_survey_score_categorization(user) rescue 'Incomplete'
  end

  def self.lifestyle_survey_questions_applicable
    Survey::Question.where(text: LIFESTYLE_SURVEY_QUESTIONS[:current]).pluck(:id)
  end

  def self.lifestyle_survey_questions_answers user
    multiple_attempts = Survey::Attempt.all.find_by(participant_id: user.id).answers.order(updated_at: :asc).select('id, question_id').where(question_id: lifestyle_survey_questions_applicable).group_by(&:question_id) rescue nil
    current_answers = []
    multiple_attempts.each { |k,v|
      multiple_attempts[k] = v.last
      current_answers << Survey::Answer.find(v.last.id)
    } if multiple_attempts
    current_answers
  end

  def self.lifestyle_survey_score user
    lifestyle_survey_questions_answers = lifestyle_survey_questions_answers(user)
    score = nil
    lifestyle_survey_questions_answers.each { |answer|
      score ||= 0
      score += answer.value || 0
    } unless lifestyle_survey_questions_answers.empty?
    score
  end

  def self.lifestyle_survey_score_categorization user
    lifestyle_survey_score = lifestyle_survey_score(user)
    unless lifestyle_survey_score.nil?
      lifestyle_survey_score_percentage = lifestyle_survey_score.to_f/(lifestyle_survey_questions_applicable.count*2)*100
      lifestyle_survey_score_percentage >= 75 ? 'High' : lifestyle_survey_score_percentage >= 50 ? 'Medium' : 'Low'
    else
      'Incomplete'
    end
  end
end
