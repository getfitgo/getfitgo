class BusinessSetting < Settingslogic
  source "#{Rails.root}/config/business_settings.yml"
  namespace Rails.env
  suppress_errors Rails.env.production?
end
