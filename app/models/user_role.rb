class UserRole < ActiveRecord::Base
  self.table_name = "corporate_members_roles"
  belongs_to :corporate_member
  belongs_to :role
end
