require 'active_support/inflections'

class Challenge < ActiveRecord::Base
  # searchable do
  # text :name
  # end
  scope :current_challenge, lambda {
    where("start_time between ? and ?", Date.today, Date.today.next_month.beginning_of_month)
  }
  has_many :corporate_challenges
  has_many :corporates, through: :corporate_challenges
  has_many :user_activities
  has_many :user, through: :user_activities
  belongs_to :category
  has_many :team_activities
  has_many :profiles
  has_many :teams, through: :team_activities
  has_many :profiles
  scope :visible, -> { where(visibility: "Public") }
  delegate :name, to: :category, allow_nil: true

  def activity_for(user)
    self.user_activities.where(user: user)
  end

  def participating_users
    UserActivity.includes(:user).where(:challenge_id => self.id, status: 'Progress').map { |ua|
      ua.user
    }
  end

  def not_warm_up?
    type.eql? 'MonthlyChallenge'
  end

  def suggested_challenges_for_team(team)
    Challenge.all
  end

  def suggested_challenges_for_user(user)
    Challenge.all
  end
end
