class ChallengeAssociation < ActiveRecord::Base
  belongs_to :monthly_challenge
  belongs_to :weekly_challenge
  default_scope :order => 'position'
  acts_as_list scope: :monthly_challenge
end
