class StreakBadge < Badge

  def self.badge_eligible_for?(user,daily_activity)
    eligible_streak_badges = []

    if user.current_challenge.challenge.not_warm_up?
      last_streak_badge = user.last_streak_badge
      next_streak_badge = last_streak_badge.next_badge rescue StreakBadge.get_first_badge
      unless next_streak_badge.nil?
        last_daily_activity_date = daily_activity.start_time.to_date #No streaks accounted for in between inputs

        daily_activities = user.daily_activities.where.not(steps_covered: nil).where('daily_activities.steps_covered >= ? and daily_activities.start_time <= ? and daily_activities.start_time > ?', next_streak_badge.eligibility['steps'].to_i, last_daily_activity_date, last_daily_activity_date - next_streak_badge.eligibility['days'].to_i.days - 1.day)
        if daily_activities.count >= next_streak_badge.eligibility['days'].to_i
          eligible_for = daily_activities.map { |da| da.steps_covered }.min
          eligible_streak_badges = user.eligible_streak_badges(eligible_for, next_streak_badge.eligibility['days'].to_i)
        end
      end
    end

    eligible_streak_badges.empty? ? NullBadge.instance : eligible_streak_badges
  end

  def self.award_badge_to_user(user,daily_activity)
    badge_awarded, already_owned = [], []
    badge_eligible = badge_eligible_for?(user,daily_activity)
    unless badge_eligible.class.eql? NullBadge
      badge_eligible.each { |badge|
        if Badge.award_badge(badge.id, badge.points, user, daily_activity.id)
          badge_awarded << badge
        else
          already_owned << badge
        end
      }
    end

    [badge_awarded, already_owned]
  end


  def self.get_first_badge
    self.where("(eligibility -> 'days')::int = ?", minimum("(eligibility -> 'days')::int").to_i).order("(eligibility -> 'steps')::int").first
  end

  def details achiever, date_achieved=nil, current_user=nil
    date_achieved.nil? ? "#{name} badge can be earned by covering #{eligibility['steps']} steps in a day." : "#{achiever} earned this badge on #{date_achieved.strftime("#{date_achieved.day.ordinalize} %B, %Y")} for completing #{eligibility['steps']} steps for #{eligibility['days']} consecutive days.#{if current_user.eql?(current_user);' Way to go!';end}"
  end
end
