class ChallengeTransition

  def initialize(user) # TODO: Why do we need to instantiate objects for a responsibilty-driven class
    @user = user
    @settings = BusinessSetting.challenge_transition
  end

  # TODO: All division to be in float
  def assign_next_challenge options={}
    challenge_based_on_profile = @user.profile.challenge
    daily_activities = @user.last_completed_challenge.daily_activities.where.not(steps_covered: nil)
    total_daily_activities = @user.last_completed_challenge.daily_activities
    if !(@user.last_completed_challenge.challenge.type.eql? 'WeeklyChallenge')
      percentage = get_percentage_completed_of_last_challenge*100 + get_feedback_of_user
      if percentage < @settings.target_achievement.lower
        challenge = MonthlyChallenge.where('number_of_steps < ?', @user.last_completed_challenge.challenge.number_of_steps).order(:number_of_steps).last || MonthlyChallenge.order(:number_of_steps).first
      elsif (percentage < @settings.target_achievement.upper) && (percentage >= @settings.target_achievement.lower)
        challenge = @user.last_completed_challenge.challenge
      else
        challenge = MonthlyChallenge.where('number_of_steps > ?', @user.last_completed_challenge.challenge.number_of_steps).order(:number_of_steps).first || MonthlyChallenge.order(:number_of_steps).last
      end
    elsif daily_activities.count >= 4
      challenge = Warmup.put_user_into_slab_and_find_challenge(@user)
      challenge_steps = @user.steps_covered.to_f/daily_activities.count
    elsif daily_activities.count < 4
      challenge = challenge_based_on_profile
      challenge_steps = challenge_based_on_profile.number_of_steps.to_f/28
    end
    # difference = MonthlyChallenge.order(:number_of_steps).index(challenge) - MonthlyChallenge.order(:number_of_steps).index(challenge_based_on_profile)
    # if difference == 0
      # target_to_set_for_the_user = challenge
    # elsif difference >= @settings.level_delta
      # target_to_set_for_the_user = challenge_based_on_profile.number_of_steps/2.0 + challenge.number_of_steps/2.0
    # else
    challenge_type = @user.last_completed_challenge.challenge.type
    if !(@user.last_completed_challenge.challenge.type.eql? 'WeeklyChallenge')
      if percentage < @settings.target_achievement.bump_down || percentage > @settings.target_achievement.bump_up
        challenge_based_on_profile = challenge unless (percentage < @settings.target_achievement.bump_down) && (@user.last_completed_challenge.updated_daily_activities.count < 18)
      end
      target_to_set_for_the_user = challenge_based_on_profile.number_of_steps*@settings.weight.send(challenge_type).user_profile + challenge.number_of_steps*@settings.weight.send(challenge_type).user_activity
    else
      target_to_set_for_the_user = challenge_based_on_profile.number_of_steps.to_f/28*@settings.weight.send(challenge_type).user_profile + challenge_steps*@settings.weight.send(challenge_type).user_activity
      target_to_set_for_the_user*=28
    end
    # end
    closest_number_of_steps = MonthlyChallenge.pluck(:number_of_steps).min_by { |x| (x.to_f - target_to_set_for_the_user).abs }
    next_challenge = MonthlyChallenge.find_by(number_of_steps: closest_number_of_steps)
    if options[:fake].nil?
      UserInformationDispatch.create(user_id: @user.id, information: 'Challenge completed', count: 1) unless (@user.last_completed_challenge.challenge.type.eql? 'WeeklyChallenge')
      user_activity = UserActivity.create(user: @user, challenge: next_challenge, start_time: Time.now, status: "Progress")
      Notification.connection_started_challenge(@user)
      user_activity.number_of_days.times do |n|
        user_activity.daily_activities.create(start_time: user_activity.start_time + n.day)
      end
    else
      [next_challenge, @user.last_completed_challenge.start_time + 28.days]
    end
  end

  def get_feedback_of_user
    UserFeedback.new(@user).feedback
  end

  def get_percentage_completed_of_last_challenge
    @user.last_completed_challenge.total_steps_covered/@user.last_completed_challenge.goal.to_f
  end

  def get_profile_of_user
    @user.profile # and calculate the challenge based on it and find the number of steps based on it.
  end
end

