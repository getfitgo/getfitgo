class Identity < ActiveRecord::Base
  belongs_to :user

  def self.find_with_omniauth(auth)
  	if auth['provider'] != "fitbit" && auth['provider'] != "moves"
      find_by provider: auth['provider'], uid: auth['uid'].to_s
    else
      find_by provider: auth['provider'], token: auth["credentials"]["token"].to_s
    end
  end

  def self.create_with_omniauth(auth)
    create(uid: auth['uid'].to_s, provider: auth['provider'], auth_token: auth['credentials']['token'], name: auth[:info][:name], username: auth[:info][:nickname], image: auth[:info][:image], email: auth[:info][:email])
  end
end
