class TeamActivity < ActiveRecord::Base
  belongs_to :team
  belongs_to :challenge
  validates :team, presence: true
  validates :challenge, presence: true
  validates_uniqueness_of :team_id, :scope => :challenge_id
  delegate :name, to: :challenge
  validates_uniqueness_of :team_id, :scope => :challenge_id
 
  def goal
    total_goal = 0
    team.members.each do |member|
      user_activity = UserActivity.find_by(user: member, challenge: challenge)
      start_date_of_user_activity = user_activity.start_time
      difference_between_the_start_days = start_time.day - start_date_of_user_activity.day
      individual_goal = challenge.number_of_steps * difference_between_the_start_days
      total_goal += individual_goal
    end
    return total_goal
  end

  def start_challenge
    start_challenge_for_each_member
    send_mail_to_all_team_members
    set_email_notification_to_be_sent
  end

  def start_challenge_for_each_member
    members = team.members
    members.each do |member|
      UserActivity.create(user: member, challenge: challenge, start_time: start_time)
    end
  end

  def send_mail_to_all_team_members
    members = team.members
    members.each do |member|
      Mailer.delay.send_email_to_all_team_members(member)
    end
  end

  def set_email_notification_to_be_sent
    members = team.members
    members.each do |member|
      Mailer.delay_until(start_time - 1.day).set_email_notification_to_be_sent(member)
    end
  end

  def self.with_rankings
    self.sort { |x, y| y.goal <=> x.goal }
  end

  def progress
    start_day = start_time.day
    current_day = Time.now.day
    difference_in_days = current_day - start_day
    target_till_now = difference_in_days * challenge.number_of_steps
    target_till_now = 0
    team.members.each do |member|
      user_activity = UserActivity.find_by(user: member, challenge: challenge)
      start_day = user_activity.start_time.day
      difference_in_days = start_time.day - current_day
      target_till_now = difference_in_days * challenge.number_of_steps
      target_till_now += target_till_now
    end
    target_members_achieved = 0
    team.members do |members|
      target_members_achieved += UserActivity.find_by(user: member, challenge: challenge).number_of_steps
    end
    [target_till_now, target_members_achieved]
  end
end
