class User < ActiveRecord::Base
  has_surveys
  acts_as_messageable
  has_many :posts, as: :subject
  has_many :activities
  has_one :fitness_level
  has_one :fitness,through: :fitness_level
  has_many :friendships
  has_many :friends, :through => :friendships,  :conditions => ['friendships.confirmed = ?',true]
  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user,:conditions => ['friendships.confirmed = ?',true]
  # has_many :mutual_friends,
  #        :through => :inverse_friendships,
  #        :source => :user,
  #         :conditions => ["friendships.user_id = ? and friendships.friend_id = ?",self.id]
  # has_many :pending_friends, :through => :friendships, :source => :friend, :conditions => "confirmed = false"
  has_one :user_level
  acts_as_voter
  has_many :achievements, :dependent => :destroy
  has_many :points
  has_many :user_tasks
  # include PublicActivity::Model
  # tracked
  has_many :badges, through: :achievements
  #, :through => :levels  has_many :levels
  has_many :events, :dependent => :destroy
  belongs_to :team
  has_many :teams, through: :memberships
  has_many :memberships
  has_many :user_activities
  has_many :challenges, through: :user_activities
  has_many :identities
  has_many :daily_activities, through: :user_activities
  scope :current_challenge, -> {  user_activities.find_by(status: "Progress") } # TODO: Remove this as a scope
  has_many :posts, dependent: :destroy
  validates :user_name, presence: true
  validates :foot, :allow_nil => true, :numericality => {:greater_than => 0, :less_than_or_equal_to => 8}
  validates :inches, :allow_nil => true, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 12}
  has_one :user_profile, dependent: :destroy
  has_one :profile, through: :user_profile
  has_one :corporate_account, foreign_key: 'user_id', class_name: "CorporateMember", dependent: :destroy
  has_many :roles, through: :corporate_account, source: :roles, dependent: :destroy

  scope  :confirmed_users, ->  { all.order(confirmed_at: :desc) - where.not(:invitation_created_at => nil).where(:invitation_accepted_at => nil) }
  scope :active_users, -> { joins(:daily_activities).where.not("daily_activities.steps_covered IS NULL").order('daily_activities.updated_at DESC') } # TODO: This does not return unique users, call to_a.uniq! on this scope

  delegate :number_of_days, to: :challenge

  after_create :create_fitness_level # TODO: Should be called only after sign up confirmation
  # after_update :challenge_transition_trigger

  has_attached_file :photo, :styles => {:medium => "300x300>", :thumb => "100x100>"},
    :default_url => "/tshirt blue.png",
    :storage => :s3,
    :s3_credentials => {
    :access_key_id => "AKIAJF255ZNBUOTPZRRA",
    :secret_access_key => "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"
  },
    :bucket => ENV['S3_BUCKET_NAME'],
    :path => ":attachment/:id"

  # Validate the attached image is image/jpg, image/png, etc
  # validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  after_post_process :change_photo_user_task

  devise :invitable, :database_authenticatable, :registerable, :validatable, :confirmable,
    :recoverable, :rememberable, :trackable, :validatable
  def mutual_friends
    (friends + inverse_friends).uniq
  end

  def create_fitness_level_user
    create_fitness_level
  end

  # TODO: Not use object methods to support views and HTML, perhaps!

  def bmi options={}
    bmi = (weight/((self.foot*12+self.inches.to_i)*0.0254)**2).round(1) rescue nil
    bmi.nil? ? options[:true].present? ? 'Incomplete' : options[:pure].nil? ? "<i class='fa fa-exclamation-circle' rel='tooltip' title='Head over to BMI Calculator in Manage Account'></i>".html_safe : 32 : bmi
  end

  def award_badge badge_id, daily_activity_id
    achievement = achievements.create(badge_id: badge_id, daily_activity_id: daily_activity_id)
    achievement.errors.messages.empty?
  end

  def award_points(point, message)
    self.points.create(score: point, description: message)
    save
  end

  def set_average_steps_covered
    self.average_steps = steps_covered/(daily_activities.where.not(steps_covered: nil).size)
    save
  end

  def average_steps_covered
    average_steps || steps_covered/(daily_activities.where.not(steps_covered: nil).size) rescue nil
  end

  def last_week_average_steps_covered
    last_week_steps_covered = current_challenge.daily_activities.where.not(:steps_covered => nil).order("start_time desc").limit(7).map(&:steps_covered) rescue nil
    last_week_steps_covered.inject(:+)/last_week_steps_covered.count if last_week_steps_covered && (last_week_steps_covered.count.eql? 7)
  end

  def award_signup_bonus
    add_points(50, "Sign-up bonus!")
    change_points(50)
    # badge = Badge.find(1)
    # badge.add(self)
  end

  def add_points(new_points, event_string)
    change_points(new_points)
    # update_score_and_level(new_points)
    # log_event(new_points, event_string)
  end

  def weekly_challenge_over?
    # Assuming the validation of unique weekly challenge,
    # last challenge in self.challenges is the candidate for weekly challenge
    weekly_challenge = challenges.where(:number_of_days => 7).last
    if weekly_challenge.nil?
      true
    else
      user_activity = UserActivity.find_by(user: self, challenge: weekly_challenge, status: "Completed")
      user_activity.nil? ? false : true
    end

  end

  def total_calories_burnt options={}
    sum = calculate_sum(:calories, options)
  end

  def steps_covered options={}
    sum = calculate_sum(:steps_covered, options)
  end

  def calculate_sum(symbol,options = {})
    sum = 0
    if options.has_key?(:days) && (options[:days] != 0)
      case options[:days]
      when 1
        sum += daily_activities.where(start_time: Time.now - 1.days).first.send(symbol).to_i rescue 0 # nil.to_i = 0
      else
        daily_activities.where("daily_activities.start_time >= ?", Date.today - options[:days].days).each do |daily_activity|
          sum += daily_activity.send(symbol).to_i # nil.to_i = 0
        end
      end
    else
      sum += daily_activities.sum(symbol)
    end
    sum
  end

  def score
    points.sum(:score)
  end

  def participating_in_challenge(challenge)
    if self.challenges.include?(challenge)
      true
    else
      false
    end
  end

  def self.from_omniauth(auth)
    find_by(email: auth["info"]["email"]) || User.create_from_omniauth(auth)
  end

  # User class creates an object from omniauth using auth here and starts a 7 day warm up for there. This is too much responsibility.
  def self.create_from_omniauth(auth)
    new do |user|
      user.email = auth["info"]["email"]
      user.provider = auth["provider"]
      user.user_name = auth["info"]["name"]
      user.skip_confirmation!
      user.save!
      start_warmup(user)
    end
  end

  def self.start_warmup(user)
    level = Level.find_by(name: "Beginner")
    user.create_user_level(level: level)
    user.user_tasks.create(:description => "#{user.user_name} joined getfitgo and started a new fitness journey.")
    challenge = Challenge.create(name: "7-day warm up", category_id: 1, number_of_days: 7)
    user_activity = UserActivity.create(user: user, challenge: challenge, start_time: Time.now, status: "Progress")
    user_activity.number_of_days.times do |n|
      user_activity.daily_activities.create(start_time: user_activity.start_time + n.day)
    end
  end

  def email_required?
    super && provider.blank?
  end

  def password_required?
    super && provider.blank?
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

  def current_challenge
    user_activities.find_by(status: "Progress") || user_activities.last # TODO: Proper rescueing according to business logic when there is no challenge in progres
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first
    unless user
      user = User.create(user_name: data["name"],
                         email: data["email"],
                         password: Devise.friendly_token[0, 20]
                        )
    end
    user
  end

  def confirm_and_seed!
    level = Level.find_by(name: "Beginner")
    create_user_level(level: level)
    user_tasks.create(:description => "#{user_name} joined getfitgo and started a new fitness journey.")
    UserInformationDispatch.create(user_id: self.id, information: 'Update profile skipped', count: 1)
    UserInformationDispatch.create(user_id: self.id, information: 'Update profile completed', count: 1)
    UserInformationDispatch.create(user_id: self.id, information: 'Challenge transition successful', count: 1)
    UserInformationDispatch.create(user_id: self.id, information: 'Challenge transition unsuccessful', count: 2)
    UserInformationDispatch.create(user_id: self.id, information: 'Week 2 update', count: 1)
    UserInformationDispatch.create(user_id: self.id, information: 'Week 3 update', count: 1)
    UserInformationDispatch.create(user_id: self.id, information: 'Week 4 update', count: 1)
    category = Category.find_or_create_by(name: "Warmup")
    challenge = WeeklyChallenge.find_or_create_by(name: "7-day warm up", category: category, number_of_days: 7)
    Notification.warm_up_started(self)
    user_activity = UserActivity.create(user: self, challenge: challenge, start_time: Time.now, status: "Progress")
    user_activity.number_of_days.times do |n|
      user_activity.daily_activities.create(start_time: user_activity.start_time+n.day)
    end
  end

  def has_activity_data?
    self.daily_activities.count > self.daily_activities.where(:steps_covered => nil, :calories => nil).count
  end

  def last_completed_challenge
    user_activities.where(status: "Completed").order("start_time").last
  end

  def completed_challenges
    user_activities.where(status: "Completed")
  end

  # devise_invitable accept_invitation! method overriden
  def accept_invitation!
    # self.confirm!
    super
  end

  def has_achievements?
    achievements.any?
  end

  def last_won_badge
    achievements.includes(:badge).order(:created_at).last.badge
  end

  def last_streak_badge
    achievements.includes(:badge).order(created_at: :desc).each.map { |achievement| badge = achievement.badge; if badge.is_a? StreakBadge; return badge if badge.eligibility['days'].to_i > 1; end }.first
  end

  def days_since_last_streak
    achievements.where(badge_id: last_streak_badge.id).first.daily_activity.created_at.to_date rescue current_challenge.start_time.to_date
  end

  def eligible_streak_badges steps, days
    eligible_badges = []
    Badge.where("(eligibility -> 'days')::int > 1").each { |badge|
      if (badge.eligibility['steps'].to_i <= steps) && (badge.eligibility['days'].to_i <= days)
        if (Date.today - days_since_last_streak).to_i >= badge.eligibility['days'].to_i # TODO: Can be moved into StreakBadge logic and avoiding this method call
          eligible_badges << badge
        end
      end
    }
    eligible_badges
  end

  def min_next_streak_days
    Badge.where("(eligibility -> 'days')::int > 1") - badges
  end

  def is_user_property_nil?
    [self.foot, self.inches, self.weight].include? nil
  end

  def current_level
    user_level.level.name
  end

  def change_photo_user_task
    activities.where(subject_type: "UserTask").each do |task|
      task.update_attributes(image: photo)
    end
  end

  def connections
    friendships.where(confirmed:true)
  end

  def has_friend_requests?
    Friendship.where(friend_id: self.id, confirmed: false).count > 0
  end

  def goal
    current_challenge.challenge.number_of_steps
  end

  def progress
    if current_challenge.challenge.not_warm_up?
      (current_challenge.total_steps_covered.to_f/goal*100).round(0) rescue nil
    else
      (current_challenge.updated_daily_activities.count/7.0*100).round(0)
    end
  end

  def required_steps
    goal * ((Date.today - current_challenge.start_time.to_date).to_i) / current_challenge.challenge.number_of_days
  end

  def current_steps
    current_challenge.total_steps_covered
  end

  def pro_rated_target_progress
    (current_steps.to_f/required_steps*100).round(0) rescue 0
  end

  def steps_to_minutes options={}
    extra_steps = required_run_rate
    extra_steps = 7571 if extra_steps > 7571
    extra_minutes = extra_steps/126.2
    extra_minutes = (extra_minutes/5.0).ceil * 5
    options[:pure].nil? ? extra_minutes + current_challenge.challenge.number_of_minutes : extra_minutes
  end

  def steps_to_kms options={}
    kms = (current_steps * 0.7 / 1000).round(0)
    options[:marathon] ? "#{(kms/42.195*100).round(0)}%" : kms
  end

  def total_steps_to_kms options={}
    (steps_covered(options) * 0.7 / 1000).round(0)
  end

  # TODO: Damn you rescue
  def total_required_run_rate
    [((goal - current_steps).to_f/(current_challenge.days_left + 1) rescue (goal - current_steps)).round(0), 0].max
  end

  def required_run_rate
    [((required_steps - current_steps)/(current_challenge.days_left + 1) rescue required_steps),0].max
  end

  def current_run_rate
    current_steps / (current_challenge.day_of_challenge - 1) rescue current_steps
  end

  def height
    (foot or inches) ? "#{foot}' #{inches}\"" : "<i class='fa fa-exclamation-circle' rel='tooltip' title='Head over to BMI Calculator in Manage Account'></i>".html_safe
  end

  def weight_in_kg
    weight ? "#{weight} kgs." : "<i class='fa fa-exclamation-circle' rel='tooltip' title='Head over to BMI Calculator in Manage Account'></i>".html_safe
  end

  def last_updated_daily_activity
    daily_activities.order(:updated_at).where.not(steps_covered: nil).last
  end

  def is_confirmed?
    confirmed_at.present?
  end

  def best_average_steps
    current_challenge.daily_activities.where.not(steps_covered: nil).order(steps_covered: :desc).first.try(:steps_covered)
  end

  def best_average_steps_times
    (steps_remaining.to_f/best_average_steps).ceil rescue 0
  end

  def current_target
    goal
  end

  def steps_remaining
    (goal - current_challenge.total_steps_covered)
  end

  def update_fitness(score)
    fitness = Fitness.where("point <= ?", score).order("point").last
    fitness_level.update_attributes(fitness: fitness)
  end

  def times_full_marathon?
    kms = steps_to_kms
    (kms/42.195).round(1)
  end

  def samosas_burnt?
    weight ||= 70
    kCal = 0.0004885 * current_steps * weight
    (kCal/300).round(0)
  end

  def male?
    gender.eql? 'male'
  end

  def self.current
    RequestStore.store[:user]
  end
  def self.current=(user)
    RequestStore.store[:user] = user
  end

  def age options={}
    Date.today.year - birth_date.to_date.year rescue nil
  end

  def profile
    super
    super.nil? ? UserProfile.profile_user(self) : UserProfile.reprofile_user(self, super)
  end

  def self.sort_community_by_activity
    community = []
    community << [active_users.to_a, (confirmed_users - active_users)]
    community.flatten!
    community-= [nil] # TODO: Fix this bug
    community & community
  end

  def leaderboard_position?
    challenge = current_challenge.challenge
    participating_users_id = challenge.user_activities.where(status: 'Progress').pluck(:user_id)
    leaderboard_position = User.find(participating_users_id).sort_by { |user| -user.current_run_rate }.index(self) + 1
    leaderboard_position.ordinalize
  end

  def number_of_participating_users?
    UserActivity.where(challenge: current_challenge.challenge).count
  end

  def tag_type
    user_tag = ['Generic', 'Pets']
    user_tag << 'Office' if (age >= 25) && male? rescue nil
    user_tag << 'Family/kids' if (age >= 35) rescue nil
    user_tag
  end

  def interests
    if Survey::Attempt.all.find_by(participant:self)
      return Survey::Attempt.all.find_by(participant:self).answers.where(question_id:Survey::Question.find_by_text("My Favourties activities include (We know are sure you have more than one favourite):")).map {|a| a.option.text }
    else
      return []
    end
  end

  def challenge_transition_trigger
    profile.nil? ? nil : current_challenge.update_status
  end

  def friends_in_monthly_challenges
    (inverse_friends.joins(:challenges).where("challenges.type = 'MonthlyChallenge'") + friends.joins(:challenges).where("challenges.type = 'MonthlyChallenge'")).uniq!.count rescue 0
  end

  def has_role? roles
    corporate_account ? corporate_account.roles.where(name: roles).present? : false
  end

  def self.invite_team_admin!(attributes={}, invited_by=nil)
    self.invite!(attributes, invited_by) do |invitable|
      invitable.invitation_instructions = :team_admin_invitation_instructions
    end
  end

  def has_role? roles
    corporate_account ? corporate_account.roles.where(name: roles).present? : false
  end

  def self.invite_team_admin!(attributes={}, invited_by=nil)
     self.invite!(attributes, invited_by) do |invitable|
     invitable.invitation_instructions = :team_admin_invitation_instructions
    end
  end

  def unread_notifications
    Mailboxer::Notification.order(priority: :asc).where(id: Mailboxer::Receipt.where(receiver_id: id, is_read: false).pluck(:notification_id)).where(header: [false, true])
  end

  def unexpired_notifications
    Mailboxer::Notification.order(priority: :asc).where(id: Mailboxer::Receipt.where(receiver_id: id).pluck(:notification_id)).where('expires > ?', Time.now).where(header: false)
  end

  def new_notifications
    Mailboxer::Notification.order(priority: :asc).where(id: Mailboxer::Receipt.where(receiver_id: id, is_read: false, created_at: [1.minute.ago..Time.now]).pluck(:notification_id)).where(header: [false, true])
  end

  def first_name
    user_name.split(' ').first rescue email
  end

  def first_transition_day
    confirmed_at + 7.days
  end

  def find_or_send_message user, body, subject, sanitize=true, attachment=nil
    receipt = Mailboxer::Receipt.joins(:notification).where(receiver_id: user.id).where('mailboxer_notifications.subject = ? AND mailboxer_notifications.body = ?', subject, body)
    receipt.empty? ? receipt = [Notification.sender_user.send_message(user, body, subject, sanitize, attachment)] : receipt.first.notification.update_attributes(expires: 1.day.from_now)
    receipt.first
  end

  def data_not_updated_for days
    activity_data = daily_activities.where(start_time: [(days+1).days.ago...Date.yesterday], steps_covered: nil)
    activity_data.count.eql?(days)
  end

  def best_performance
    daily_activities.where.not(steps_covered: nil).order(steps_covered: :desc).first
  end

  def consistent_performer?
    activity_data = daily_activities.where(start_time: [8.days.ago...Date.yesterday]).where.not(steps_covered: nil)
    daily_goal = current_challenge.daily_goal
    activity_data.count.eql?(7) && !activity_data.pluck(:steps_covered).any? { |steps| steps < 0.8*daily_goal }
  end

  def slow_performance? date
    activity_data = daily_activities.where(start_time: [(date-1.day)..date]).where.not(steps_covered: nil)
    daily_goal = current_challenge.daily_goal
    activity_data.count.eql?(2) && !activity_data.pluck(:steps_covered).any? { |steps| steps > 0.7*daily_goal }
  end
end
