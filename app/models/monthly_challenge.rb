class MonthlyChallenge < Challenge
	has_many :challenge_associations
	has_many :weekly_challenges,class_name: "WeeklyChallenge",through: :challenge_associations, :order => 'challenge_associations.position'
end