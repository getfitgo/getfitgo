class UserActivity < ActiveRecord::Base
  include ActionDispatch::Routing::UrlFor
  include Rails.application.routes.url_helpers
  after_create :post_creation

  def post_creation
    users = [user] + user.mutual_friends
    users.each do |friend|
      if challenge.number_of_steps
        create_feed_activity(friend)
      end
    end
  end

  def create_feed_activity(friend)
    Activity.create(
      subject: user,
      name: "#{ActionController::Base.helpers.link_to(user.user_name, user_path(user.id))} has started a new training plan to walk #{challenge.number_of_steps} steps in #{challenge.number_of_days/7} weeks.",
      user: friend,
      visible: true
    )
  end

  validates_uniqueness_of :user_id, {:scope => [:challenge_id, :status]}
  belongs_to :challenge
  belongs_to :user
  has_many :daily_activities, dependent: :destroy
  delegate :category, to: :challenge
  delegate :name, to: :challenge
  delegate :number_of_days, to: :challenge

  has_and_belongs_to_many :insights, join_table: 'insight_user_activities'

  def total_steps_covered
    daily_activities.sum(:steps_covered)
  end

  def target_for_today
    current_week.number_of_steps/7
  end

  def current_week
    challenge.challenge_associations.find_by(position:user.current_challenge.daily_activities.where('start_time < ?',Time.now).order(:start_time).count/7).weekly_challenge
  end

  def challenge_status
    if self.start_time
      date = self.start_time.to_date
      if self.start_time < Time.now
        "Started on #{date.strftime("#{date.day.ordinalize} %B, %Y")}"
      else
        "Starts on #{date.strftime("#{date.day.ordinalize} %B, %Y")}"
      end
    else
      "Please choose the start date of the challenge."
    end
  end

  def total_calories_burnt
    daily_activities.sum(:calories)
  end

  def over?
    if start_time
      if (start_time + challenge.number_of_days.days).to_date < Time.now.to_date
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def goal
    challenge.number_of_steps
  end

  def activity_status
    update_status
    self.status
  end

  def daily_goal
    challenge.number_of_steps/challenge.number_of_days
  end

  def update_status
    if start_time
      if completed?
        status = "Completed"
        update_attributes(status: status)
        ChallengeTransition.new(user).assign_next_challenge
      else
        status = "Progress"
        update_attributes(status: status)
      end
    end
  end

  def completed?
    challenge.number_of_days <= (Date.today - start_time.to_date).to_i && user.profile
    # daily_activities.each do |d|
    #   if d.steps_covered.nil?
    #     return false
    #   end
    # end
    # return true
  end

  def in_progress?
    if completed?
      return false
    end
    if start_time
      day_difference = (Time.now - start_time)/1.day
      if day_difference.to_i > challenge.number_of_days || (Time.now - start_time) < 0
        return false
      end
    else
      return false
    end
    true
  end

  def average
    # ((total_steps_covered.to_f / (1 + date_difference.to_i)).round(0)) rescue 0
    ((total_steps_covered.to_f / updated_daily_activities.count).round(0)) rescue 0
  end

  def date_difference
    daily_activities.where.not(:steps_covered => nil).last.start_time.to_date - start_time.to_date
  end

  def start_challenge
    UserActivity.find_by(user: user, challenge: challenge).update_attributes(start_time: Time.now)
  end

  def last_activity
    updated_daily_activities.last
  end

  def updated_daily_activities
    daily_activities.where.not(:steps_covered => nil).order("start_time")
  end

  def days_left
    [challenge.number_of_days - (Date.today - start_time.to_date).to_i - 1, 0].max
  end

  def dates
    active_daily_activities = daily_activities
    [active_daily_activities.first.start_time, active_daily_activities.last.start_time]
  end

  def day_of_challenge
    (Date.today - start_time.to_date).to_i + 1
  end

  def extend_challenge
    challenge.number_of_days += 1
    challenge.save
    daily_activities.create(start_time: Time.now)
  end

  def target_achieved?
    challenge.not_warm_up? ? (user.current_steps >= challenge.number_of_steps ? 1 : 0) : 0
  end

  def total_steps_covered_in_last_week
    daily_activities.where.not(steps_covered: nil).where(start_time: [(Date.today - 7.days)...Date.today]).sum(:steps_covered)
  end

  def achievements_unlocked
    user.achievements.joins(:daily_activity).where('daily_activities.start_time BETWEEN ? AND ?', start_time.to_date, (start_time + 27.days).to_date)
  end

  def points_earned
    user.points.where(created_at: [(start_time + 1.day)..(start_time + 28.days)]).sum(:score)
  end
end
