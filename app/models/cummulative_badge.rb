class CummulativeBadge < Badge
  def self.badge_eligible_for?(user,daily_activity)
    badge_eligible = []
    self.all.each do |badge|
      if user.steps_covered > badge.eligibility['total_steps'].to_i
        badge_eligible << badge
      end
    end
    badge_eligible.empty? ? NullBadge.instance : badge_eligible
  end

  def self.award_badge_to_user(user,daily_activity)
    badge_awarded, already_owned = [], []
    badge_eligible = badge_eligible_for?(user,daily_activity)
    unless badge_eligible.class.eql? NullBadge
      badge_eligible.each { |badge|
        if Badge.award_badge(badge.id, badge.points, user, daily_activity.id)
          badge_awarded << badge
        else
          already_owned << badge
        end
      }
    end

    [badge_awarded, already_owned]
  end

  def details achiever, date_achieved=nil, current_user=nil
    date_achieved.nil? ? "#{name} badge can be earned for completing #{eligibility['total_steps']} steps." : "#{achiever} earned this badge on #{date_achieved.strftime("#{date_achieved.day.ordinalize} %B, %Y")} for completing #{eligibility['total_steps']} steps.#{if current_user.eql?(current_user);' Keep going!';end}"
  end
end
