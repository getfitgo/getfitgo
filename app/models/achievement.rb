class Achievement < ActiveRecord::Base
  belongs_to :user
  belongs_to :badge
  belongs_to :daily_activity
  validates_uniqueness_of :user_id, :scope => [:badge_id]
  after_create :create_activities

  def create_activities
    Activity.create(
        subject: user,
        name: "#{user.user_name} earned the '#{badge.name} badge'",
        user: user
    )
  end

  def achievement_date
    daily_activity.start_time rescue nil
  end
end
