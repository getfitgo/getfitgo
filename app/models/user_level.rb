class UserLevel < ActiveRecord::Base
  belongs_to :user
  belongs_to :level

  after_update :create_activities

  def create_activities
    if level
      old_level = Level.where("score < ?", level.score).order("score").last
      if old_level
        Activity.create(
            subject: user,
            name: "#{user.user_name} has advanced from a '#{old_level.name}' to a '#{level.name}' on getfitgo.",
            user: user
        )
      end
    end
  end

  def self.assign_level_to(user, points)
    level = Level.where("score <= ?", points).order("score").last
    if user.user_level.level.score < level.score
      user.user_level.update_attributes(level: level)
    end
  end
end
