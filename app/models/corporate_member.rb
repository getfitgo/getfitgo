class CorporateMember < ActiveRecord::Base
  belongs_to :corporate
  belongs_to :user
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles, dependent: :destroy
end
