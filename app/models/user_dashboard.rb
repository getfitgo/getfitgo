class UserDashboard
  attr_reader :user
  def initialize(user)
    @user = user
  end

  # if (@user.daily_activities.where.not(steps_covered: nil).size) > 0
  #   @users_below_me = User.where("average_steps<?", @user.average_steps_covered).order("average_steps desc").limit(5)
  #   @users_above_me = User.where("average_steps>?", @user.average_steps_covered).order("average_steps").limit(5)
  # end

  def daily_activity
    da = @user.daily_activities.order(:start_time).where('daily_activities.start_time <= ?', Time.now.to_date).where(steps_covered: nil).first
    da.nil? ? DailyActivity.new : da
  end

  def teams_joined_by_me
    @user.teams.includes(:admin)
  end

  def completed_challenges
    UserActivity.where(user: @user, status: "Completed")
  end

  def challenges_to_start
    UserActivity.where(user: @user, status: "Start")
  end

  def team_invited_to
    Team.find(@user.invited_to_team_id) if @user.invited_to_team_id
  end

  def daily_activities
    @user.daily_activities.where("daily_activities.start_time < ?", Time.now.to_date)
  end

  def leader
    User.all
  end

  def challenges
    if @user.challenges.count == 0
      challenges = Challenge.where(number_of_days: 7)
    else
      challenges = Challenge.where(number_of_days: 30)
    end
    challenges
  end

  def random_feel_image
    blacklist = [".", ".."]
    file_names = Dir.entries('public/reasons')
    blacklist.each do |blacklisted|
      file_names.delete(blacklisted)
    end
    "/reasons/#{file_names.shuffle.first}"
  end

  def unread_notifications
    notifications = @user.unread_notifications + @user.unexpired_notifications
    notifications.first.update_attributes(expires: 1.day.from_now) if notifications.present? && notifications.first.expires.nil?
    notifications
  end
end
