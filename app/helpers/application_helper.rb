module ApplicationHelper
  def avatar user
    if user.photo.url == "/photos/original/missing.png"
      image_tag "/tshirt blue.png", :alt => '', :class => 'avatarImg'
    else
      image_tag user.photo.url(:medium), :alt => '', :class => 'avatarImg'
    end
  end

  def avatar_email user
    image_url = "#{ENV['HOST']}/tshirt%20blue.png"
    if user.photo.url == "/tshirt%20blue.png"
      image_url = "#{ENV['HOST']}/tshirt%20blue.png"
      image_tag image_url, :alt => '', :class => 'avatarImg'
    else
      image_tag user.photo.url(:medium), :alt => '', :class => 'avatarImg'
    end
  end

  def background_image page_url
    if ['/users/login'].any? { |x| page_url.include?(x) }
      background_image = '/homeback.jpg'
    elsif ['/users/invitation/accept'].any? { |x| page_url.include?(x) }
      background_image = '/homeback.jpg'
    elsif ['/registrations/welcome_page'].any? { |x| page_url.include?(x) }
      if is_mobile_device?
        background_image = '/threebg.JPG'
      else
        background_image = '/never_too_late.jpg'
      end
    end
    "background: url(#{background_image}) no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size:cover;" if background_image.present?
  end
end
