module UserActivitiesHelper
  # def show_next_challenge
  #   Category.create(name:"Fitness")
  #   challenge1 = Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 150000,level:"Easy",category_id:1)
  #   challenge2 = Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 150000,level:"Easy",category_id:1)
  #   UserActivity.create(user:current_user,challenge: challenge1,level:"Basic")
  #   UserActivity.create(user:current_user,challenge: challenge2,level:"Upper")
  # end

  def show_next_challenge(current_user)
    weekly_challenge = current_user.challenges.where(number_of_days: 7).first
    user_activity = UserActivity.find_by(user: current_user, challenge: weekly_challenge)
    steps_covered_by_user = user_activity.steps_covered
    basic_challenge, _ = get_target_for_user(steps_covered_by_user)
    Category.create(name: "Fitness")
    challenge1 = Challenge.find_or_create_by(name: "Walking Challenge", number_of_days: 30, number_of_steps: basic_challenge, level: "Easy", category_id: 1)
    user_activity = UserActivity.find_or_create_by(user: current_user, challenge: challenge1, start_time: Time.now)
    user_activity.number_of_days.times do |n|
      user_activity.daily_activities.create(start_time: user_activity.start_time+n.day)
    end
    [user_activity]
  end

  def get_target_for_user(steps_covered_by_user)
    case steps_covered_by_user
      when 3001..5000
        basic_challenge = 100000
        advance_challenge = 200000
        return [basic_challenge, advance_challenge]
      when 5001..8000
        basic_challenge = 200000
        advance_challenge = 300000
        return [basic_challenge, advance_challenge]
      when 8001..10000
        basic_challenge = 270000
        advance_challenge = 300000
        return [basic_challenge, advance_challenge]
      when 10001..12500
        basic_challenge = 300000
        advance_challenge = 400000
        return [basic_challenge, advance_challenge]
      when 12500..15000
        basic_challenge = 400000
        advance_challenge = 500000
        return [basic_challenge, advance_challenge]
    end
  end
end
