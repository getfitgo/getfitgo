module DashboardHelper

  def challenge_title challenge, user
    if challenge.not_warm_up?
      if current_user.eql? user
        'My 4-Week Training Plan'
      else
        user.user_name.split(' ').first + "'s 4-Week Training Plan"
      end
    else
      'Warm up <span class="hidden-xs">to</span> <span class="hidden-xs" style="color:#00c8c8">get</span><span class="hidden-xs" style="color:#fa9600">fit</span><span class="hidden-xs" style="color:#00c8c8">go</span>'.html_safe

    end
  end

  def challenge_details challenge, view_challenge
    if challenge.not_warm_up?
      (link_to 'View challenge', challenge, :style => 'float: right') if view_challenge
    else
      (link_to 'View warm up', introduction_path, :style => 'float: right') if view_challenge
    end if challenge.participating_users.include? current_user
  end

  def steps_to_minutes steps
    minutes = steps/126.2
    minutes = (minutes/5.0).ceil * 5
  end
end
