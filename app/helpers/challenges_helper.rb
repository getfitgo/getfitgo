module ChallengesHelper
  # def join_challenge(challenge)
  #   if signed_in?
  #     if current_user.participating_in_challenge(challenge)
  #     else
  #       link_to "Join Challenge", challenge_join_path(@challenge), method: :post
  #     end
  #   else
  #     link_to "Join Challenge", challenge_join_path(@challenge), method: :post
  #   end
  # end

  def goal(challenge)
    "The goal of this challenge is to walk #{challenge.number_of_steps} steps in #{challenge.number_of_days} days"
  end

  def message_for_warmup user
    today = Date.today
    current_challenge = user.current_challenge
    daily_activities = current_challenge.updated_daily_activities.count
    challenge_start_date = current_challenge.start_time
    days_in_warmup = (today - challenge_start_date.to_date).to_i

    case days_in_warmup
    when 0
        ""
    when (1..5)
        '<ul class="">
          <li> <i class=""></i> Get familiar, track your daily fitness activity with your tracker and update it to getfitgo </li>
          <li> <i class=""></i> Experts will track your fitness and recommend personalized challenges for you. </li>
          <li> <i class=""></i> You’ll start an exciting 4-Week Training Plan after your warm up period. </li>
        </ul>'

    when (6..7)
        case daily_activities
          when 0
            "You haven't updated your fitness activity for any day. <i class='fa fa-meh-o'></i><br/><br/>We need your activity data to track your fitness and plan an exciting 4-Week Training Plan just for you."
          when (1...4)
            "You’re almost through with your 7-day warm up.<br/<br/>We hope you've been carrying your activity tracker with you. With more activity data our experts will be able to evaluate your fitness level better and recommend a 4-Week Training Plan best suited for you. <i class='fa fa-wrench'></i>"
          when (4...7)
            "You’re almost through with your 7-day warm up.<br/<br/>Your personal trainer has been tracking your fitness level and is planning an exciting 4-Week Training Plan for you.<br/>Your quest will start on #{(challenge_start_date + 7.days).strftime('%d/%m/%Y')}. Get ready! This will be fun. <i class='fa fa-thumbs-up'></i>"
         end
    when (8..11)
      case daily_activities
      when (0..3)
        'We hope you’ve been carrying your device and tracking your daily fitness. We’d like to know a little more about your daily activities, so our trainers can plan a walking challenge just for you. <i class="fa fa-wrench"></i><br/<br/>In case you’ve missed updating your data, your device stores data for 15 days. You could enter data for previous days too.<br/><br/>Ensure that you update your activity in your <span style="color:#00c8c8">get</span><span style="color:#fa9600">fit</span><span style="color:#00c8c8">go</span> dashboard.'
      when (4..7)
        "Welcome to your first Training Plan. All the best! <i class='fa fa-smile-o'></i>"
      end
    else
      "We noticed you haven’t updated your daily activity. We don’t have enough activity data to recommend the right 4-Week Training Plan for you. <i class='fa fa-frown-o'></i><br/><br/>We suggest you update your daily activity for at least 5 days for our trainers to evaluate and recommend the right challenge.<br/>Your device stores data for 15 days. You can update the data anytime and get started."
    end
  end

  def progress_bar_color relative_progress
    if relative_progress > 100*0.75
      'success'
    elsif relative_progress < 80*0.75
      'danger'
    else
      'warning'
    end
  end
end

