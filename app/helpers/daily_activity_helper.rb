module DailyActivityHelper
  def daily_activity_chart_data
    (@get_current_challenge_of_current_user.start_time.to_date..(Date.today-1.day)).map do |date|
      {
        start_time: date,
        steps_covered: current_user.daily_activities.find_by(start_time: date).steps_covered
      }
    end
  end

  def date_value daily_activity, user
    begin
      (user.last_updated_daily_activity.start_time + 1.day).strftime('%d-%m-%Y')
    rescue
      user.daily_activities.where(steps_covered: nil).first.start_time.strftime('%d-%m-%Y')
    end
  end

  def date_min daily_activity
    if daily_activity.new_record?
      Time.now.strftime('%d-%m-%Y')
    else
      daily_activity.start_time.strftime('%d-%m-%Y')
    end
  end

  def date_max daily_activity
    (Time.now-1.day).strftime('%d-%m-%Y')
  end

  def date_excluded daily_activity
    [*date_min(daily_activity).to_date..date_max(daily_activity).to_date] - current_user.daily_activities.where(steps_covered: nil).pluck(:start_time)
  end
end
