module UsersHelper
  def report user, options={}
    activities = user.daily_activities.where.not(steps_covered: nil).order(start_time: :asc)

    unless activities.empty?
      case options[:days]
      when '0'
        start_time ||= activities.first.start_time.to_date
      else
        start_time = Date.today - options[:days].to_i.days
      end

      report_activities = activities.where(start_time: (start_time..Date.today)).pluck(:start_time,:steps_covered, :calories,:feel)

      report_activities.each.map do |activity_data|
        {
          start_time: activity_data[0],
          steps_covered: activity_data[1],
          calories: activity_data[2],
          feel: activity_data[3] || ''
        }
      end
    end
  end

  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.user_name, class: "gravatar img-circle")
  end

  def display_picture user, location
    case location
    when 'profile'
      width, height, style, top = 64, 64, 'thumbnail', '0', '0'
    when 'leaderboard'
      width, height, style, top, right = 40, 40, 'circle', '0', '10'
    when 'community'
      width, height, style, top, bottom = 48, 48, 'circle', '0', '10'
    end

    if user.photo.url == "/photos/original/missing.png"
      image_tag "/walking_man.png", :size => "#{width}x#{height}", :style => "margin-top:#{top}px;margin-right:#{right}px;margin-bottom:#{bottom}px;"
    else
      image_tag user.photo.url(:medium), :size => "#{width}x#{height}", :style => "margin-top:#{top}px;margin-right:#{right}px;margin-bottom:#{bottom}px;", :class => "img-#{style}"
    end
  end
end
