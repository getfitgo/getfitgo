module PostsHelper

  def recursive_child_comment_subtree key
    # Base case for leaf notes
    if @comment_threads[key].nil?
      return nil
    else
      @comment_threads[key].map { |child_comment|
        { child_comment => recursive_child_comment_subtree(child_comment.id) }
      }
    end
  end

  def display_threaded_comments commentable
    comments_to_be_displayed = []
    @comment_threads = commentable.comment_threads.order(created_at: :asc).group_by(&:parent_id)
    @comment_threads[nil].each { |parent_comment|
      comments_to_be_displayed << { parent_comment => recursive_child_comment_subtree(parent_comment.id) }
    } unless @comment_threads.empty? # nil key means they are parent comments
    comments_to_be_displayed
  end
end
