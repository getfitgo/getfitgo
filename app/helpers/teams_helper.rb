module TeamsHelper
  def join_team(team)
    if team.members.include? current_user
    else
      link_to "Join Team", team_join_path(team), method: :post
    end
  end
end
