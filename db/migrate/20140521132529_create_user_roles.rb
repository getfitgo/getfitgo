class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :corporate_members_roles do |t|
      t.belongs_to :corporate_member, index: true
      t.belongs_to :role, index: true
      t.timestamps
    end
  end
end
