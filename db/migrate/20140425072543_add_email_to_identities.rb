class AddEmailToIdentities < ActiveRecord::Migration
  def self.up
    add_column :identities, :email, :string
  end
end
