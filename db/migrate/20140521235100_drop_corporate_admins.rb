class DropCorporateAdmins < ActiveRecord::Migration
  def self.up
    drop_table :corporate_admins
  end
end
