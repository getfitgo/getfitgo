class AddInformationIndexToUserInformationDispatch < ActiveRecord::Migration
  def up
    add_index :user_information_dispatches, :information
  end
end
