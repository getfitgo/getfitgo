class CreateJoinTableInsightQualification < ActiveRecord::Migration
  def change
    create_table :insights_qualifications, :id => false do |t|
      t.integer :insight_id
      t.integer :qualification_id
    end
    add_index :insights_qualifications, :insight_id
    add_index :insights_qualifications, :qualification_id
  end
end
