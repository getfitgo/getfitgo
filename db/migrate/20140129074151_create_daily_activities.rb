class CreateDailyActivities < ActiveRecord::Migration
  def change
    create_table :daily_activities do |t|
      t.string :steps_covered
      t.timestamps
    end
  end
end
