class AddPositionToChallengeAssociations < ActiveRecord::Migration
  def change
    add_column :challenge_associations, :position, :integer
  end
end
