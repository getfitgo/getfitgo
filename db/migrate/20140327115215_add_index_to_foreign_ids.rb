class AddIndexToForeignIds < ActiveRecord::Migration
  def self.up
    add_index :challenges, :category_id
    add_index :events, :user_id
    add_index :daily_activities, :user_activity_id
    add_index :friendships, :friend_id
    add_index :invitations, :sender_id
    # add_index :levels, :badge_id
    add_index :points, :user_id
    add_index :teams, :user_id
    add_index :users, :invitation_id
    add_index :users, [:invited_by_id, :invited_by_type]
  end
end
