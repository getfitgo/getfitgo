class CreateCorporates < ActiveRecord::Migration
  def change
    create_table :corporates do |t|
      t.string :name
      t.string :url
      t.text :message
      t.boolean :status
      t.date :start_date
      t.date :end_date
      t.integer :max_employees
      t.timestamps
    end
  end
end
