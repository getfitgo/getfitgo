class RemoveStepsAndDaysAndAddEligibilityToBadges < ActiveRecord::Migration
  def self.up
    remove_column :badges, :steps
    remove_column :badges, :days
    add_column :badges, :eligibility, :hstore
    add_index :badges, :eligibility, using: 'gin'
  end

  def self.down
  end
end
