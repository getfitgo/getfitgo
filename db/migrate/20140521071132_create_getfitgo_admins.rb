class CreateGetfitgoAdmins < ActiveRecord::Migration
  def self.up
    create_table :getfitgo_admins , :id => false do |t|
      t.integer :user_id
      t.timestamps
    end
  end
end
