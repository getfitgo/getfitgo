class AddFeelToDailyActivity < ActiveRecord::Migration
  def change
    add_column :daily_activities, :feel, :string
  end
end
