class ChangeUidTypeText < ActiveRecord::Migration
  def self.up
  	change_column :identities, :uid, :text
  end
end
