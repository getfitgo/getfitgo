class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.text :message

      t.timestamps
    end
  end
end
