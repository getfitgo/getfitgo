class AddDisplayNameToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :display_name, :string
  end
end
