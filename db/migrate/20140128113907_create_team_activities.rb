class CreateTeamActivities < ActiveRecord::Migration
  def change
    create_table :team_activities do |t|
      t.belongs_to :team, index: true
      t.belongs_to :challenge, index: true

      t.timestamps
    end
  end
end
