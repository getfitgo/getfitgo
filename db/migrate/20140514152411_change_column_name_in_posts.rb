class ChangeColumnNameInPosts < ActiveRecord::Migration
  def self.up
   change_column :posts, :name, :text
  end
end
