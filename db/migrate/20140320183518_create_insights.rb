class CreateInsights < ActiveRecord::Migration
  def change
    create_table :insights do |t|
      t.integer :priority
      t.hstore :rule
      t.references :action

      t.timestamps
    end
    add_index :insights, :rule, using: 'gin'
    add_index :insights, :action_id
  end
end
