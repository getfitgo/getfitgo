class AddIndexOnUserActivities < ActiveRecord::Migration
  def change
    add_index :user_activities, [:user_id, :challenge_id]
  end
end
