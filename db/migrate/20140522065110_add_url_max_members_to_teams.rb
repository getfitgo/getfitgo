class AddUrlMaxMembersToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :url, :string
    add_column :teams, :max_members, :integer
    add_column :teams, :status, :string
  end
end
