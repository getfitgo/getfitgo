class CreateCorporateAdmins < ActiveRecord::Migration
def self.up
    create_table :corporate_admins do |t|
      t.belongs_to :corporate, index: true
      t.belongs_to :user, index: true
      t.string :designation

      t.timestamps
    end
  end
end
