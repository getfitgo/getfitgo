class AddUniqueIndexOnTeamActivity < ActiveRecord::Migration
  def change
    def change
      remove_index :team_activities, [:team_id, :challenge_id]
      add_index :team_activities, [:team_id, :challenge_id], unique: true
    end
  end
end
