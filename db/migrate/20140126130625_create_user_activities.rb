class CreateUserActivities < ActiveRecord::Migration
  def change
    create_table :user_activities do |t|
      t.belongs_to :user
      t.belongs_to :challenge
      t.datetime :start_time
      t.datetime :end_time
      t.integer :steps_covered
    end
  end
end
