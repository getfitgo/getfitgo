class AddNumberOfStepsAndNumberOfDaysToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :number_of_steps, :integer
    add_column :challenges, :number_of_days, :integer
  end
end
