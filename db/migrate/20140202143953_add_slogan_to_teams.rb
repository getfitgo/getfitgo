class AddSloganToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :slogan, :text
  end
end
