class AddNumberOfMinutesToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :number_of_minutes, :integer
  end
end
