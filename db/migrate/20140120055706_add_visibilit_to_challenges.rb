class AddVisibilitToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :visibility, :string, default: "Public"
  end
end
