class CreateTableActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.timestamps null: false
      t.integer :subject_id, null: false
      t.string :subject_type, null: false
      t.string :name, null: false
      t.integer :user_id, null: false
    end
    add_index :activities, :subject_id
    add_index :activities, :subject_type
    add_index :activities, :user_id
  end
end
