class CreateCorporateTeams < ActiveRecord::Migration
  def change
    create_table :corporate_teams do |t|
      t.belongs_to :corporate, index: true
      t.belongs_to :team, index: true

      t.timestamps
    end
  end
end
