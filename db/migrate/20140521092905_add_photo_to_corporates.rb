class AddPhotoToCorporates < ActiveRecord::Migration
  def self.up
    add_attachment :corporates, :photo
  end

  def self.down
    remove_attachment :corporates, :photo
  end
end
