class AddPreviousBadgeAndNextBadgeToBadges < ActiveRecord::Migration
  def self.up
    add_column :badges, :previous_badge_id, :integer
    add_column :badges, :next_badge_id, :integer
  end
end
