class AddNameAndUsernameToIdentities < ActiveRecord::Migration
  def change
    add_column :identities, :name, :string
    add_column :identities, :username, :string
    add_column :identities, :image, :string
  end
end
