class CreateCorporateChallenges < ActiveRecord::Migration
  def change
    create_table :corporate_challenges do |t|
      t.belongs_to :corporate, index: true
      t.belongs_to :challenge, index: true

      t.timestamps
    end
  end
end
