class CreateUserLevels < ActiveRecord::Migration
  def change
    create_table :user_levels do |t|
      t.references :user, index: true
      t.references :level, index: true

      t.timestamps
    end
  end
end
