class AddDailyActivityIdToAchievements < ActiveRecord::Migration
  def self.up
    add_column :achievements, :daily_activity_id, :integer
    add_index :achievements, :daily_activity_id
  end

  def self.down
    remove_index :achievements, :daily_activity_id
    remove_column :achievements, :daily_activity_id
  end
end
