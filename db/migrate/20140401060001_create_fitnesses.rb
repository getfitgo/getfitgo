class CreateFitnesses < ActiveRecord::Migration
  def change
    create_table :fitnesses do |t|
      t.string :name
      t.integer :point
      t.integer :number_of_stars
      t.timestamps
    end
  end
end
