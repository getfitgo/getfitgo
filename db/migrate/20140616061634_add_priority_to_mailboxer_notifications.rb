class AddPriorityToMailboxerNotifications < ActiveRecord::Migration
  def change
    add_column :mailboxer_notifications, :priority, :integer
  end
end
