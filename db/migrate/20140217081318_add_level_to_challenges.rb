class AddLevelToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :level, :string
  end
end
