class AddHeaderToMailboxerNotifications < ActiveRecord::Migration
  def change
    add_column :mailboxer_notifications, :header, :boolean, default: false
  end
end
