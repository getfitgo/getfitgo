class AddPointsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :points, :integer
  end
end
