class AddVisibilityToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :visibility, :string
  end
end
