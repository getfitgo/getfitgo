class AddInvitedToTeamIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :invited_to_team_id, :integer
  end
end
