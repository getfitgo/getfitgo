class AddDeletedToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :deleted, :boolean, default: false
  end
end
