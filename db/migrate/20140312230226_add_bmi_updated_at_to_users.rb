class AddBmiUpdatedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bmi_updated_at, :timestamp
  end
end
