class AddDefaultValueToNameInCategories < ActiveRecord::Migration
  def self.up
    change_column :categories, :name, :string, :default => "Fitness"
  end
end
