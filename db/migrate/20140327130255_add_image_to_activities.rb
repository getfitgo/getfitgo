class AddImageToActivities < ActiveRecord::Migration
  def self.up
    add_attachment :activities, :image
  end

  def self.down
    remove_attachment :activities, :image
  end
end
