class AddSlugToCorporates < ActiveRecord::Migration
  def change
    add_column :corporates, :slug, :string
    add_index :corporates, :slug, unique: true
  end
end
