class AddIndexToMailboxerConversations < ActiveRecord::Migration
  def change
    add_index :mailboxer_conversations, :subject
  end
end
