class CreateInsightUserActivities < ActiveRecord::Migration
  def change
    create_table :insight_user_activities do |t|
      t.integer :insight_id
      t.integer :user_activity_id
      t.timestamps
    end
    add_index :insight_user_activities, :insight_id
    add_index :insight_user_activities, :user_activity_id
  end
end
