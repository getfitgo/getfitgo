class AddProfileNumberToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :profile_number, :integer
  end
end
