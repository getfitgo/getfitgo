class AddSourceToDailyActivities < ActiveRecord::Migration
  def change
    add_column :daily_activities, :source, :string
  end
end
