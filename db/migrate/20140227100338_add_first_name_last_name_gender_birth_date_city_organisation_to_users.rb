class AddFirstNameLastNameGenderBirthDateCityOrganisationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :gender, :string
    add_column :users, :birth_date, :date
    add_column :users, :city, :string
    add_column :users, :organisation, :string
    add_column :users, :foot, :integer
    add_column :users, :inches, :integer
    add_column :users, :weight, :integer
  end
end
