class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :name
      t.integer :user_id
      t.boolean :visible
      t.boolean :deleted
      t.timestamps
    end
    add_index :posts, [:user_id, :created_at]
  end
end
