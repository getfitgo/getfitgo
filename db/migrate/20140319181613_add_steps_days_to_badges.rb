class AddStepsDaysToBadges < ActiveRecord::Migration
  def self.up
    add_column :badges, :steps, :integer
    add_column :badges, :days, :integer
  end
end
