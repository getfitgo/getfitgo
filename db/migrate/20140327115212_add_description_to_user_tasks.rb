class AddDescriptionToUserTasks < ActiveRecord::Migration
  def change
    add_column :user_tasks, :description, :string
  end
end
