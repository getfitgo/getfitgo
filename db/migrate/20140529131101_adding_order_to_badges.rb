class AddingOrderToBadges < ActiveRecord::Migration
  def self.up
    add_column :badges, :achievement_order, :integer
    add_index :badges, :achievement_order
  end

  def self.down
    remove_index :badges, :achievement_order
    remove_column :badges, :achievement_order
  end
end
