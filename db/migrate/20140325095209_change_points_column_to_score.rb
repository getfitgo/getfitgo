class ChangePointsColumnToScore < ActiveRecord::Migration
  def change
    rename_column :points, :points, :score
  end
end
