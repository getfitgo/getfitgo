class AddStartTimeToDailyActivities < ActiveRecord::Migration
  def change
    add_column :daily_activities, :start_time, :datetime
  end
end
