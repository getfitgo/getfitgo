class AddIndexesToComments < ActiveRecord::Migration
  def self.up
    add_index :comments, :parent_id
    add_index :comments, :lft
    add_index :comments, :rgt
  end
end
