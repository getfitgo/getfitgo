class AddSkipToSurveyAnswers < ActiveRecord::Migration
  def change
    add_column :survey_answers,:skip,:boolean
  end
end
