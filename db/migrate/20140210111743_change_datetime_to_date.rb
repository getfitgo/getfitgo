class ChangeDatetimeToDate < ActiveRecord::Migration
  def self.up
    change_column :daily_activities, :start_time, :date
  end

  def self.down
    change_column :daily_activities, :start_time, :datetime
  end
end
