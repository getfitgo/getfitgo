class AddUserActivityIdToDailyActivities < ActiveRecord::Migration
  def change
    add_column :daily_activities, :user_activity_id, :integer
  end
end
