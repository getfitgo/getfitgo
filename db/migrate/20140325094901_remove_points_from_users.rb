class RemovePointsFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :points
  end
end
