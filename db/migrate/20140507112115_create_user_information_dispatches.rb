class CreateUserInformationDispatches < ActiveRecord::Migration
  def change
    create_table :user_information_dispatches do |t|
      t.integer :user_id
      t.string :information
      t.integer :count
      t.timestamps
    end
    add_index :user_information_dispatches, :user_id
  end
end
