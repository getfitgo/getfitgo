class ChangePointsColumnToFloat < ActiveRecord::Migration
  def self.up
    change_column :users,:points,:float
    change_column :events,:points,:float
  end
end
