class AddStartTimeToTeamActivities < ActiveRecord::Migration
  def change
    add_index :team_activities, [:team_id, :challenge_id], unique: true
    add_column :team_activities, :start_time, :datetime
  end
end
