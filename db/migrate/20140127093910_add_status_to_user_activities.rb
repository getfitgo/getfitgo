class AddStatusToUserActivities < ActiveRecord::Migration
  def change
    add_column :user_activities, :status, :string
  end
end
