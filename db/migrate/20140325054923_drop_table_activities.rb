class DropTableActivities < ActiveRecord::Migration
  def self.up
    drop_table :activities
  end
end
