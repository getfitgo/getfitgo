class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.hstore :assessment
      t.timestamps
    end
    add_index :profiles, :assessment, using: 'gin'
  end
end
