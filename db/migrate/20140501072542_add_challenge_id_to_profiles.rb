class AddChallengeIdToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :challenge_id, :integer
    add_index :profiles, :challenge_id
  end
end
