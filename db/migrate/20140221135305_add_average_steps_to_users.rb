class AddAverageStepsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :average_steps, :integer
  end
end
