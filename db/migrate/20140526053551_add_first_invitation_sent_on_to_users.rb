class AddFirstInvitationSentOnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_invitation_sent_on, :datetime
  end
end
