class RemoveDateFieldsFromUserActivities < ActiveRecord::Migration
  def self.up
    remove_column :user_activities,:day1
    remove_column :user_activities,:day2
    remove_column :user_activities,:day3
    remove_column :user_activities,:day4
    remove_column :user_activities,:day5
    remove_column :user_activities,:day6
    remove_column :user_activities,:day7
    remove_column :user_activities,:day8
    remove_column :user_activities,:day9
    remove_column :user_activities,:day10
    remove_column :user_activities,:day11
    remove_column :user_activities,:day12
    remove_column :user_activities,:day13
    remove_column :user_activities,:day14
    remove_column :user_activities,:day15
    remove_column :user_activities,:day16
    remove_column :user_activities,:day17
    remove_column :user_activities,:day18
    remove_column :user_activities,:day19
    remove_column :user_activities,:day20
    remove_column :user_activities,:day21
    remove_column :user_activities,:day22
    remove_column :user_activities,:day23
    remove_column :user_activities,:day24
    remove_column :user_activities,:day25
    remove_column :user_activities,:day26
    remove_column :user_activities,:day27
    remove_column :user_activities,:day28
    remove_column :user_activities,:day29
    remove_column :user_activities,:day30
  end
end
