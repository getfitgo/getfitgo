class UpdateUniquenessIndexOnUserActivities < ActiveRecord::Migration
  def self.up
    remove_index :user_activities, [:user_id, :challenge_id]
    add_index :user_activities, [:user_id, :challenge_id, :status]
  end
end
