class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.integer :profile_id
      t.integer :user_id
      t.timestamps
    end
    add_index :user_profiles, :profile_id
    add_index :user_profiles, :user_id
  end
end
