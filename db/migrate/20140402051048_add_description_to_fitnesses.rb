class AddDescriptionToFitnesses < ActiveRecord::Migration
  def change
    add_column :fitnesses, :description, :text
  end
end
