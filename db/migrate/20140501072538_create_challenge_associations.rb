class CreateChallengeAssociations < ActiveRecord::Migration
  def change
    create_table :challenge_associations do |t|
      t.belongs_to :monthly_challenge, index: true
      t.belongs_to :weekly_challenge, index: true

      t.timestamps
    end
  end
end
