class AddTagToActions < ActiveRecord::Migration
  def change
    add_column :actions, :tag, :string
    add_index :actions, :tag
  end
end
