class AddColumnsToPoints < ActiveRecord::Migration
  def change
    add_column :points, :points, :integer
    add_column :points, :description, :string
    add_column :points, :user_id, :integer
  end
end
