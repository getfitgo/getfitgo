class CreateFitnessLevels < ActiveRecord::Migration
  def change
    create_table :fitness_levels do |t|
      t.belongs_to :user, index: true
      t.belongs_to :fitness, index: true
      t.timestamps
    end
    add_index :fitness_levels, [:user_id,:fitness_id]
  end
end
