class AddIndexTrueToAchievements < ActiveRecord::Migration
  def change
    remove_index :achievements, column: [:user_id, :badge_id]
    add_index :achievements, [:user_id, :badge_id], unique: true
  end
end
