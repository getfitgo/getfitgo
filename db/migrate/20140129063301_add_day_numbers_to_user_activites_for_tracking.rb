class AddDayNumbersToUserActivitesForTracking < ActiveRecord::Migration
  def self.up
    add_column :user_activities,:day1,:integer
    add_column :user_activities,:day2,:integer
    add_column :user_activities,:day3,:integer
    add_column :user_activities,:day4,:integer
    add_column :user_activities,:day5,:integer
    add_column :user_activities,:day6,:integer
    add_column :user_activities,:day7,:integer
    add_column :user_activities,:day8,:integer
    add_column :user_activities,:day9,:integer
    add_column :user_activities,:day10,:integer
    add_column :user_activities,:day11,:integer
    add_column :user_activities,:day12,:integer
    add_column :user_activities,:day13,:integer
    add_column :user_activities,:day14,:integer
    add_column :user_activities,:day15,:integer
    add_column :user_activities,:day16,:integer
    add_column :user_activities,:day17,:integer
    add_column :user_activities,:day18,:integer
    add_column :user_activities,:day19,:integer
    add_column :user_activities,:day20,:integer
    add_column :user_activities,:day21,:integer
    add_column :user_activities,:day22,:integer
    add_column :user_activities,:day23,:integer
    add_column :user_activities,:day24,:integer
    add_column :user_activities,:day25,:integer
    add_column :user_activities,:day26,:integer
    add_column :user_activities,:day27,:integer
    add_column :user_activities,:day28,:integer
    add_column :user_activities,:day29,:integer
    add_column :user_activities,:day30,:integer
  end
end
