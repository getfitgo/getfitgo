class AddLevelToUserActivities < ActiveRecord::Migration
  def change
    add_column :user_activities, :level, :string
  end
end
