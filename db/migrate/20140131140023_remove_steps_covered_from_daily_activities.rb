class RemoveStepsCoveredFromDailyActivities < ActiveRecord::Migration
  def self.up
    remove_column :daily_activities,:steps_covered
    add_column :daily_activities, :steps_covered, :integer,limit: 4
  end
end
