class CreateQualifications < ActiveRecord::Migration
  def change
    create_table :qualifications do |t|
      t.integer :priority
      t.hstore :qualification

      t.timestamps
    end
    add_index :qualifications, :qualification, using: 'gin'
  end
end
