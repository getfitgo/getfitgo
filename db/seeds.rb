Category.delete_all
Category.create(name: "Warmup")
Category.create(name:"30 Day Challenge")
monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 309480, type:"MonthlyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 56700 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 60480 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 64260 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 68040 + 15000, type:"WeeklyChallenge")

monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 264120, type:"MonthlyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 45360 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 49140 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 52920 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 56700 + 15000, type:"WeeklyChallenge")

monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 218760, type:"MonthlyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 34020 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 37800 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 41580 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 45360 + 15000, type:"WeeklyChallenge")

monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 173400, type:"MonthlyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 22680 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 26460 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 30240 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 34020 + 15000, type:"WeeklyChallenge")

monthly_challenge = MonthlyChallenge.create(number_of_days:28, number_of_steps: 131820, type:"MonthlyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 15120 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 15120 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 18900 + 15000, type:"WeeklyChallenge")
monthly_challenge.weekly_challenges.create(number_of_days:7, number_of_steps: 22680 + 15000, type:"WeeklyChallenge")

# # # @valid_params = {} #User required attributes

# # # [[300,[*100...3000]],[600,[*3000...5000]],[80,[*5000...8000]],[15,[*8000...10000]],[5,[*10000...12500]]].each{|a|
# # #   a[0].times do |i|
# # #     @valid_params[:average_steps] = a[1].sample
# # #       user = User.new(user_name:Faker::Internet.user_name,email:Faker::Internet.email,password:"testtest",password_confirmation:"testtest")
# # #      user.skip_confirmation!
# # #      user.update_attributes(@valid_params)
# # #   end
# # # }

# # #Challenge.delete_all

# # @valid_params = {} #User required attributes

# # [[300,[*100...3000]],[600,[*3000...5000]],[80,[*5000...8000]],[15,[*8000...10000]],[5,[*10000...12500]]].each{|a|
# #   a[0].times do |i|
# #     @valid_params[:average_steps] = a[1].sample
# #       user = User.new(user_name:Faker::Internet.user_name,email:Faker::Internet.email,password:"testtest",password_confirmation:"testtest")
# #      user.skip_confirmation!
# #      user.update_attributes(@valid_params)
# #   end
# # }


# #Challenge.delete_all

# Category.delete_all

# Category.create(name: "Fitness")
# # # # Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 150000,level:"Easy",category_id:1)
# # # # Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 500000,level:"Very Hard",category_id:1)
# # # # Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 300000,level:"Medium",category_id:1)
# # # # Challenge.create(name:"Walking Challenge", number_of_days: 30, number_of_steps: 400000,level:"Hard",category_id:1)
# # # # Challenge.create(name:"Walking Challenge",number_of_days:7,number_of_steps:35000,level:"Easy",category_id:1)
# # # # Challenge.create(name:"Walking Challenge",number_of_days:7,number_of_steps:50000,level:"Medium",category_id:1)
# # # # Challenge.create(name:"Walking Challenge",number_of_days:7,number_of_steps:70000,level:"Hard",category_id:1)
# # # # Challenge.create(name:"Walking Challenge",number_of_days:7,number_of_steps:80000,level:"Very Hard",category_id:1)

# TODO: These need to be run twice to set next_badge also as they haven't been created yet
# MilestoneBadge
[['Starter','','Rookie',{:days=>1, :steps=>3000}],
['Rookie','Starter','Debutant',{:days => 1, :steps => 5000}],
['Debutant','Rookie','Breakthrough',{:days => 1, :steps => 7500}],
['Breakthrough','Debutant','High Flyer',{:days => 1, :steps => 10000}],
['High Flyer','Breakthrough','Big Wig',{:days => 1, :steps => 12500}],
['Big Wig','High Flyer','Master Blaster',{:days => 1, :steps => 15000}],
['Master Blaster','Big Wig','',{:days => 1, :steps => 20000}]].each_with_index { |badge, index|
  b = MilestoneBadge.find_or_create_by_name(badge[0])
  b.previous_badge = Badge.find_by_name(badge[1])
  b.next_badge = Badge.find_by_name(badge[2])
  b.eligibility = badge[3]
  file = File.open("public/#{badge[0]}.png")
  b.image = file
  b.save
}

# StreakBadge
[['Three peat Silver','','Three peat Gold',{:days=>3, :steps=>5000}],
['Three peat Gold','Three peat Silver','On a Roll',{:days=>3, :steps=>10000}],
['On a Roll','Three peat Gold','Super Blitz',{:days=>5, :steps=>10000}],
['Super Blitz','Three peat Gold','Out of the World',{:days=>5, :steps=>12000}],
['Out of the World','Super Blitz','Shooting Star',{:days=>7, :steps=>10000}],
['Shooting Star','Out of the World','',{:days=>10, :steps=>12000}]].each_with_index { |badge, index|
  b = StreakBadge.find_or_create_by_name(badge[0])
  b.previous_badge = Badge.find_by_name(badge[1])
  b.next_badge = Badge.find_by_name(badge[2])
  b.eligibility = badge[3]
  file = File.open("public/#{badge[0]}.png")
  b.image = file
  b.save
}

# CummulativeBadge
[['100k',100,100000],['200k',150,200000],['500k',150,500000],['1Million',200,1000000]].each_with_index { |badge, index|
  file = File.open("public/#{badge[0]}.png")
  CummulativeBadge.create!(
      name: badge[0],
      image: file,
      display_name: badge[0],
      points: badge[1],
      eligibility: {total_steps: badge[2]}
  )
}

Level.delete_all
Level.create(name: "Beginner", score: 0)
Level.create(name: "Scout", score: 1000)
Level.create(name: "Ranger", score: 2000)
Level.create(name: "Strider", score: 4000)
Level.create(name: "Champion", score: 10000)
Level.create(name: "Ace", score: 15000)
Level.create(name: "Olympian", score: 40000)
Level.create(name: "Hero", score: 100000)
Level.create(name: "SuperHero", score: 150000)


my_survey = Survey::Survey.new do |survey|
  survey.name = "Lifestyle check"
  survey.description = "Take the lifestyle survey to help our experts know you better. Get personalized tips and challenges."
  survey.attempts_number = 100
  survey.active = true
end

# Let's add some questions and options
question_1 = Survey::Question.new do |question|
  question.text = 'On average, how many days each week do you accumulate at least 30 minutes of physical activity / exercise over the day?'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => '2-4 days',  :weight => 1),
    Survey::Option.new(:text => '5 days or more', :weight => 2),
    Survey::Option.new(:text => 'None to 1', :weight => 0, correct:false)
  ]
end

question_2 = Survey::Question.new do |question|
  question.text = 'How long do you normally exercise?'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => '<15 minutes', :weight => 0,correct:false),
    Survey::Option.new(:text => '15-30 minutes',     :weight => 1),
    Survey::Option.new(:text => '>30 minutes',       :weight => 2)
  ]
end
question_3 = Survey::Question.new do |question|
  question.text = 'If a friend gave you five free sessions at a gym / dance class / sporting club, would you'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => 'Pass, ignore it or give it to someone else ', :weight => 0,correct:false),
    Survey::Option.new(:text => "Try it out five times just so you don't hurt your friend's feelings", :weight => 1),
    Survey::Option.new(:text => 'Go five times and then keep it up ', :weight => 2)
  ]
end

question_4 = Survey::Question.new do |question|
  question.text = 'Can you touch your toes without bending your knees while sitting on the floor?'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => 'Yes ', :weight => 2),
    Survey::Option.new(:text => 'No',     :weight => 0,correct:false)
  ]
end

question_6 = Survey::Question.new do |question|
  question.text = 'In the recent past, have you experienced any body pain or discomfort during a physical activity?'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => 'Yes', :weight => 0,correct:false),
    Survey::Option.new(:text => 'No',     :weight => 0,correct:false)
  ]
end

question_8 = Survey::Question.new do |question|
  question.text = 'On a typical day do you'
  question.has_one_option = true
  question.options = [
    Survey::Option.new(:text => 'Spend most of your day standing or moving around', :weight => 2),
    Survey::Option.new(:text => 'Spend most of your time sitting', :weight => 0,correct:false),
    Survey::Option.new(:text =>  'Most of the time sitting with a few small walks in between', :weight => 1)
  ]
end

question_7 = Survey::Question.new do |question|
  question.text = "My favourite activities include (we're sure you have more than one favourite):"

  question.options = [
    Survey::Option.new(:text => 'Walking', :weight => 1),
    Survey::Option.new(:text => 'Jogging' ,:weight => 1),
    Survey::Option.new(:text => 'Running', :weight => 1),
    Survey::Option.new(:text => 'Cycling' ,:weight => 1),
    Survey::Option.new(:text => 'Cricket' ,:weight => 1),
    Survey::Option.new(:text => 'Football', :weight => 1),
    Survey::Option.new(:text => 'Basketball', :weight => 1),
    Survey::Option.new(:text => 'Volleyball', :weight => 1),
    Survey::Option.new(:text => 'Tennis' ,:weight => 1),
    Survey::Option.new(:text => 'Badminton', :weight => 1),
    Survey::Option.new(:text => 'Squash', :weight => 1),
    Survey::Option.new(:text => 'Golf', :weight => 1),
    Survey::Option.new(:text => 'Swimming', :weight => 1),
    Survey::Option.new(:text => 'Yoga' ,:weight => 1),
    Survey::Option.new(:text => 'Aerobics',:weight => 1),
    Survey::Option.new(:text => 'Zumba Dancing',:weight => 1),
    Survey::Option.new(:text => 'Dancing' ,:weight => 1),
    Survey::Option.new(:text => 'Aerobics',:weight => 1),
    Survey::Option.new(:text => 'Strength Training' ,:weight => 1),
    Survey::Option.new(:text => 'Martial Arts' ,:weight => 1),
    Survey::Option.new(:text => 'Trekking',:weight => 1),
    Survey::Option.new(:text => 'Other',:weight => 1)
  ]
end

my_survey.questions = [question_8, question_1, question_2,question_3,question_4,question_6,question_7]
my_survey.save!

Fitness.create(name:"SUPER FIT",point:10,number_of_stars:5,description:"Excellent. You are an active individual who lives healthy. Your fitness regimen would serve you well. Use getfitgo to choose your medium / long term wellness goals and monitor your progress while fulfilling your wellness journey. Cheers!")
Fitness.create(name:"GOING STRONG",point:7,number_of_stars:4,description:"Very good. Your lifestyle has many active moments to it and you are on the right path to leading a healthy life. Use getfitgo to lock in your medium term health goals and track yourself regularly. Our panel of experts will constantly provide relevant insights and guidance to help you get better each day.")
Fitness.create(name:"LOTS TO WORK ON",point:0,number_of_stars:1,description:"Taking the first step is perhaps the biggest challenge and you have crossed that barrier. getfitgo will help you set goals and then take small incremental steps each day to reach your target. Our expert panel is waiting to help you through your journey.")
Fitness.create(name:"GETTING THERE",point:2,number_of_stars:2,description:"Relax. While you have work to do, you are not alone. Lifestyle change does not happen overnight. Pursuing your wellness journey on getfitgo is one step towards making the changes required. Ease your way through various challenges and levels while focusing on being consistent at what you do. Best Wishes!")
Fitness.create(name:"FIT",point:4,number_of_stars:3,description:"Good. You have many indicators of a healthy lifestyle and would be well served by having a more holistic view of your wellbeing. Our fitness experts will help you take your fitness to the next level by providing constant feedback on making the right changes in your lifestyle.")
# User.delete_all
# user1 = User.create(user_name:"a",email:"a@a.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# user1.confirm!
# user2 = User.create(user_name:"a",email:"b@b.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# user2.confirm!
# user3 = User.create(user_name:"a",email:"c@c.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# user3.confirm!
# user4 = User.create(user_name:"a",email:"d@d.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# user4.confirm!
# user5 = User.create(user_name:"a",email:"e@e.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# user5.confirm!
# user6 = User.create(user_name:"a",email:"f@f.com",password:"aaaaaaaa",password_confirmation:"aaaaaaaa")
# # user1.confirm!
# # user2.confirm!
# # user3.confirm!
# # user4.confirm!
# # user5.confirm!
#  user6.confirm!
# # Let's answer it with User 1
# attempt = Survey::Attempt.new(:survey => my_survey, :participant => User.first)
# answer_1 = Survey::Answer.new(:option => question_1.options.first )
# answer_2 = Survey::Answer.new(:option => question_2.options.first )
# attempt.answers = [answer_1, answer_2]
# attempt.save
# # Let's answer it with User 2
# attempt = Survey::Attempt.new(:survey => my_survey, :participant => User.last)
# answer_1 = Survey::Answer.new(:option => question_1.options.first )
# answer_2 = Survey::Answer.new(:option => question_2.options.last )
# attempt.answers = [answer_1, answer_2]
# attempt.save

# # Let's pull some metrics

# survey = Survey::Survey.active.first

# # select all the attempts from this survey
# survey_answers = survey.attempts

# # check the highest score for user 2
# user_2_highest_score  = survey_answers.for_participant(User.last).high_score
# puts user_2_highest_score


# #check the highest score made for this survey
# global_highest_score = survey_answers.high_score
# puts global_highest_score

# Profiles seed; refer models/concerns/profile_enum.rb
profile_number = 1
[[18,25],[26,35],[36,45],[46,55],[55,155]].each_with_index { |(age_min, age_max), x|
  ['0', '1', '2'].each_with_index { |lifestyle_survey, y|
    ['0', '1', '2'].each_with_index { |bmi, z|
      Profile.create(assessment: { lifestyle_survey: lifestyle_survey, bmi: bmi, age_min: age_min, age_max: age_max }, profile_number: profile_number)
      profile_number+=1
    }
  }
}

# Profile-Challenge mapping
Profile.where(profile_number: [1,10]).each { |profile|
  profile.challenge_id = Challenge.find_by_number_of_steps(309480).id
  profile.save
}
Profile.where(profile_number: [2,4,11,13]).each { |profile|
  profile.challenge_id = Challenge.find_by_number_of_steps(264120).id
  profile.save
}
Profile.where(profile_number: [3,5,7,12,14,16,19,20,21,22,23,25,28,29,30,31,32,34]).each { |profile|
  profile.challenge_id = Challenge.find_by_number_of_steps(218760).id
  profile.save
}
Profile.where(profile_number: [6,8,15,17,24,26,33,35,37,38,40,41]).each { |profile|
  profile.challenge_id = Challenge.find_by_number_of_steps(173400).id
  profile.save
}
Profile.where(profile_number: [9,18,27,36,39,42,43,44,45]).each { |profile|
  profile.challenge_id = Challenge.find_by_number_of_steps(131820).id
  profile.save
}

# Feedback messages
Action.create(message: "Remember, you don’t have to be great to start, but you have to start to be great. And your journey to fitness has just STARTED!\n\nDid you know that you have completed f33lg00dn@w_kms of a full marathon so far!", tag: 'feedback_1')
Action.create(message: "Looks like you need to start picking up. It's time to stretch your boundaries.\n\nThere are 1,440 minutes in a day. Use atleast f33lg00dn@w_minutes of them to walk around and you'll get closer to your target.", tag: 'feedback_2_1')
Action.create(message: "Keep up the spirit and step it up in the coming days. It's time you pick a notch higher!\n\nThere are 1,440 minutes in a day. Use atleast f33lg00dn@w_minutes of them to walk around and you'll get closer to your target.", tag: 'feedback_2_2')
Action.create(message: "Well done! You are well on your way to the fitness journey.\n\nThere are 1,440 minutes in a day. Use atleast f33lg00dn@w_minutes of them to walk around and you'll get closer to your fitness goal.", tag: 'feedback_2_3')
Action.create(message: "Kudos! That's a great start to the walking journey. Be proud, but never satisfied. Keep it going!", tag: 'feedback_2_4')
Action.create(message: "You are in the second half of your training plan. It's time to buck up! Your Personal Best so far is f33lg00dn@w_best steps/day. Try and achieve that thrice in the next 5 days.", tag: 'feedback_3_1')
Action.create(message: "You are in the second half of your training plan. It's time to buck up! Your Personal Best so far is f33lg00dn@w_best steps/day. Try and achieve that thrice in the next 5 days.", tag: 'feedback_3_2')
Action.create(message: "You are in the second half of your training plan. It's time to buck up! Your Personal Best so far is f33lg00dn@w_best steps/day. You are f33lg00dn@w_repeat PERSONAL BESTs away from the finish line.", tag: 'feedback_3_3')
Action.create(message: "You are in the second half of your training plan. Your Personal Best so far is f33lg00dn@w_best steps/day. You are f33lg00dn@w_repeat PERSONAL BESTs away from the finish line.", tag: 'feedback_3_4')
Action.create(message: "Looks like your performance has not been upto the mark. Deadline is right around the corner!!\n\nRemember, the pain of today is the victory of tomorrow!", tag: 'feedback_4_1')
Action.create(message: "Looks like you need to step it up a notch to be successful in the challenge. Deadline is right around the corner!!\n\nRemember, the pain of today is victory of tomorrow!", tag: 'feedback_4_2')
Action.create(message: "Bravo! You are well on your way to triumph! Deadline is right around the corner.\n\nAnything above f33lg00dn@w_target steps will earn you 20% extra points. Let's stretch beyond the target for some extra bounty.", tag: 'feedback_4_3')
Action.create(message: "Take a bow! That's some breathtaking performance so far. Deadline is right around the corner.\n\nAnything above f33lg00dn@w_target steps will earn you 20% extra points. Let's stretch beyond the target for some extra bounty.", tag: 'feedback_4_4')
Action.create(message: "You are almost at the end of your challenge. You only have three choices; Give up, give in, or give it all you have got.", tag: 'feedback_5_1')
Action.create(message: "You are almost at the end of your challenge. You only have three choices; Give up, give in, or give it all you have got.", tag: 'feedback_5_2')
Action.create(message: "You are almost at the end of your challenge. You currently stand f33lg00dn@w_leaderboard_position out of the f33lg00dn@w_participants_count people who participated in the challenge with you.", tag: 'feedback_5_3')
Action.create(message: "You are almost at the end of your challenge. You currently stand f33lg00dn@w_leaderboard_position out of the f33lg00dn@w_participants_count people who participated in the challenge with you.", tag: 'feedback_5_4')
Action.create(message: "Congratulations! You have reached your challenge target. Every step beyond your target gives you 20% EXTRA points!", tag: 'target_achieved_1')
Action.create(message: "Congratulations! You have reached your challenge target. Every step beyond your target gives you 20% EXTRA points! You currently stand f33lg00dn@w_leaderboard_position out of the f33lg00dn@w_participants_count people who participated in the challenge with you.", tag: 'target_achieved_2')

# Insight rules
Insight.create(rule: { activity_data: 5, day_of_challenge_min: 8 }, action: Action.find_by_tag('feedback_1'))
Insight.create(rule: { activity_data: 6, day_of_challenge_min: 8 }, action: Action.find_by_tag('feedback_1'))
Insight.create(rule: { activity_data: 7, day_of_challenge_min: 8 }, action: Action.find_by_tag('feedback_1'))
Insight.create(rule: { progress_max: 49, progress_min: 0, activity_data: 10, day_of_challenge_min: 11 }, action: Action.find_by_tag('feedback_2_1'))
Insight.create(rule: { progress_max: 79, progress_min: 50, activity_data: 10, day_of_challenge_min: 11 }, action: Action.find_by_tag('feedback_2_2'))
Insight.create(rule: { progress_max: 99, progress_min: 80, activity_data: 10, day_of_challenge_min: 11 }, action: Action.find_by_tag('feedback_2_3'))
Insight.create(rule: { progress_max: 199, progress_min: 100, activity_data: 10, day_of_challenge_min: 11 }, action: Action.find_by_tag('feedback_2_4'))
Insight.create(rule: { progress_max: 49, progress_min: 0, activity_data: 15, day_of_challenge_min: 16 }, action: Action.find_by_tag('feedback_3_1'))
Insight.create(rule: { progress_max: 79, progress_min: 50, activity_data: 15, day_of_challenge_min: 16 }, action: Action.find_by_tag('feedback_3_2'))
Insight.create(rule: { progress_max: 99, progress_min: 80, activity_data: 15, day_of_challenge_min: 16 }, action: Action.find_by_tag('feedback_3_3'))
Insight.create(rule: { progress_max: 199, progress_min: 100, activity_data: 15, day_of_challenge_min: 16 }, action: Action.find_by_tag('feedback_3_4'))
Insight.create(rule: { progress_max: 49, progress_min: 0, activity_data: 20, day_of_challenge_min: 21 }, action: Action.find_by_tag('feedback_4_1'))
Insight.create(rule: { progress_max: 79, progress_min: 50, activity_data: 20, day_of_challenge_min: 21 }, action: Action.find_by_tag('feedback_4_2'))
Insight.create(rule: { progress_max: 99, progress_min: 80, activity_data: 20, day_of_challenge_min: 21 }, action: Action.find_by_tag('feedback_4_3'))
Insight.create(rule: { progress_max: 199, progress_min: 100, activity_data: 20, day_of_challenge_min: 21 }, action: Action.find_by_tag('feedback_4_4'))
Insight.create(rule: { progress_max: 49, progress_min: 0, activity_data: 25, day_of_challenge_min: 26 }, action: Action.find_by_tag('feedback_5_1'))
Insight.create(rule: { progress_max: 79, progress_min: 0, activity_data: 25, day_of_challenge_min: 26 }, action: Action.find_by_tag('feedback_5_2'))
Insight.create(rule: { progress_max: 99, progress_min: 0, activity_data: 25, day_of_challenge_min: 26 }, action: Action.find_by_tag('feedback_5_3'))
Insight.create(rule: { progress_max: 199, progress_min: 0, activity_data: 25, day_of_challenge_min: 26 }, action: Action.find_by_tag('feedback_5_4'))
Insight.create(rule: { target_achieved: 1, activity_data_max: 19 }, action: Action.find_by_tag('target_achieved_1'))
Insight.create(rule: { target_achieved: 1, activity_data_max: 28 }, action: Action.find_by_tag('target_achieved_2'))

Qualification.create(qualification: {challenge_days_min: 8})

# TODO: Queries to run on next deployment

['getfitgo_admin', 'corporate_admin', 'corporate_member', 'getfitgo_member'].each{|role| Role.create(name: role)}

['Starter', 'Rookie', 'Debutant', 'Three peat Silver', 'Breakthrough', 'Three peat Gold','On a Roll', 'Out of the World', 'High Flyer', 'Super Blitz', 'Shooting Star', 'Big Wig', 'Master Blaster', '100k', '200k', '500k', '1Million'].each_with_index {|name, i|
  badge = Badge.find_by(name: name)
  badge.update_attributes(achievement_order: i)
}

# User.all.each do |user|
#   new_challenge = Challenge.where("steps_covered > ?",user.current_challenge.challenge.number_of_steps).sort(:number_of_steps).first
#   user.current_challenge.update_attributes(challenge:new_challenge)
# end
