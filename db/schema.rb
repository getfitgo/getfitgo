# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140619113114) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "achievements", force: true do |t|
    t.integer  "user_id"
    t.integer  "badge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "daily_activity_id"
  end

  add_index "achievements", ["daily_activity_id"], name: "index_achievements_on_daily_activity_id", using: :btree
  add_index "achievements", ["user_id", "badge_id"], name: "index_achievements_on_user_id_and_badge_id", unique: true, using: :btree

  create_table "actions", force: true do |t|
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tag"
  end

  add_index "actions", ["tag"], name: "index_actions_on_tag", using: :btree

  create_table "activities", force: true do |t|
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "subject_id",                            null: false
    t.string   "subject_type",                          null: false
    t.string   "name",                                  null: false
    t.integer  "user_id",                               null: false
    t.integer  "cached_votes_total",    default: 0
    t.integer  "cached_votes_score",    default: 0
    t.integer  "cached_votes_up",       default: 0
    t.integer  "cached_votes_down",     default: 0
    t.integer  "cached_weighted_score", default: 0
    t.boolean  "visible",               default: false
    t.boolean  "deleted",               default: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "activities", ["cached_votes_down"], name: "index_activities_on_cached_votes_down", using: :btree
  add_index "activities", ["cached_votes_score"], name: "index_activities_on_cached_votes_score", using: :btree
  add_index "activities", ["cached_votes_total"], name: "index_activities_on_cached_votes_total", using: :btree
  add_index "activities", ["cached_votes_up"], name: "index_activities_on_cached_votes_up", using: :btree
  add_index "activities", ["cached_weighted_score"], name: "index_activities_on_cached_weighted_score", using: :btree
  add_index "activities", ["subject_id"], name: "index_activities_on_subject_id", using: :btree
  add_index "activities", ["subject_type"], name: "index_activities_on_subject_type", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "badges", force: true do |t|
    t.string   "name"
    t.integer  "points"
    t.boolean  "default"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "display_name"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "previous_badge_id"
    t.integer  "next_badge_id"
    t.hstore   "eligibility"
    t.string   "type"
    t.integer  "achievement_order"
  end

  add_index "badges", ["achievement_order"], name: "index_badges_on_achievement_order", using: :btree
  add_index "badges", ["eligibility"], name: "index_badges_on_eligibility", using: :gin

  create_table "categories", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       default: "Fitness"
  end

  create_table "challenge_associations", force: true do |t|
    t.integer  "monthly_challenge_id"
    t.integer  "weekly_challenge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position"
  end

  add_index "challenge_associations", ["monthly_challenge_id"], name: "index_challenge_associations_on_monthly_challenge_id", using: :btree
  add_index "challenge_associations", ["weekly_challenge_id"], name: "index_challenge_associations_on_weekly_challenge_id", using: :btree

  create_table "challenges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "visibility",        default: "Public"
    t.integer  "number_of_steps"
    t.integer  "number_of_days"
    t.integer  "category_id"
    t.string   "level"
    t.string   "type"
    t.integer  "number_of_minutes"
  end

  add_index "challenges", ["category_id"], name: "index_challenges_on_category_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "commentable_id",   default: 0
    t.string   "commentable_type"
    t.string   "title"
    t.text     "body"
    t.string   "subject"
    t.integer  "user_id",          default: 0,     null: false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",          default: false
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["lft"], name: "index_comments_on_lft", using: :btree
  add_index "comments", ["parent_id"], name: "index_comments_on_parent_id", using: :btree
  add_index "comments", ["rgt"], name: "index_comments_on_rgt", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "corporate_challenges", force: true do |t|
    t.integer  "corporate_id"
    t.integer  "challenge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "corporate_challenges", ["challenge_id"], name: "index_corporate_challenges_on_challenge_id", using: :btree
  add_index "corporate_challenges", ["corporate_id"], name: "index_corporate_challenges_on_corporate_id", using: :btree

  create_table "corporate_members", force: true do |t|
    t.integer  "corporate_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",       default: false
  end

  add_index "corporate_members", ["corporate_id"], name: "index_corporate_members_on_corporate_id", using: :btree
  add_index "corporate_members", ["user_id", "corporate_id"], name: "index_corporate_members_on_user_id_and_corporate_id", unique: true, using: :btree
  add_index "corporate_members", ["user_id"], name: "index_corporate_members_on_user_id", using: :btree

  create_table "corporate_members_roles", force: true do |t|
    t.integer  "corporate_member_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "corporate_members_roles", ["corporate_member_id"], name: "index_corporate_members_roles_on_corporate_member_id", using: :btree
  add_index "corporate_members_roles", ["role_id", "corporate_member_id"], name: "index_corporate_members_roles_on_role_id_corporate_member_id", unique: true, using: :btree
  add_index "corporate_members_roles", ["role_id"], name: "index_corporate_members_roles_on_role_id", using: :btree

  create_table "corporate_suggested_challenges", force: true do |t|
    t.integer  "challenge_id"
    t.integer  "corporate_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "corporate_suggested_challenges", ["challenge_id"], name: "index_corporate_suggested_challenges_on_challenge_id", using: :btree
  add_index "corporate_suggested_challenges", ["corporate_id"], name: "index_corporate_suggested_challenges_on_corporate_id", using: :btree

  create_table "corporate_teams", force: true do |t|
    t.integer  "corporate_id"
    t.integer  "team_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "corporate_teams", ["corporate_id"], name: "index_corporate_teams_on_corporate_id", using: :btree
  add_index "corporate_teams", ["team_id"], name: "index_corporate_teams_on_team_id", using: :btree

  create_table "corporates", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "message"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "max_employees"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "deleted"
    t.string   "members_file_file_name"
    t.string   "members_file_content_type"
    t.integer  "members_file_file_size"
    t.datetime "members_file_updated_at"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
  end

  add_index "corporates", ["end_date"], name: "index_corporates_on_end_date", unique: true, using: :btree
  add_index "corporates", ["name"], name: "index_corporates_on_name", unique: true, using: :btree
  add_index "corporates", ["slug"], name: "index_corporates_on_slug", unique: true, using: :btree
  add_index "corporates", ["start_date"], name: "index_corporates_on_start_date", unique: true, using: :btree
  add_index "corporates", ["url"], name: "index_corporates_on_url", unique: true, using: :btree

  create_table "corporates_email_domains", id: false, force: true do |t|
    t.integer "corporate_id",    null: false
    t.integer "email_domain_id", null: false
  end

  add_index "corporates_email_domains", ["corporate_id", "email_domain_id"], name: "index_corporates_email_domains_corporate_id_and_email_domain_id", unique: true, using: :btree
  add_index "corporates_email_domains", ["email_domain_id", "corporate_id"], name: "index_corporates_email_domains_email_domain_id_and_corporate_id", unique: true, using: :btree

  create_table "daily_activities", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_activity_id"
    t.date     "start_time"
    t.integer  "steps_covered"
    t.integer  "calories"
    t.string   "feel"
    t.string   "source"
  end

  add_index "daily_activities", ["user_activity_id"], name: "index_daily_activities_on_user_activity_id", using: :btree

  create_table "email_domains", force: true do |t|
    t.string   "domain",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.integer  "user_id"
    t.string   "text"
    t.float    "points"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "fitness_levels", force: true do |t|
    t.integer  "user_id"
    t.integer  "fitness_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fitness_levels", ["fitness_id"], name: "index_fitness_levels_on_fitness_id", using: :btree
  add_index "fitness_levels", ["user_id", "fitness_id"], name: "index_fitness_levels_on_user_id_and_fitness_id", using: :btree
  add_index "fitness_levels", ["user_id"], name: "index_fitness_levels_on_user_id", using: :btree

  create_table "fitnesses", force: true do |t|
    t.string   "name"
    t.integer  "point"
    t.integer  "number_of_stars"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "friendships", force: true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.boolean  "confirmed",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "friendships", ["friend_id"], name: "index_friendships_on_friend_id", using: :btree
  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id", using: :btree

  create_table "identities", force: true do |t|
    t.text     "uid"
    t.string   "provider"
    t.integer  "user_id"
    t.string   "auth_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "username"
    t.string   "image"
    t.string   "email"
    t.string   "token"
    t.string   "secret"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "insight_user_activities", force: true do |t|
    t.integer  "insight_id"
    t.integer  "user_activity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "insight_user_activities", ["insight_id"], name: "index_insight_user_activities_on_insight_id", using: :btree
  add_index "insight_user_activities", ["user_activity_id"], name: "index_insight_user_activities_on_user_activity_id", using: :btree

  create_table "insights", force: true do |t|
    t.integer  "priority"
    t.hstore   "rule"
    t.integer  "action_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "insights", ["action_id"], name: "index_insights_on_action_id", using: :btree
  add_index "insights", ["rule"], name: "index_insights_on_rule", using: :gin

  create_table "insights_qualifications", id: false, force: true do |t|
    t.integer "insight_id"
    t.integer "qualification_id"
  end

  add_index "insights_qualifications", ["insight_id"], name: "index_insights_qualifications_on_insight_id", using: :btree
  add_index "insights_qualifications", ["qualification_id"], name: "index_insights_qualifications_on_qualification_id", using: :btree

  create_table "invitations", force: true do |t|
    t.integer  "sender_id"
    t.string   "recipient_email"
    t.string   "token"
    t.datetime "sent_at"
    t.string   "new"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invitations", ["sender_id"], name: "index_invitations_on_sender_id", using: :btree

  create_table "levels", force: true do |t|
    t.string   "name"
    t.integer  "score"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mailboxer_conversation_opt_outs", force: true do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  create_table "mailboxer_conversations", force: true do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "mailboxer_conversations", ["subject"], name: "index_mailboxer_conversations_on_subject", using: :btree

  create_table "mailboxer_notifications", force: true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
    t.integer  "priority"
    t.boolean  "header",               default: false
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree

  create_table "mailboxer_receipts", force: true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree

  create_table "memberships", force: true do |t|
    t.integer "team_id"
    t.integer "user_id"
    t.boolean "confirmed", default: false
  end

  add_index "memberships", ["team_id"], name: "index_memberships_on_team_id", using: :btree
  add_index "memberships", ["user_id", "team_id"], name: "index_memberships_on_user_id_and_team_id", unique: true, using: :btree
  add_index "memberships", ["user_id"], name: "index_memberships_on_user_id", using: :btree

  create_table "points", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "score"
    t.string   "description"
    t.integer  "user_id"
  end

  add_index "points", ["user_id"], name: "index_points_on_user_id", using: :btree

  create_table "posts", force: true do |t|
    t.text     "name"
    t.integer  "user_id"
    t.boolean  "visible"
    t.boolean  "deleted"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "creator_id"
    t.integer  "team_id"
    t.integer  "subject_id"
    t.integer  "subject_type"
  end

  add_index "posts", ["creator_id"], name: "index_posts_on_creator_id", using: :btree
  add_index "posts", ["user_id", "created_at"], name: "index_posts_on_user_id_and_created_at", using: :btree

  create_table "profiles", force: true do |t|
    t.hstore   "assessment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "challenge_id"
    t.integer  "profile_number"
  end

  add_index "profiles", ["assessment"], name: "index_profiles_on_assessment", using: :gin
  add_index "profiles", ["challenge_id"], name: "index_profiles_on_challenge_id", using: :btree

  create_table "qualifications", force: true do |t|
    t.integer  "priority"
    t.hstore   "qualification"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "qualifications", ["qualification"], name: "index_qualifications_on_qualification", using: :gin

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_answers", force: true do |t|
    t.integer  "attempt_id"
    t.integer  "question_id"
    t.integer  "option_id"
    t.boolean  "correct"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "skip"
  end

  create_table "survey_attempts", force: true do |t|
    t.integer "participant_id"
    t.string  "participant_type"
    t.integer "survey_id"
    t.boolean "winner"
    t.integer "score"
  end

  create_table "survey_options", force: true do |t|
    t.integer  "question_id"
    t.integer  "weight",      default: 0
    t.string   "text"
    t.integer  "point"
    t.boolean  "correct",     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_questions", force: true do |t|
    t.integer  "survey_id"
    t.string   "text"
    t.boolean  "has_one_option"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_surveys", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "attempts_number", default: 0
    t.boolean  "finished",        default: false
    t.boolean  "active",          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "team_activities", force: true do |t|
    t.integer  "team_id"
    t.integer  "challenge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "start_time"
    t.string   "status"
  end

  add_index "team_activities", ["challenge_id"], name: "index_team_activities_on_challenge_id", using: :btree
  add_index "team_activities", ["team_id", "challenge_id"], name: "index_team_activities_on_team_id_and_challenge_id", unique: true, using: :btree
  add_index "team_activities", ["team_id"], name: "index_team_activities_on_team_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "slogan"
    t.integer  "points"
    t.string   "url"
    t.integer  "max_members"
    t.string   "status"
    t.string   "members_file_file_name"
    t.string   "members_file_content_type"
    t.integer  "members_file_file_size"
    t.datetime "members_file_updated_at"
    t.integer  "corporate_id"
    t.string   "team_identifier"
    t.string   "visibility"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
  end

  add_index "teams", ["team_identifier"], name: "index_teams_on_team_identifier", unique: true, using: :btree

  create_table "user_activities", force: true do |t|
    t.integer  "user_id"
    t.integer  "challenge_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "steps_covered"
    t.string   "status"
    t.string   "level"
    t.string   "state"
  end

  add_index "user_activities", ["user_id", "challenge_id", "status"], name: "index_user_activities_on_user_id_and_challenge_id_and_status", using: :btree

  create_table "user_information_dispatches", force: true do |t|
    t.integer  "user_id"
    t.string   "information"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_information_dispatches", ["information"], name: "index_user_information_dispatches_on_information", using: :btree
  add_index "user_information_dispatches", ["user_id"], name: "index_user_information_dispatches_on_user_id", using: :btree

  create_table "user_levels", force: true do |t|
    t.integer  "user_id"
    t.integer  "level_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_levels", ["level_id"], name: "index_user_levels_on_level_id", using: :btree
  add_index "user_levels", ["user_id"], name: "index_user_levels_on_user_id", using: :btree

  create_table "user_profiles", force: true do |t|
    t.integer  "profile_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_profiles", ["profile_id"], name: "index_user_profiles_on_profile_id", using: :btree
  add_index "user_profiles", ["user_id"], name: "index_user_profiles_on_user_id", using: :btree

  create_table "user_tasks", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  add_index "user_tasks", ["user_id"], name: "index_user_tasks_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                    default: "", null: false
    t.string   "encrypted_password",       default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "user_name"
    t.integer  "invitation_id"
    t.integer  "invitation_limit"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invited_to_team_id"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "invitations_count"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "state"
    t.integer  "average_steps"
    t.string   "gender"
    t.date     "birth_date"
    t.string   "city"
    t.string   "organisation"
    t.integer  "foot"
    t.integer  "inches"
    t.integer  "weight"
    t.integer  "slab"
    t.datetime "bmi_updated_at"
    t.string   "living"
    t.date     "last_synced_at"
    t.date     "last_fitbit_synced_at"
    t.date     "last_moves_synced_at"
    t.datetime "first_invitation_sent_on"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["invitation_id"], name: "index_users_on_invitation_id", using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invited_by_id", "invited_by_type"], name: "index_users_on_invited_by_id_and_invited_by_type", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", name: "mb_opt_outs_on_conversations_id", column: "conversation_id"

  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", name: "notifications_on_conversation_id", column: "conversation_id"

  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", name: "receipts_on_notification_id", column: "notification_id"

end
